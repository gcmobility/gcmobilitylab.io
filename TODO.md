# To Do List

Some areas to explore for this site:

* commit branch iteration27

## themes
Try to create a WET-BOEW theme

## Plugins
source: https://github.com/getpelican/pelican-plugins
### Testing

### Installed
* Sitemap 	Generates plain-text or XML sitemaps
* Share post 	Creates share URLs of article
* Auto Pages 	Generate custom content for generated Author, Category, and Tag pages (e.g. author biography)
* Dead Links 	Manage dead links (website not available, errors such as 403, 404)
* Neighbor articles 	Adds next_article (newer) and prev_article (older) variables to the article's context

### Failed
* Pelican YouTube 	Enables you to embed YouTube videos in your pages and articles
  * Issue logged here: https://github.com/kura/pelican_youtube/issues/6

### To Test


* GA Page View 	Display Google Analytics page views on individual articles and pages
* Events 	Add event start, duration, and location info to post metadata to generate an iCalendar file
* Disqus static comments 	Adds a disqus_comments property to all articles. Comments are fetched at generation time using disqus API
* Better figures/samples 	Adds a style="width: ???px; height: auto;" attribute to any <img> tags in the content

* Footer Insert 	Add standardized footer (e.g., author information) at end of every article
* Jinja2 Content 	Allows the use of Jinja2 template code in articles, including include and import statements. Replacement for pelican-jinja2content.
* Just table 	Allows you to easily create and manage tables. You can embed the tables into posts with a simple way.
* Libravatar 	Allows inclusion of user profile pictures from libravatar.org
* Load CSV 	Adds csv Jinja tag to display the contents of a CSV file as an HTML table

* PDF generator 	Automatically exports articles and pages as PDF files
* PDF Images 	If an img tag contains a PDF, EPS or PS file as a source, this plugin generates a PNG preview which will then act as a link to the original file.
* pelican-ert 	Allows you to add estimated reading time of an article
* pelican_javascript 	Allows you to embed Javascript and CSS files into individual articles
* Pelican Meetup Info 	Include your Meetup.com group and event information on generated pages and articles
* pelican-toc 	Generates a Table of Contents and make it available to the theme via article.toc
* Photos 	Add a photo or a gallery of photos to an article, or include photos in the body text. Resize photos as needed.
* Pin to top 	Pin Pelican's article(s) to top "Sticky article"
* Random article 	Generates a html file which redirect to a random article
* Read More link 	Inserts an inline "read more" or "continue" link into the last html element of the object summary
* Related posts 	Adds the related_posts variable to the article's context
* Shortcodes 	Easy and explicit inline jinja2 macros
* Similar Posts 	Adds a list of similar posts to every article's context.
* Static comments 	Allows you to add static comments to an article
* Summary 	Allows easy, variable length summaries directly embedded into the body of your articles
* tag_cloud 	Provides a tag_cloud
* Video Privacy Enhancer 	Increases user privacy by stopping YouTube, Google, et al from placing cookies via embedded video


Simon (Design and change management)
Christine

Identity Management
*

To Do:
* Share the SOP
* Share heat maps
* email of other areas (groups or stakeholders)
  - GC coworking
    - Active Directory login for login
    - Security
  - CDOE
  - Credentials Management
  - CIO onboarding
  - FAA reference
  - Active Directory
  - Pan Canadian Security Framework
* TBS Security Pilots
  - Especialy for exchanging
* PSPC Industrial Security

* Workshop next Thursday PM
  * Validate
  * Ideate
