REM This file should be executed on the MS windows command line

REM Install PIP requirements from requirements.txt
pip install -r requirements.txt

REM remove existing pelican site resources in quiet mode
REM We now keep the local theme directory intact
REM del .\themes\gcmobility /Q

del public /Q

REM copy the GCmobility theme from working directory to site directory
REM Moved theme development locally for now to support rapid prototyping
REM robocopy ..\pelican-themes\gcmobility .\themes\gcmobility /S /E

REM generate a new Pelican site
REM pelican content -t .\themes\gcmobility --ignore-cache

pelican content --ignore-cache

REM listen for web requests on localhost:8000

pelican --listen
