# Welcome to the GCmobility Contribution Page

These are some general approaches for contributiing to the project. This page will focus on contributing content and general contribution.

## About Pelican Site Input content

Contributing content in the form of posts (eg blog posts) and pages (time independant). First off read up on how to [create content for Pelican sites](https://docs.getpelican.com/en/stable/content.html) including Pelican Posts and [Pelican Pages](https://docs.getpelican.com/en/stable/content.html#pages).

Whether you are contributing [pages or posts](https://docs.getpelican.com/en/stable/content.html#articles-and-pages) we ask that you try to use Markdown as the prefered format. Pelican offers other formats for input, but it helps to keep things organized if we stick to a single input format.

On the rare occasion we can also accept straight up HTML, but make sure all the styling and CSS use the existing site theme.

## Git Workflows

The general approach for contributing to the GCmobility project is to leverage some concepts used in Git and Gitlab. These concepts help segregate work by contributors and facilitates a streamlined pulishing workflow.

For those unfamiliar with Git we reccomend reading up on two approaches to workflow management with Git. The first and original is [GitFlow](https://nvie.com/posts/a-successful-git-branching-model/), and the second is [GitHub Flow](https://guides.github.com/introduction/flow/) they both have things to teach us.

The two approaches differ slightly where GitFlow is concerned with specificly versioned software packages, where as  GitHub flow focuses on continuous itegration and continuous delivery of primarily web applications. Where GitFlow is robust, Github Flow is focused on agility.

The GCmobility project fits both of these models. First, because the GCmobility project is delivered in a structured 2 week iteration cycle, site contents and project deliverables are often delivered at the end of the iteration and thus could be seen a version release. However, some content might be released before the end of the iteration like a blog post that is not associated with a planning or update notice.

## GCmobility workflow.

The general workflow we are experimenting with would look like the following.

A new member of the team joins and starts contributing content. They would generally get a user account and be added to the team repository. They would take the current Iteration branch (eg Iteration##) and create a second branch (eg Iteration##-username). They would do their work and when complete submit a merge request back to the iteration branch (eg Iteration##). Then at the end of the iteration all the merge requests are merged to a single Iteration## branch and finaly merged to master.

Alternatively, if there is something to submit outside of the regular iterative cycle then they might start with branch 'master' and create a new branch (eg. username-blog-YYYY-MM-DD) add whatever they are adding and then submit a merge request back to master.
