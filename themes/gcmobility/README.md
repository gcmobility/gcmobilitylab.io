# GCmobility Theme

The GCmobility theme was developed for the site https://gcmobility.gitlab.io

Site source code is located at: https://gitlab.com/gcmobility/gcmobility.gitlab.io

Theme Source code: https://gitlab.com/gcmobility/gcmobility.gitlab.io/-/tree/master/themes/gcmobility

## Derivative Theme
This theme is an extension of the 'notmyidea' default theme for Pelican

notmyidea theme source: https://github.com/getpelican/pelican/tree/master/pelican/themes/notmyidea

More about Pelican themes here: https://docs.getpelican.com/en/stable/themes.html
More about Jinja templating engine: https://jinja.palletsprojects.com/en/2.11.x/
## Dependencies
* Pelican ~=4.1.0
* Jinja2

## Plugins
The following plugins have been incorporated into this theme
* [share_post](https://github.com/getpelican/pelican-plugins/tree/master/share_post)


## Settings
These are some recomended settings for this themes
* USE_FOLDER_AS_CATEGORY = False
*

## Templates
This sections captures new and updated templates

### categories.html
* Created new template based on category.html and using patterns from tags.html
* for autopages plugins
  * .
* Implemented Settings:
  * CATEGORY_PAGE_PATH

### tags.html
* for autopages plugins
  * .
* Implemented Settings:
  * TAG_PAGE_PATH

### authors.html
* for autopages plugins
  * .
* Implemented Settings:
  * AUTHOR_PAGE_PATH

### gitlab.html
* Created new template based on github.html
* Updated base.html template to include {% include 'gitlab.html' %}
* Updated Ribbon URL's according to https://gitlab.com/seanwasere/fork-me-on-gitlab
* Implemented Settings:
  * GITLAB_URL = 'https://gitlab.com/your-site-repository'
  * GITLAB_POSITION = 'left' or 'right' (default: right) (colours: right=blue, left=cyan)

### share_post.html
* Created new template to house HTML markup required by share_post plugin

### neighbors.html
* Added new template to support neighbors plugin.

### article.html
* added {% include 'share_post.html' %} for share_post plugin
* added {% include 'neighbors.html' %} for neighbors plugin
* added {% include 'article_toc.html' %} for article plugin

### index.html
* added {% include 'share_post.html' %} for share_post plugin
* added {% include 'neighbors.html' %} for neighbors plugin

### base.html
* Added "<p>This site uses the <a href="https://gitlab.com/gcmobility/gcmobility.gitlab.io/-/tree/master/themes/gcmobility">GCmobility</a> theme </p>"
* added {% include 'pelicanjs_css.html' %} inside <head></head>
* added {% include 'pelicanjs_js.html' %} after </body>

### article_toc.html
* Added new template to support pelican-toc plugin for table of contents
* see article.html template for reference

### pelicanjs_css
* created to support pelican_javascript plguin for CSS in header
* More docs as per https://github.com/mortada/pelican_javascript/blob/master/README.md

### pelicanjs_js
* created to support pelican_javascript plguin for CSS after </body>
* More docs as per https://github.com/mortada/pelican_javascript/blob/master/README.md
