#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

# This file is only used if you use `make publish` or
# explicitly specify it as your config file.

import os
import sys
sys.path.append(os.curdir)
from pelicanconf import *

SITEURL = 'https://gcmobility.gitlab.io'
RELATIVE_URLS = False

FEED_ALL_ATOM = 'feeds/all.atom.xml'
CATEGORY_FEED_ATOM = 'feeds/{slug}.atom.xml'

FEED_ALL_RSS = 'feeds/all.rss.xml'
CATEGORY_FEED_RSS = 'feeds/{slug}.rss.xml'

DELETE_OUTPUT_DIRECTORY = True

# Following items are often useful when publishing

#DISQUS_SITENAME = ""
#GOOGLE_ANALYTICS = ""

#############################################################
# Custom Settings for gcmobility                            #
# ref https://docs.getpelican.com/en/stable/settings.html   #
#############################################################


GOOGLE_ANALYTICS = 'UA-158042885-1'

########################
# Site Plugin Settings #
########################
# This option expects a sub-directory as part of your site
PLUGIN_PATHS = ['./plugins/']

# plugins final list for production.
PLUGINS = ['sitemap','share_post','neighbors','pelican-toc','pelican_javascript']
