#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals


#########################################
# Default Pelican settings              #
# #commented settings are not in effect #
#########################################
AUTHOR = 'David Sampson'
SITENAME = 'GCmobility'
SITESUBTITLE =  'Designing For Mobile Careers '
SITEURL = ''

PATH = 'content'
OUTPUT_PATH = 'public'

# Timezones: https://en.wikipedia.org/wiki/List_of_tz_database_time_zones#
TIMEZONE = 'America/Toronto'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Wiki', 'https://wiki.gccollab.ca/GCmobility'),
        ('GCmobility on GCcollab', 'https://gccollab.ca/groups/profile/2183003/engcmobilityfrgcmobilitu00e9'),
        ('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
         )

# Social widget
SOCIAL = (('Follow GCmobility on Twitter', 'https://twitter.com/GCmobility'),
          )
# TWITTER_USERNAME = 'GCmobility'

DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

#############################################################
# Custom Settings for gcmobility                            #
# ref https://docs.getpelican.com/en/stable/settings.html   #
#############################################################

THEME = './themes/gcmobility'
DEFAULT_CATEGORY = 'Blog'
DISPLAY_PAGES_ON_MENU = True
DISPLAY_CATEGORIES_ON_MENU = True
USE_FOLDER_AS_CATEGORY = False
GITLAB_URL = 'https://gitlab.com/gcmobility/gcmobility.gitlab.io'
GITLAB_POSITION = 'right'
STATIC_PATHS = ['images','charts','css','data',]

########################
# Site Plugin Settings #
########################
# This next option expects a cloned repository of pelican plugins to reside outside the site.
# git clone git clone --recursive https://github.com/getpelican/pelican-plugins

# Uncomment For Testing new plugins from external Directory
#PLUGIN_PATHS = ['../pelican-plugins']

# Uncomment For publishing imported plugins from internal plugins directory
PLUGIN_PATHS = ['./plugins/']

# Testing plugins short list
PLUGINS = ['sitemap','share_post', 'neighbors', 'pelican-toc', 'pelican_javascript']

# Testing Plugins Long List
#PLUGINS = ['sitemap','share_post','deadlinks', 'neighbors','ga_page_view',]

# Plugin Sitemap
SITEMAP = {
    'format': 'xml',
    'priorities': {
        'articles': 0.5,
        'indexes': 0.5,
        'pages': 0.5
    },
    'changefreqs': {
        'articles': 'monthly',
        'indexes': 'daily',
        'pages': 'monthly'
    }
}



# Deadlinks Checker
# source: https://github.com/silentlamb/pelican-deadlinks
DEADLINK_VALIDATION = True
DEADLINK_OPTS = {
        'archive':  True,
        'classes': ['custom-class1', 'disabled'],
        'labels':   True,
        'timeout_duration_ms': 1000,
        'timeout_is_error':    False,
    }

# Plugin pelican-toc
# no settings to set for this plugin yet
