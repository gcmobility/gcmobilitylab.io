# Welcome to the GCmobility Install Guide

This install guide assumes you are developing on a linux box or perhaps the Linux Subsystem for Windows

## Prerequisites
Install the Python scripting Language

`apt-get install python`

[Install Git client](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
`apt-get install git-all `

Make a new directory to build the site in

`mkdir gcmobility-site | cd gcmobility-site`

Create a local close of the main git repository:

SSH:

`git@gitlab.com:gcmobility/gcmobility.gitlab.io.git`

HTTPS:

`https://gitlab.com/gcmobility/gcmobility.gitlab.io.git`

OPTIONAL: Fork your own copy of the main project and check out your forked version. [Click here ot fork](https://gitlab.com/gcmobility/gcmobility.gitlab.io/-/forks/new)

[Install virtualenv](https://virtualenv.pypa.io/en/latest/installation.html) through PIP

`pip install virtualenv`

Create a python virtual environment of the same version as virtualenv is installed into:

`virtualenv .`

[Activate](https://virtualenv.pypa.io/en/latest/user_guide.html#activators) the virtual environment

`source bin/activate`

Install all the dependencies for the gcmobility site

`cd gcmobility.gitlab.io`
`pip install -r requirements.txt`

This process will have also installed the [Pelican Static Site Generator](https://blog.getpelican.com/) which will generate the website



## Building the Site

Navigate to where you built the virtual environment and cloned the site source code

`cd gcmobility-site`

Change into the site directory holding the config files for pelican

`cd gcmobility.gitlab.io`

Start up the pelican site generator with auto re-Load

`pelican --listen --autoreload -v --debug`

Read up on how to create content for the site.

## Restarting the site

Stop the pelican script

`ctrl-z`

kill any hung processes for pelican

`pkill -9 pelican`

restart Pelican

`pelican --listen --autoreload -v --debug`


## Upgrade Pelican.

From time to time there might be some new updates. The recommended way to do this is to re-run:

`pip install -r requirements.txt`
