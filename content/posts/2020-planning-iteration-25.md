Title: GC Mobility Iteration Planning 25
Date: 2020-03-02
Modified: 2020-03-03
Category: Blog
Tags: planning
Slug: gcmobility-iteration-planning-25
Toc_Include_Title: false
Status: published
Summary: This post outlines the goals for Iteration 25 of the GCmobility initiative

## Intro
GCmobility is managed using the Agile Scrum of iterative delivery. Each iteration occurs over a 2 week period. The Iteration starts with a planning meeting and the Iteration ends with an Iteration review which includes a product demonstration.

## Iteration Goals
This section highlights some of the specific goals we have identified for this iteration. Currently we are transitioning between two product backlogs, on Hosted on GCCode and the other hosted on Gitlab.


### GCcode
These issues are managed on [GCcode](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility) which is internal to the Government of Canada. Some links may only be accessible to the internal GC network during this transition time.

* clean up backlog
* [x] Remove SSC branding from Slide decks.
* [x] Add generic GC branding to slide deck
* [x] [Iteration Review Questionnaire]({filename}/pages/iteration-feedback.md)


### Gitlab
These issues are managed on [Gitlab.com](https://gitlab.com/gcmobility/gcmobility-project) and is accessible generally to the World Wide Web. If links are blocked, it may be due to your organizations firewall.

* [ ] migrate / create backlog
* [ ] Import any issues exported from GCcode after backlog clean up
*

### Site
This section highlights the iteration goals for the [gcmobility.gitlab.io site](https://gcmobility.gitlab.io) and are hosted on Gitlab.com.

* [x] Post Iteration 24 Video and slides
* [x] Fix bulleted unorded lists
* [x] Confirm the use of HTTPS
* [x] Create dashboards for project stats
  - [x] Project Metrics
  - [x] Stakeholder Metrics
  - [x] Web Metrics analytics
* [x] Move Demonstration content to site contents
  -  [x] Recent pivots
  -  [x] Non Demonstration Items
* [ ] Migrate older Content
  - [ ] Digital Upskilling
  - [ ] Iteration 20
* [ ] New Content
  - [ ] Mobility Data: Sources, Metrics and Gaps
  - [ ] GCmobility Office Hours and Bookings (Contact us)

### Non Demonstration items
These items may include various activities such as recuring events, project management tasks and stakeholder engagements. These items are shared to provide insight into the various types of work that go into GCmobility, beyond the typical "product development" tasks often communicated during an iteration cycle.

**Recuring Activities**

* Weekly Update for the Senior Assistant Deputy Minister of Networks, Security and Digital Services (NSDS), Shared Services Canada (SSC)
* GCMobility
    * Iteration Planning
    * Itertation Review
    * Open Office Hours (Tues and Thurs: 10-12, 1-3)
* GCmobility Digital Upskilling Pilot
    * Planning
    * Work Sessions
* Service Evolution Team Meeting
* GCmobility Communications Check in
* GCmobility Bilat with Director of Service Evolution
* Common Desktop Operation Environment (CDOE) Working Group Meeting

**Iteration Specific Activities**

* [x] Identity Management Workshop (SSC)
* [x] HR Data Discussion with Free Agents
* [x] Teleworking presentation by Frank Assu
* [ ] Beyond 2020 Webcast "What you need to Fuel Public Service Innovation"
* [x] Coffee and Agile - Agile Community of Enablement (ACE): Virtual Meetup
* [ ] Engagement with Teams implementing Jarvis (PCH, ECCC)(rescheduled)
* [x] Mobility Perspectives of Executives
* [x] Remote interactive Workshop - Futures in Government

## Agile Resources
To learn more about Agile Scrum there are many resources on the web. One resource you may consider checking out are the resources offered by [Mike Cohn](https://www.mountaingoatsoftware.com/company/about-mike-cohn) at [Mountain Goat Software](https://www.mountaingoatsoftware.com/), where you can [Learn About Agile](https://www.mountaingoatsoftware.com/agile).
