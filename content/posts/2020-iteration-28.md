Title: GC Mobility Update Iteration 28
Date: 2020-04-23
Modified: 2020-04-23
Category: Blog
Tags: review, demos
Slug: gcmobility-iteration-review-28
Toc_Include_Title: false
Status: published
Summary: Iteration 28 of GCmobility focused on our most recent pivots in response to COVID-19 including setting up an alpha mobility service.

## Summary
Iteration 28 of GCmobility focused on our most recent pivots in response to COVID-19 including setting up an alpha mobility service.
## Overview
This post represents the update for Iteration 28 of the GCmobility project.

After reading this update please take some time to complete the [GCmobility Retrospective Questionnaire]({filename}/pages/iteration-feedback.md) after you have viewed the demo and/or checked out the resources on this page. The questionnaire asks three simple questions. What we should:

It has now been X weeks since Federal Public Servants have shifted their working locations from the office to their home. That is if they are lucky enough to be identified as priority workers with access to their departmental networks. Some of us, including the GCmobility team have a unique opportunity to practice working in the open and off the Government of Canada Network.

In our latest [Iteration 27 Update]({filename}/posts/2020-Iteration-27.md) we shared with you some of the challenges faced by public servants and projects like GCmobility to keep moving forward. Since making the decision to pivot due to Covid-19 situation GCmobility has made some significant movement forward on the concept of an interdepartment digital service to support mobility.

Our latest pivot in an ongoing [History of GCmobility Pivots]({filename}/posts/2020-gcmobility-pivots.md) gave us an opportunity to start and put together the [GCmobility Service]({filename}/pages/service-about.md). We are excited to launch this service, however we have limited capacity. The service uses the only human we have working on GCmobility, and as such we are scoping our target users to the Canada's Free Agent program.

The GCmobility service will help us address one of our [two core objectives]({filename}/pages/about.md).

> Decrease the time it takes for mobile employees (eg. Free Agents) to cross board (onboard and off board) between departments during their short term and rapid assignments throughout the Canadian Federal Government

The service currently has the following components:

* GCmobility [Consierge Service]({filename}/pages/service-about.md#consierge-service)
* GCmobility [Resource Library]({filename}/pages/service-about.md#resource-library)
* GCmobility [Community support]({filename}/pages/service-about.md#community-support)
* GCmobility [Discussion on GCmessage]({filename}/pages/service-about.md#discussion-on-gcmessage)
* GCmobility [Mailing List]({filename}/pages/service-about.md#mailing-list)
* GCmobility [News Feeds]({filename}/pages/service-about.md#community-support)
* GCmobility on [Twitter]({filename}/pages/service-about.md#twitter)

Our current focus is to ensure that the Free Agent family is well supported as the challenges with crossboarding between departments are magnified during COVID-19.

We hope to bring back recorded demos next iteration. The challenge has been since moving off the GC network and onto a personal device some of our tools have changed. One tool impacted has been WebEx which has limited support for operating systems like [Ubuntu Linux](https://ubuntu.com/). As such our approach to recording demos will need to change. We have a couple of possibilities we have explored, however we need some more time to play and test things out.

[Learn more about the GCMobility initiative]({filename}/pages/about.md), a joint partnership between [Canada's Free Agents](https://www.linkedin.com/company/freeagents-agentslibres/about/) and [Shared Services Canada](https://www.canada.ca/en/shared-services.html).

If you are interested in joining us for future live demonstrations please [reach out to us]({filename}/pages/contact-us.md).

## Living The Pivot
A pivot is a "structured course correction designed to test a new fundamental hypothesis about the product, strategy, and engine of growth.“ Alternatively, Steve Blank defines a pivot as "changing (or even firing) the plan instead of the executive”. (source: [Wikipedia: Lean Startup#pivot](https://en.wikipedia.org/wiki/Lean_startup#Pivot))

### COVID-19 Pivot

This section captures some of the potential ways we could pivot GCmobility in light of the ever changing situation regarding COVID-19. We expanded on the following concepts in our [Iteration 27 update]({filename}/posts/2020-Iteration-27.md) if you want some more context.

**Step 1**: Put on your own oxygen mask (or [cloth medical mask](https://www.canada.ca/en/health-canada/services/drugs-health-products/medical-devices/activities/announcements/covid19-notice-home-made-masks.html)) before helping others    
**Step 2**: Understand that for some employees, remote work is hard    
**Step 3**: Find a need and fill it    


### Recent GCmobility Pivots:

* **COVID-19**: How to work effectively "Off line"
* **Technology**: POC 2: 1 Phone, 1 Laptop/Tablet, 1 canonical e-mail)
* **Processes**: Move Focus from onboarding to cross-boarding
* **People**: Hyper Mobile Employees and Executives (rates > 30%)
* **Policies**: Enterprise Architecture Supporting Mobility
* **Services**: Crossboarding as an enterprise, interdepartmental, internal digital service

## Iteration Results

* GCmobility [Consierge Service]({filename}/pages/service-about.md#consierge-service)
* GCmobility [Resource Library]({filename}/pages/service-about.md#resource-library)
* GCmobility [Community support]({filename}/pages/service-about.md#community-support)
* GCmobility [Discussion on GCmessage]({filename}/pages/service-about.md#discussion-on-gcmessage)
* GCmobility [Mailing List]({filename}/pages/service-about.md#mailing-list)
* GCmobility [News Feeds]({filename}/pages/service-about.md#community-support)
* GCmobility on [Twitter]({filename}/pages/service-about.md#twitter)

**COVID-19 Priorities**
Due to the current situation around COVID-19 GCmobility is focusing towards the following Priorities

* [ ] Ensure all Free Agents are supported with remote working
  - [x] Create cross boarding tracker
  - [x] Offer support regarding MyKey/VPN/GCSRA
* [x] Ensure all Free Agents are supported with remote working
  - [x] Prototype "GCmobility service desk"
    - [x] Setup and Launch [Gitlab Service Desk](https://docs.gitlab.com/ee/user/project/service_desk.html) for GCmobility
    - [x] GCmobility Generic e-mail hosted by Gmail
    - [x] Autogeneration of tickets from e-mail
    - [x] Gitlab Service Desk bot replies to original recipient when ever an issue is updated
    - [x] Service Desk admins handle service desk issues in a simmilar way they do for other project issues or product backlog.
  - [x] Address Parks Canada Issues.


## Iteration Review
### Index
Check out the embeded Iteration Review video and presentation.

If you are tight on time then try these time stamped video chapters:

<!-- include timesstamped links to sections of video eg (https://youtu.be/U8Q5e5WCg8I?t=1m35s)-->

### Video

Demonstration video </br>

The Video will be posted after the demo has been recorded. We have not had a chance to record a demo for this iteration. Check back soon or subscribe to our [RSS](https://gcmobility.gitlab.io/feeds/all.rss.xml) or [ATOM](https://gcmobility.gitlab.io/feeds/all.atom.xml) feeds to receive real time updates on our site contnet.

<!--
<iframe width="560" height="315" src="https://www.youtube.com/embed/U8Q5e5WCg8I" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/1kZ7j-5MvFk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


-->

### Slide Deck
Demonstration Presentation </br>

The slide deck for this demo will be posted after it has been recorded. We have not had a chance to record a demo for this iteration. Check back soon or subscribe to our [RSS](https://gcmobility.gitlab.io/feeds/all.rss.xml) or [ATOM](https://gcmobility.gitlab.io/feeds/all.atom.xml) feeds to receive real time updates on our site contnet.

<!-- Go into slide deck, select file > publis to web > embed
Do not use share link

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vT0-21U3akusGQyYoutEan2XhEZ-bVaqRv7mRKCppHn5-f1lAunnyaFmj_C3K3T0A/embed?start=false&loop=false&delayms=3000" frameborder="0" width="640" height="389" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

-->

## News and Upcoming Events

**Iteration Schedule**

Iteration   |Start Date |Demo Date  |End Date
------------|-----------|-----------|-----------
29          | 4/27/2020 | 5/7/2020  | 5/8/2020
30          | 5/11/2020 | 5/21/2020 | 5/22/2020
31          | 5/25/2020 | 6/4/2020  | 6/5/2020
32          | 6/8/2020  | 6/18/2020 | 6/19/2020
33          | 6/22/2020 | 7/2/2020  | 7/3/2020
34          | 7/6/2020  | 7/16/2020 | 7/17/2020
35          | 7/20/2020 | 7/30/2020 | 7/31/2020
36          | 8/3/2020  | 8/13/2020 | 8/14/2020
37          | 8/17/2020 | 8/27/2020 | 8/28/2020
38          | 8/31/2020 | 9/10/2020 | 9/11/2020
39          | 9/14/2020 | 9/24/2020 | 9/25/2020
40          | 9/28/2020 | 10/8/2020 | 10/9/2020

**GC Mobility Community Meetups**

Schedule for 2020-2021: **TBD**

# Additional Resources:

* Be Mobile with [GCmobility!](http://bit.ly/gcmobility-wiki)
* Soyez mobile avec [GCmobility!](http://bit.ly/gcmobility-wiki)

# Next Iteration
We will see you back here in 2 weeks for the next installment of our iteration reviews.

* [Iteration 29 Planning]({filename}/posts/2020-planning-iteration-29.md): April 27, 2020
* Next Demo/Update: May 7, 2020
