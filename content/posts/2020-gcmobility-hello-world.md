Title: Hello World! We are GCmobility.
Date: 2020-02-04
Category: Blog
Tags: gcmobility
Slug: hello-world-gcmobility
Status: published

Hello World Wide Web,

GCmobility is about helping Canadian Federal Public Servants design mobile careers. GCmobility is an internal initiative within the Canadian Federal Government. You can learn more about this initiative through the [About](https://gcmobility.gitlab.io/pages/about-gcmobility.html) page.

## Our Vision:

By 2025, Canadian Federal Public Servants
Have the tools to work effectively and efficiently
From anywhere,
At anytime
For any department and beyond

GCmobility started in 2017 as GClaptop with the vision of mobile employees to be enabled by a single technology stack. For example, when you first join the Government of Canada, you would be issued a single laptop or tablet, a phone and a @canada.ca e-mail. The concept was simple, however was complex to implement. GCmobility now takes broader look at the various aspects of mobility that might prevent employees from moving around. These barriers may be beyond the realm of finding the right technology solution.

The Government of Canada (GC or GoC) is committed to developing a mobile, and modern workforce. The [Government of Canada Strategic Plan for Information Management and Information Technology 2017 to 2021](https://www.canada.ca/en/treasury-board-secretariat/services/information-technology/strategic-plan-2017-2021.html) provides a high level roadmap of how to get there by outlining key Principles and Goals.

* [Principle 6: enable a modern workplace: anywhere, anytime with anyone](https://www.canada.ca/en/treasury-board-secretariat/services/information-technology/strategic-plan-2017-2021.html#toc6-6), "provides its employees with modern technology that supports information retrieval, use, sharing and collaboration by making information and data accessible when and where needed".
* [Strategic Goal 4: Agility](https://www.canada.ca/en/treasury-board-secretariat/services/information-technology/strategic-plan-2017-2021.html#toc7-4), "provides a technologically advanced workplace that supports mobility".

Beyond the Strategic Plan for IT, the Treasury Board Secretariat (TBS) developed the [Policy on Interchange Canada](https://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=12552) which "Enables mobility between the core public administration and organizations in the private, public and not‑for‑profit sectors in Canada and abroad to support job enrichment, talent management and skills exchange." In support of this policy, the [Directive on Interchange Canada Canada](https://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=12553) (2007 and 2012) is meant to "Ensure that Interchange Canada assignments are managed in a fair and transparent manner."

As a result of these policy instruments,

> " facilitates temporary assignments of individuals in and out of the core public administration to:
>
> * Ensure transfer of knowledge and expertise
> * Build a better understanding and improve networks between the core > > * Public administration and other business sectors
> * Contribute to the professional development of participants
> * Meet organizational needs, such as attracting experts, and facilitating pre-retirement transitions"

Source: [Interchange Canada](https://www.canada.ca/en/treasury-board-secretariat/services/professional-development/interchange-canada.html)

Because of the presence of Interchange Canada and its purpose to move individuals in and out of the public service, GCmobility has included "and beyond" to its vision statement, in recognition that mobility of public servants are not limited to the core public service.

# Working In the Open
So why would an internal initiative within the GC decide to come out from behind an internal networking platform and start a web presence in the open, on the web, without user authentication? Great question.

First some context, GCmobility started life on an internal to GC GitLab site. [Gitlab](https://about.gitlab.com/company/#open-source) is a company and open source software used primarily by the software development community. But it is also a great tool for managing digital projects. The [Shared Services Canada](https://www.canada.ca/en/shared-services.html)'s Innovation Team hosts an internal GitLab instance branded as [GCcode](https://gccode.ssc-spc.gc.ca/). This site is where we have done of our project management to date, starting in 2017. If you are on the internal GC network check out the [original GCMobility README page](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/blob/master/README.md).

While an internal [Git repository](https://en.wikipedia.org/wiki/Git) hosted internaly to GC served us well to get started, we knew there was a larger audience. So in steps [GCcollab](https://gccollab.ca), which can be though of a cross between Facebook and LinkedIn, built on the open source [Elgg platform](https://elgg.org/).

> "GCcollab is your professional collaboration platform.
>
> Hosted by the Government of Canada, GCcollab is designed to connect you with the information and people you need to work with across Canadian universities, colleges and all levels of government.
>
> Visitors to GCcollab can view all content on the GCcollab Wiki. Here you can find information, news of upcoming events and opportunities to collaborate, to name just a few examples. To add, modify or edit content you are required to be a registered GCcollab user. Once registered you also have access to the collaboration portion of GCcollab, where you are encouraged to join or create groups related to your expertise or general interests."

source: [GCcollab Terms and Conditions](https://gccollab.ca/terms)

The [GCmobility community group on GCcollab](https://gccollab.ca/groups/profile/2183003/engcmobilityfrgcmobilitu00e9) has been slowly growing since it started in April 2019, and as of this posting has a mebership of 176 members. These members span federal, provicial and municapl governments as well as certain external stakeholders that have joined through special invite as per the GCcollab terms and conditions.

> "Canadians are able to join GCcollab by invitation. All registered users are able and encouraged to invite other colleagues, stakeholders and partners to collaborate on GCcollab, making it a truly collaborative environment."

Digital government and digital services have been hot topics of discussion and the focus of digital and data strategic plans for a number of years within the GC. Then in March of 2019, the TBS released its first viersion of the [Government of Canada Digital Standards](https://www.canada.ca/en/government/system/digital-government/government-canada-digital-standards.html).

> "Our goal is to provide public services to Canadians which are simple to use and trustworthy. The Government of Canada’s Digital Standards form the foundation of the government’s shift to becoming more agile, open, and user-focused. They will guide teams in designing digital services in a way that best serves Canadians.
>
>   These digital standards were co-created with the public and key stakeholder groups. They are living standards and they will continue to evolve over time as we better understand the complexities involved in putting them into practice."

source: [Government of Canada Digital Standards](https://www.canada.ca/en/government/system/digital-government/government-canada-digital-standards.html).

These standards include:

* Design with users
* Iterate and improve frequently
* Work in the open by default
* Use open standards and solutions
* Address security and privacy risks
* Build in accessibility from the start
* Empower staff to deliver better services
* Be good data stewards
* Design ethical services
* Collaborate widely

While we are currently undertaking a full self assessment of how GCmobility meets this standard, this site tries to address the following digital principles.

*Work in the open by default*: It does not get much more open than the full world wide web. Some consider this this a dangerous and risky move, while others consider it mandatory to achieve the transparency expected in a democracy. Jumping onto the web may help us or hinder us, we don't know for sure, but we are hoping for the best possible outcomes.

*Use open standards and solutions*: When choosing where to host this site, we wanted to align as best we could with the standards. So [the source code](https://gitlab.com/gcmobility/gcmobility.gitlab.io) for this site is hosted on a [GitLab instance](https://about.gitlab.com/company/#open-source), which is open source and hosted by the [Gitlab.com](https://about.gitlab.com/company/) company which develops the open source platform. The website content is written in an open source text format known as [Markdown](https://daringfireball.net/projects/markdown/).

> "Thus, “Markdown” is two things: (1) a plain text formatting syntax; and (2) a software tool, written in Perl, that converts the plain text formatting to HTML. See the Syntax page for details pertaining to Markdown’s formatting syntax. You can try it out, right now, using the online Dingus."

source: [DaringFireball.net: Markdown](https://daringfireball.net/projects/markdown/)
Then The HTML pages are rendered using the [Pelican Static Site Generator](https://blog.getpelican.com/), which uses the open source [Python Programing Langugage](https://www.python.org/). As of this post the static site is hosted for free on [Gitlab Pages](https://about.gitlab.com/stages-devops-lifecycle/pages/)

*Collaborate widely*: While it was great to engage on the GCcode and GCcollab platforms, those platforms were designed to limit collaboration. GCcode only available to federal public servants. GCcollab opened up the exposure to federal, provincial, territorial and municipal governments with academics joining as they were identified. While GCcollab propper was not indexed by google by design, the [GCcollab Wiki](https://wiki.gccollab.ca) was indexed by search engines, but achieved very low rankings, in part due to the limited amount of content actually published there. In the case of the [GCmobility Wiki](https://wiki.gccollab.ca/GCmobility) content would not appear in google results unless it was super specific. So our hope is that coming to the wild digital world of the internet at large and the world wide web we will be able to engage an even larger stakeholder group.

We hope you stick around and join our growing community. To stay up to date on new website content, consider subscribing to our [Atom Feed](https://gcmobility.gitlab.io/feeds/all.atom.xml) in your favourite newsreader.

You can engage GCmobility through multiple channels on our [Contact Us page](../pages/contact us.html).

Welcome to GCmobility.

Be Mobile!
