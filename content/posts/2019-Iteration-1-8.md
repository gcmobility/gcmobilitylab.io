Title: GC Mobility Update Iterations 1-8
Date: 2019-08-01
Modified: 2020-02-27
Category: Blog
Tags: gcmobility, demos, iteration-8
Slug: gcmobility-iteration-review-1-to-8
Toc_Include_Title: false
Status: published
Summary: Iterations 1-8 of GCmobility focused on initial initiative approvals, documentation of initiative concepts, proposing a partnership approach and initiatng stakeholder engagement

*UPDATE*: This content was originally published in the [GCmobility group within GCcollab](https://gccollab.ca/groups/profile/2183003/engcmobilityfrgcmobilitu00e9). "Hosted by the Government of Canada, GCcollab is designed to connect you with the information and people you need to work with across Canadian universities, colleges and all levels of government." ([GCcollab Terms and Conditions](https://gccollab.ca/terms)). Some links may lead to internal resources.

## Iteration 1
* Started onboarding
* Draft Deck for project approval
* Participate in research interview "Github Use For Government Related Work"
* introduction to Engineering team
* Shared MyKey Process document with Free Agents

## Iteration 2
* Installation of Hyper-V
* Setup Telework Office
* Draft Deck for project approval (Continued)
* Present to Senior Director

## Iteration 3
* Demonstration of Hyper-V
* Agile lite: Planning Meetings, Demo Place Holder
* Attended Free Agent Block Cert Presentation
* Researching Failure Reports
* First Visit to GCcoworking @ L'esplanade
* Worked with Abe to design addition of IT questions to monthly CFA questionnaire

## Iteration 4
* Skiing on my birthday
* Meetup with GCCoworking manager to discuss possible collaborations.
* Upgrade work Blackberry to Samsung 8, faster speeds, better UI, now able to tether and hotspot easier.
* Synergi Information Session
* Establish internal GCmobility governance with engineering team
* Attend SSC Employee Town Hall
* Connect with GCEntrepreneur Laura Portal (re: Innovative Staffing Article)
* introdcutons to the SSC Enterprise Architecture team (Daily Scrum)
* Learn about Chatbot pilot at SSC
* Receive Secure USB
* CFA training
*

## Iteration 5
* GCmobility Remote working Test #1: Charleston Lake
* Learn about presenting IT projects to SSC Enterprise Architecture
* Present GCmobility project to DG
* Follow up on GCCase for CFA program.
* Synergi Site Admin training
* Qualiware demo
* SSC Ideation Sessions
	* Mobility:  What is the future of mobile productivity in the workplace and how do we enable it?
	* Security/Privacy: How do we balance the security and privacy needs of Canadians with enabling Open Government?


## Iteration 6
* First GCMobility Community Meetup at CSPS
* Meet with FlexGC program
* Learn about a Hoe's Free Agent adventures in NZ
* SSC Ideation Sessions
	* Workplace of the future:  What will employees need to be productive, happy and healthy in the workplace?
	* Data Centres:  What computer or storage needs can we anticipate to support the future of Digital Operations?
* Complete Phase 4 of Manager Development Program.

## Iteration 7
* GCmobility Remote working Test #2: Algonquin Park
* GCmobility Remote working Test #3: Pancake Bay Provincial Park
* First GCMobility Open Office Hours @ GCCoworking L'asplanade
* GCCase for CFA follow up

## Iteration 8
* Draft Questionnaires
* second meetup with GCmobility
* Received CE Email Translation
* PHAC Code For Canada Project Panel and KNowledge transfer
* Connect with Amanda Bloom of XFN program
* Learn about data science at GAC with Ravendra Naidoo



Onboarding
* Network Account
* Laptop Assigned
* Security Clearance
* Synergi Site access
* Syngergi Site Creation
* Synergi Site training

* Present to DG
* Created GCCollab site
* Created Project Site

* Evolved MyKey Process Document
* Experimenting with Presentations.
* Draft SSC Client Executive E-Mail
* Send CE Email for Transdlation
* SSC Ideation sessions
*
