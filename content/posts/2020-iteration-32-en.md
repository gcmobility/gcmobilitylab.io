Title: GC Mobility Update Iteration 32
Date: 2020-06-18
Modified: 2020-06-18
Category: Blog
Tags: review, demos
Slug: gcmobility-iteration-review-32
Toc_Include_Title: false
Status: published
lang: en
Summary: Iteration 32 of GCmobility focused on kicking off the M365/Teams Proof of Concept (POC) for Mobile employees. The POC is leveraging the GCcollaboration platform. There was no iteration 31, due to taking some vacation.

## Summary
Iteration 32 of GCmobility focused on kicking off the M365/Teams Proof of Concept (POC) for Mobile employees. The POC is leveraging the GCcollaboration platform. This Proof of Concept was originally proposed on March 23, 2020 with a request to create a separate team for Canada's Free Agents on GCcollaboration.ca. The request went through many different groups with many different reccomendations, the POC was approved May 22, 2020.  There was no iteration 31, due to taking some vacation.  

* A primary objective of GCmobility is to decrease crossboarding time between departments.
* This pilot is the latest opportunity for Canada's Free Agents to participate in some mobility research. 
* This pilot is a unsupervised-remote-exploratory usability study using Microsoft 365. 
* The pilot will make use of [GCcollaboration.ca](http://gccollaboration.ca), the government of Canada's response to enabling federal public servants to remain connected and productive during the COVID-19 quarantine in support of remote and telework. 
* The initial study period will last for aproximately 4 weeks
* Users wishing to continue using the system and participating in furth user research may continue using the system after the initial pilot.
* GCcollaboration.ca as a system will be shutdown in September 2020, all data and information must be managed appropriateley.
* This system is only approved for unclassified and non protected infomration and data.
* As the pilot progresses, we will be adding to the list of Frequently Asked Questions
* The registration form was anticipated to take 5-10 minutes to complete.

## Overview
This post represents the update for Iteration 30 of the GCmobility project. There was no iteration 31, due to taking some vacation.

After reading this update please take some time to complete the [GCmobility Retrospective Questionnaire]({filename}/pages/iteration-feedback.md) after you have viewed the demo and/or checked out the resources on this page. The questionnaire asks three simple questions.

GCmobility has been experimenting with M365 since the roll out of [GCcollaboration](http://gccollaboration.ca) to employees at [NRCAN](https://www.nrcan.gc.ca/). We were excited to see a growing number of departments who decided to adopt this multi-departmental [M365](https://www.microsoft.com/en-us/microsoft-365) tenant. Currently we know of [19 departments using GCcollaboration](https://wiki.gccollab.ca/GCcollaboration_Department_Listing). The fact we had 19 departms sharing a M365 deployment meant we could test various Crossboarding and interdepartmental collaboration use cases. 

GCmobility is driven by the goals of increasing workforce mobility, decreasing crossboarding times, increasing the value proposition of hiring mobile employees, and increasing the autonomy of mobile works as well as their hiring managers and executives. The purpose of the **GC Mobility Proof of Concept: M365/Teams Pilot for Mobile employees** is to explore if functionality and usability of M365/Teams can help address pain points specific to hyper mobile employees.

While it made sense to experiment on GCcollalboration.ca as an established platform there were a number of considerations. One important consideration is that GCcollaboration.ca is planned to be decomissioned September 2020. THis meant our pilot had a distinct end point, however it also meant that any risks taken during the pilot would be mitigated by a defined end point. The first challenge for the pilot was to get it approved and roll out a sandbox MS Teams for experimenting. As of this writting we are in full pilot mode.

**Timeline for Proof of Concept**:
* Proof of Concept was proposed on March 23, 2020 
* POC was approved May 22, 2020
* The Canada's Free Agent Team was created on June 11, 2020
* initial 3 Team Champions (aka Owners) being added on June 11, 2020
* The first 15 test users were submitted on June 12, 2020
* onboarding welcome package sent to team champions June 15, 2020
* The second wave of 3 users was submitted on June 18, 2020

Since we also know of some departments who opted out of GCcollaboration or stood up their own paralel instance, this also provided an opportunity to test out the Business to Business integration between GCcollaboration and other M365 deployments. Based on various conversations concerning architectural patterns for rolling out M365 it became clear that while decision makers were expecting certain features for the user experinece, these vary widely depending on how they roll out their instance. So far the driving business requirements seem to be driven by intra-departmental user requirements and not intra-departmental requirements such as cross boarding. Therefore, we suspect that some roll outs of M365 across the GC may result in mobility anti-patterns. 

This pilot uses as an existing instance of MS365/Teams at the E3 Licence level. At one point there were discussions of rolling out an instance specific for the pilot, but the quickest rouote was to use existing infrastructure. Microsoft has some resources outlining the different [features and functions on intertennant collaboration](https://docs.microsoft.com/en-us/office365/enterprise/office-365-inter-tenant-collaboration), and the actual user experience may depend on how these tenants are configured. Here is an article outlining some [Pros and Cons of Single Tenant vs Multiple Tenants in Office 365](https://blogit.create.pt/miguelisidoro/2019/01/07/pros-and-cons-of-single-tenant-vs-multiple-tenants-in-office-365/). GCmobility wants to test the merrits of a single tenant, like GCcollaboration and explore how it may facilitate increasing mobility among mobile workers. In the end this pilot was launched by standing up a new team for Canada's Free Agents within the existing GCcollaboration.ca tennant.

There will be no demo video for Iteration 32. We will be sharing some findings and conclusions as the pilot moves on.

## Demonstration
This section higlights what we will cover in the Iteration Review and Demo. 

As an iteration focused on planning and 

## Site Updates: 
This section covers any major changes to the GCmobility site design, layout and content.

 We have now added the ability to add bilingual content. look out for the "Translations" heading pointing to alternative languages.

## Mobility Service Updates:

The service is evolving and currently has the following metrics

Service Issues by status:
* Mobility Service Issues Launched: 10
* Mobility Service Issues Open: 4
* Mobility Service Issues Closed: 6

Service Issues by Service Type:

* E-Mail: 3
* Data Management: 1
* Misc: 1
* MyKey: 5


[Learn more about the GCMobility initiative]({filename}/pages/about.md), a joint partnership between [Canada's Free Agents](https://www.linkedin.com/company/freeagents-agentslibres/about/) and [Shared Services Canada](https://www.canada.ca/en/shared-services.html).

If you are interested in joining us for future live demonstrations please [reach out to us]({filename}/pages/contact-us.md).

### Recent GCmobility Pivots:

* **Mobility Service**: Running an Alpha mobility service for mobile employees.
* **COVID-19**: How to work effectively "Off line"
* **Technology**: POC 2: 1 Phone, 1 Laptop/Tablet, 1 canonical e-mail)
* **Processes**: Move Focus from onboarding to cross-boarding
* **People**: Hyper Mobile Employees and Executives (rates > 30%)
* **Policies**: Enterprise Architecture Supporting Mobility
* **Services**: Crossboarding as an enterprise, interdepartmental, internal digital service

## Iteration Results


## Iteration Review
### Index
There is no video to index at this time.
<!--Check out the embeded Iteration Review video and presentation.

If you are tight on time then try these time stamped video chapters:-->

<!-- include timesstamped links to sections of video eg (https://youtu.be/U8Q5e5WCg8I?t=1m35s)-->

### Video

Demonstration video </br>

The Video will be posted after the demo has been recorded. We have not had a chance to record a demo for this iteration. Check back soon or subscribe to our [RSS](https://gcmobility.gitlab.io/feeds/all.rss.xml) or [ATOM](https://gcmobility.gitlab.io/feeds/all.atom.xml) feeds to receive real time updates on our site contnet.

<!--
<iframe width="560" height="315" src="https://www.youtube.com/embed/U8Q5e5WCg8I" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/1kZ7j-5MvFk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


-->

### Slide Deck
Demonstration Presentation </br>

The slide deck for this demo will be posted after it has been recorded. We have not had a chance to record a demo for this iteration. Check back soon or subscribe to our [RSS](https://gcmobility.gitlab.io/feeds/all.rss.xml) or [ATOM](https://gcmobility.gitlab.io/feeds/all.atom.xml) feeds to receive real time updates on our site contnet.

<!-- Go into slide deck, select file > publis to web > embed
Do not use share link

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vT0-21U3akusGQyYoutEan2XhEZ-bVaqRv7mRKCppHn5-f1lAunnyaFmj_C3K3T0A/embed?start=false&loop=false&delayms=3000" frameborder="0" width="640" height="389" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

-->

## News and Upcoming Events

**Iteration Schedule**

Iteration   |Start Date |Demo Date  |End Date
------------|-----------|-----------|-----------
33          | 6/22/2020 | 7/2/2020  | 7/3/2020
34          | 7/6/2020  | 7/16/2020 | 7/17/2020
35          | 7/20/2020 | 7/30/2020 | 7/31/2020
36          | 8/3/2020  | 8/13/2020 | 8/14/2020
37          | 8/17/2020 | 8/27/2020 | 8/28/2020
38          | 8/31/2020 | 9/10/2020 | 9/11/2020
39          | 9/14/2020 | 9/24/2020 | 9/25/2020
40          | 9/28/2020 | 10/8/2020 | 10/9/2020


# Additional Resources:

* Be Mobile with [GCmobility!](http://bit.ly/gcmobility-wiki)
* Soyez mobile avec [GCmobility!](http://bit.ly/gcmobility-wiki)

# Next Iteration
We will see you back here in 2 weeks for the next installment of our iteration reviews.

* Iteration 33 Planning will start June 22, 2020
* Next Demo/Update: July 2, 2020
