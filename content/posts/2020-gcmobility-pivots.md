Title: GC Mobility Pivots
Date: 2020-04-09
Modified: 2020-04-15
Category: Blog
Tags: review, demos
Slug: gcmobility-pivots
Toc_Include_Title: false
Status: published
Summary: GCmobility has been following many aspects of Lean Startup. At regular intervals a decision is made whether to Pivot or Perservere. This page tries to capture some of the history of these pivots.

## Summary
GCmobility has been following many aspects of Lean Startup. At regular intervals a decision is made whether to Pivot or Perservere. This page tries to capture some of the history of these pivots.

## Living The Pivot
A pivot is a "structured course correction designed to test a new fundamental hypothesis about the product, strategy, and engine of growth.“ (Wikipedia: Lean Startup#pivot)

Alternatively, Steve Blank defines a pivot as "changing (or even firing) the plan instead of the executive”

#### The Options
The traditional options when considering a pivot within a Lean Startup are simple "Pivot" or "Perservere". GCmobility has also been considering 2 other additions to the decision making. Two alternative complementary options include "Perish" and "Patch". These other options


**Perish**:

* Is it time to change the vision?
* Is your product still needed?
* Has a competiting product filled the space?
* Is it time to kill your project or product?

[Read More](https://appstretto.com/kill-early-and-often-startup-methodology/) about killing startups (projects) early and often

**Pivot**

* Is it time to change the plan to execute your vision?
* Have you learned something new rom your stakeholders to clarify what their need is?
* Is it time to redefine your product?
* Can the problem be solved a different way?

[Read More](https://www.startuplessonslearned.com/2009/06/pivot-dont-jump-to-new-vision.html) about Pivots in Lean Startup

**Patch**

* "The changes are too big to call it persevering but too small to call them a pivot"
* Is there a refinement in defining your stakeholders
* Has your project matured to a point that something needs to change (eg Governance)?

[Read more](https://medium.com/swlh/pivot-patch-or-persevere-i-patched-the-lean-startup-206d63023b9c) about patching in Lean Startup

**Perservere**:

* Is the Vision the same?
* Does the plan still work to achieve the vision?
* Can you continue to deliver

[Read More](https://www.startuplessonslearned.com/2009/06/pivot-dont-jump-to-new-vision.html) about Perserverance in Lean Startup

## Recent Pivots: COVID-19 Pivot

## Iteration 27

So we now have [empty offices with full houses](https://www.bloomberg.com/news/articles/2020-03-06/empty-offices-full-homes-covid-19-might-strain-the-internet), [60% increased newtork usage during the day in Canada](https://edmontonjournal.com/news/national/covid-19-canadian-telecom-companies-beefing-up-networks-as-usage-surges-from-remote-working/) and still the business of government to conduct. SSC has been working hard to [build a gateway to mobile workforce](https://www.canada.ca/en/shared-services/campaigns/stories/right-connections.html) on many fronts especialy VPN and GCCollaboration.

* **COVID-19**: How to work effectively "Off line"

Read more about [our COVID-19 pivot]({filename}/posts/2020-covid-19-pivot.md)

### Iteration 25

The increased spread of COVID-19 has resulted in Federal Public Servants being sent home.

* "Thousands of federal employees to receive paid vacation since they can't work from home" (source:[Nationalpost.com](https://nationalpost.com/news/canada/thousands-of-federal-employees-to-receive-paid-vacation-since-they-cant-work-from-home))


### Iteration 23
During [Iteration 23]({filename}/posts/2020-Iteration-23.md), we took the opportunity to revisit the Government of Canada Digital Standards we first explored in Iteration 1.

* We developed and performed GCmobility Self Assessments against:
  * [Canada's Digital Standards](https://youtu.be/U8Q5e5WCg8I?t=899)
  * [Canadian Digital Service](https://youtu.be/U8Q5e5WCg8I?t=1077)

### Iteration 21
This was the iteration when Canada Saw it's first COVID-19 Case. We did not know it at the time, however it would cause a shift in the GCmobility efforts by Iteration 25, and cause a full pivot by Iteration 27.

* "The date of symptom onset for the first case of COVID-19 reported in Canada became ill on January 15, 2020" (source:[Canada.ca](https://www.canada.ca/en/public-health/services/diseases/2019-novel-coronavirus-infection/health-professionals/epidemiological-summary-covid-19-cases.html))


### Iteration 20
While reviewing much of the Stakeholder input and feedback during Iteration 20  we had received since the begining, we started to think that the intersection between People, Policies, Processes and Technology could actually be Services. We are now exploring the concept of Crossboarding as a digital service.

* **Services**: Crossboarding as an enterprise, interdepartmental, internal digital service

### Iteration 11
We knew early on that technology alone was not going to solve GC's mobility issues. We had to pivot away from technology and towards people, Policies, Processes and Services.

* **Technology**: POC 2: 1 Phone, 1 Laptop/Tablet, 1 canonical e-mail)
* **Processes**: Move Focus from onboarding to cross-boarding
* **People**: Hyper Mobile Employees and Executives (rates > 30%)
* **Policies**: Enterprise Architecture Supporting Mobility


### Iteration 2-10
The intial phases of GCmobility focused in part at trying to find a "technology solution" for mobility. Se we looked into.

* Virtual work environments through Virtual machines
* SSC's Common Desktop Operating Environment (CDOE)
* Common mobile computers to run Virtual machines based on CDOE

### Iteration 1
The Government of Canada released the [Government of Canada Digital Standards](https://www.canada.ca/en/government/system/digital-government/government-canada-digital-standards.html) at the end of the 2018-2019 fiscal year end. Many of the standards already resonated with with the team but we were not sure how they would fit in yet. The standards included:

* Design with users
* Iterate and improve frequently
* Work in the open by default
* Use open standards and solutions
* Address security and privacy risks
* Build in accessibility from the start
* Empower staff to deliver better services
* Be good data stewards
* Design ethical services
* Collaborate widely

# Additional Resources:

* Be Mobile with [GCmobility!](http://bit.ly/gcmobility-wiki)
* Soyez mobile avec [GCmobility!](http://bit.ly/gcmobility-wiki)

# Next Iteration
We will see you back here in 2 weeks for the next installment of our iteration reviews.
