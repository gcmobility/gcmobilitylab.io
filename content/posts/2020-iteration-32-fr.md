Title: GC Mobility Update Iteration 32
Date: 2020-06-18
Modified: 2020-06-18
Category: Blog
Tags: review, demos
Slug: gcmobility-iteration-review-32
Toc_Include_Title: false
Status: published
lang: fr
Summary: L'itération 32 de GCmobilité s'est concentrée sur le lancement de la preuve de concept (POC) des équipes M365/Teams pour les employés mobiles. La POC s'appuie sur la plateforme de collaboration GC. Il n'y a pas eu d'itération 31, en raison de vacances.

## Résumé
L'itération 32 de GCmobilité s'est concentrée sur le lancement de la Preuve de Concept (POC) des équipes M365 pour les employés mobiles. La POC s'appuie sur la plateforme de collaboration GC. Cette preuve de concept a été initialement proposée le 23 mars 2020 avec une demande de création d'une équipe distincte pour les agents libres du Canada sur GCcollaboration.ca. La demande est passée par de nombreux groupes différents avec de nombreuses recommandations différentes, le POC a été approuvé le 22 mai 2020.  Il n'y a pas eu d'itération 31, en raison de vacances.  

* Un des objectifs principaux de GCmobility est de réduire le temps de passage entre les ministères.
* Ce projet pilote est la dernière occasion pour les agents libres du Canada de participer à des recherches sur la mobilité. 
* Ce projet pilote est une étude exploratoire non supervisée sur la facilité d'utilisation à distance de Microsoft 365. 
* Le projet pilote utilisera [GCcollaboration.ca] (http://gccollaboration.ca), la réponse du gouvernement du Canada pour permettre aux fonctionnaires fédéraux de rester connectés et productifs pendant la quarantaine COVID-19 pour soutenir le travail à distance et le télétravail. 
* La période d'étude initiale durera environ 4 semaines
* Les utilisateurs qui souhaitent continuer à utiliser le système et participer à des recherches sur les utilisateurs ultérieurs peuvent continuer à utiliser le système après le projet pilote initial.
* Le système GCcollaboration.ca sera fermé en septembre 2020, toutes les données et informations doivent être gérées de manière appropriée.
* Ce système n'est approuvé que pour les informations et les données non classifiées et non protégées.
* Au fur et à mesure de l'avancement du projet pilote, nous ajouterons à la liste des questions les plus fréquemment posées
* Le formulaire d'inscription devait prendre de 5 à 10 minutes à remplir.

## Vue d'ensemble
Ce post représente la mise à jour pour l'Itération 30 du projet GCmobilité. Il n'y a pas eu d'itération 31, en raison de vacances.

Après avoir lu cette mise à jour, veuillez prendre le temps de remplir le [GCmobility Retrospective Questionnaire]({filename}/pages/iteration-feedback.md) après avoir visionné la démo et/ou consulté les ressources de cette page. Le questionnaire pose trois questions simples.

GCmobility expérimente le M365 depuis le déploiement de [GCcollaboration](http://gccollaboration.ca) auprès des employés de [NRCAN](https://www.nrcan.gc.ca/). Nous avons été ravis de constater qu'un nombre croissant de ministères ont décidé d'adopter ce locataire multiservices [M365](https://www.microsoft.com/en-us/microsoft-365). Actuellement, nous savons que [19 ministères utilisent GCcollaboration] (https://wiki.gccollab.ca/GCcollaboration_Department_Listing). Le fait que nous ayons 19 départs partageant un déploiement M365 nous a permis de tester différents cas d'utilisation de la Crossboarding et de la collaboration interministérielle. 

La GCmobilité est motivée par les objectifs d'accroître la mobilité de la main-d'œuvre, de réduire les délais d'embarquement, d'augmenter la proposition de valeur de l'embauche d'employés mobiles et d'accroître l'autonomie des travailleurs mobiles ainsi que de leurs gestionnaires et cadres de recrutement. L'objectif de la **Preuve de concept de la mobilité GC : M365/Teams Pilot for Mobile employees** est d'explorer si la fonctionnalité et la convivialité de M365/Teams peuvent aider à résoudre les problèmes spécifiques aux employés hyper mobiles.

Bien qu'il soit logique d'expérimenter sur GCcollalboration.ca en tant que plateforme établie, il y a eu un certain nombre de considérations. Une considération importante est que GCcollaboration.ca est prévu pour être mis hors service en septembre 2020. Cela signifie que notre projet pilote a un point final distinct, mais cela signifie également que tout risque pris pendant le projet pilote sera atténué par un point final défini. Le premier défi pour le pilote a été de le faire approuver et de mettre en place une équipe MS de bac à sable pour l'expérimentation. Au moment où nous écrivons ces lignes, nous sommes en mode pilote complet.

**Calendrier pour la preuve de concept** :
* La preuve de concept a été proposée le 23 mars 2020 
* Le POC a été approuvé le 22 mai 2020
* L'équipe d'agents libres du Canada a été créée le 11 juin 2020
* les 3 premiers champions d'équipe (alias propriétaires) seront ajoutés le 11 juin 2020
* Les 15 premiers utilisateurs test ont été soumis le 12 juin 2020
* envoi d'un dossier de bienvenue aux champions d'équipe le 15 juin 2020
* La deuxième vague de 3 utilisateurs a été soumise le 18 juin 2020

Comme nous connaissons également certains ministères qui ont choisi de ne pas participer à la GCcollaboration ou qui ont créé leur propre instance parallèle, cela a également permis de tester l'intégration interentreprises entre la GCcollaboration et d'autres déploiements M365. Sur la base de diverses conversations concernant les modèles architecturaux pour le déploiement du M365, il est apparu clairement que si les décideurs attendaient certaines fonctionnalités pour l'expérience utilisateur, celles-ci varient largement en fonction de la manière dont ils déploient leur instance. Jusqu'à présent, les exigences commerciales semblent être dictées par les besoins des utilisateurs au sein des services et non par des exigences intra-services telles que la transversalité. Par conséquent, nous soupçonnons que certains déploiements de M365 dans l'ensemble du GC peuvent entraîner des anti-modèles de mobilité. 

Ce pilote utilise comme exemple existant des équipes MS365 au niveau de la licence E3. À un moment donné, il a été question de déployer une instance spécifique pour le pilote, mais la solution la plus rapide consistait à utiliser l'infrastructure existante. Microsoft dispose de certaines ressources décrivant les différentes [caractéristiques et fonctions de la collaboration interlocutoire] (https://docs.microsoft.com/en-us/office365/enterprise/office-365-inter-tenant-collaboration), et l'expérience réelle des utilisateurs peut dépendre de la configuration de ces locataires. Voici un article décrivant certains [avantages et inconvénients de la collaboration entre un seul locataire et plusieurs locataires dans Office 365] (https://blogit.create.pt/miguelisidoro/2019/01/07/pros-and-cons-of-single-tenant-vs-multiple-tenants-in-office-365/). GCmobility veut tester les mérites d'un locataire unique, comme GCcollaboration, et explorer comment il peut faciliter la mobilité croissante des travailleurs mobiles. Ce projet pilote a finalement été lancé en mettant sur pied une nouvelle équipe pour les agents libres du Canada au sein du locataire existant GCcollaboration.ca.

Il n'y aura pas de vidéo de démonstration pour l'itération 32. Nous partagerons certains résultats et conclusions au fur et à mesure que le projet pilote avancera.

## Démonstration
Cette section met en évidence ce que nous allons couvrir dans le bilan et la démonstration des itérations. 

En tant qu'itération axée sur la planification et la 

## Mises à jour du site : 
Cette section couvre tous les changements majeurs apportés à la conception, à la présentation et au contenu du site GCmobility.

 Nous avons maintenant ajouté la possibilité d'ajouter du contenu bilingue. Attention à la rubrique "Traductions" qui indique les langues alternatives.

## Mises à jour du service de mobilité :

Le service évolue et dispose actuellement des paramètres suivants

Questions de service par statut :
* Lancement des questions de service de mobilité : 10
* Questions ouvertes sur les services de mobilité : 4
* Questions relatives aux services de mobilité closes : 6

Problèmes de service par type de service :

* E-Mail : 3
* Gestion des données : 1
* Divers : 1
* Ma Clé : 5


[En savoir plus sur l'initiative GCMobility]({nom du fichier}/pages/about.md), un partenariat conjoint entre [Canada's Free Agents](https://www.linkedin.com/company/freeagents-agentslibres/about/) et [Shared Services Canada](https://www.canada.ca/en/shared-services.html).

Si vous souhaitez vous joindre à nous pour de futures démonstrations en direct, veuillez [nous contacter] ({nom du fichier}/pages/contact-us.md).

### Pivots récents de GCmobilité :

* **Service de mobilité** : Gestion d'un service de mobilité Alpha pour les employés mobiles.
* **COVID-19** : Comment travailler efficacement "hors ligne" ?
* **Technologie** : POC 2 : 1 téléphone, 1 ordinateur portable/tableau, 1 courriel canonique)
* **Processus** : Passer de l'embarquement à l'embarquement croisé
* **People** : Employés et cadres hyper mobiles (taux > 30%)
* **Politiques** : L'architecture d'entreprise au service de la mobilité
* **Services** : Crossboarding en tant qu'entreprise, interservices, service numérique interne




## Bilan des itérations
### Index
Il n'y a pas de vidéo à indexer pour le moment.
<!--Contrôlez la vidéo et la présentation de l'examen des itérations.

Si vous êtes pressé par le temps, essayez ces chapitres vidéo horodatés:-->

<!-- inclure des liens horodatés vers des sections de la vidéo, par exemple (https://youtu.be/U8Q5e5WCg8I?t=1m35s)-->

### Vidéo

Vidéo de démonstration </br>

La vidéo sera publiée après l'enregistrement de la démo. Nous n'avons pas eu la possibilité d'enregistrer une démo pour cette itération. Revenez nous voir bientôt ou abonnez-vous à nos flux [RSS](https://gcmobility.gitlab.io/feeds/all.rss.xml) ou [ATOM](https://gcmobility.gitlab.io/feeds/all.atom.xml) pour recevoir les mises à jour en temps réel sur le site contnet.

<!--
<iframe width="560" height="315" src="https://www.youtube.com/embed/U8Q5e5WCg8I" frameborder="0" allow="accelerometer ; autoplay ; encrypted-media ; gyroscope ; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/1kZ7j-5MvFk" frameborder="0" allow="accelerometer ; autoplay ; encrypted-media ; gyroscope ; picture-in-picture" allowfullscreen></iframe>


-->

### Diaporama
Présentation de la démonstration </br>

Le jeu de diapositives de cette démo sera mis en ligne après son enregistrement. Nous n'avons pas eu l'occasion d'enregistrer une démo pour cette itération. Revenez nous voir bientôt ou abonnez-vous à nos flux [RSS](https://gcmobility.gitlab.io/feeds/all.rss.xml) ou [ATOM](https://gcmobility.gitlab.io/feeds/all.atom.xml) pour recevoir les mises à jour en temps réel sur le site contnet.

<!-- Allez dans le diaporama, sélectionnez le fichier > publis to web > embed
Ne pas utiliser le lien de partage

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vT0-21U3akusGQyYoutEan2XhEZ-bVaqRv7mRKCppHn5-f1lAunnyaFmj_C3K3T0A/embed?start=false&loop=false&delayms=3000" frameborder="0" width="640" height="389" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

-->

## Nouvelles et événements à venir

**Calendrier d'introduction**

Itération |Date de début |Date de démonstration |Date de fin
------------|-----------|-----------|-----------
33 | 6/22/2020 | 7/2/2020 | 7/3/2020
34 | 7/6/2020 | 7/16/2020 | 7/17/2020
35 | 7/20/2020 | 7/30/2020 | 7/31/2020
36 | 8/3/2020 | 8/13/2020 | 8/14/2020
37 | 8/17/2020 | 8/27/2020 | 8/28/2020
38 | 8/31/2020 | 9/10/2020 | 9/11/2020
39 | 9/14/2020 | 9/24/2020 | 9/25/2020
40 | 9/28/2020 | 10/8/2020 | 10/9/2020


# Ressources supplémentaires :

* Soyez mobile avec [GCmobilité !] (http://bit.ly/gcmobility-wiki)
* Soyez mobile avec [GCmobility !] (http://bit.ly/gcmobility-wiki)

# Itération suivante
Nous vous retrouverons ici dans deux semaines pour le prochain volet de nos révisions d'itération.

* La planification de l'itération 33 commencera le 22 juin 2020
* Prochaine démo/mise à jour : 2 juillet 2020
