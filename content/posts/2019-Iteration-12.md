Title: GC Mobility Update Iteration 12
Date: 2019-09-12
Modified: 2020-02-27
Category: Blog
Tags: gcmobility, demos, iteration-12
Slug: gcmobility-iteration-review-12
Toc_Include_Title: false
Status: published
Summary: Iteration 12 of GCmobility focused on stakeholder engagement. September to November presents projects wanting to engage with stakeholders a 3 month window to make some great progress.

*UPDATE*: This content was originally published in the [GCmobility group within GCcollab](https://gccollab.ca/groups/profile/2183003/engcmobilityfrgcmobilitu00e9). "Hosted by the Government of Canada, GCcollab is designed to connect you with the information and people you need to work with across Canadian universities, colleges and all levels of government." ([GCcollab Terms and Conditions](https://gccollab.ca/terms)). Some links may lead to internal resources.

## Overview
This post represents the update for Iteration 12 of the GCmobility project.

The [GCMobility initiative](https://gccollab.ca/groups/profile/2183003/engcmobilityfrgcmobilitu00e9) is a joint partnership between Canada's Free Agents and Shared Services Canada

**Iteration 11 was skipped, due to annual leave**. I find that taking leave from work is like taking a break from a hard problem to come back and look at it from a fresh vantage point. Spending 2 Weeks on the north shore of PEI and the remainder of my time with family camping in Sandbanks Provincial Park are 2 great ways for me to break from the routine and return to the issue of mobility with a new perspective.

**Iteration 12 was planned out well before the start of this iteration**. For a while we knew that iteration 12 was going to focus on stakeholder engagement. September to November presents projects wanting to engage with stakeholders a 3 month window to make some great progress. Folks are usually back from vacation and Winter break is but a distant thought. However, all other projects know this too, so time is at a premium. I appreciate all those stakeholders who have agreed to meet and discuss mobility to date, and I look forward to those we have yet to engage.

Currently our focus is on stakeholder engagement. We are working to connect with various HR mobility projects to learn about their pain points. As we present in our introductory slide deck, there are many enablers to mobility and **each enabler currently faces challenges to enable mobility**.

The GCcollab community here certainly grew over the last couple of weeks. We jumped pretty quickly for 30 to 85 members at the start of iteration 12 and **now have a membership of 90**. This tells us that mobility is an issue that have a growing stakeholder base.

**Conversations related to mobility** continue in the [GCmobility discussion forums](https://gccollab.ca/discussion/owner/2183003). [Susan Lehamm](https://gccollab.ca/profile/Susan.Lehmann) shares some of the challenges of delayed security checks and raises the question "So what is the deal with having wifi in buildings but not letting anyone but the building occupants on?" in [WIFI in GC Buildings](https://gccollab.ca/discussion/view/2849693/enwifi-in-gc-buildingsfr). In the discussion [Cow In a Barn](https://gccollab.ca/discussion/view/2796458/encow-in-a-barn-perceptions-about-mobility-drivers-ottawa-citizen-articlefrvache-dans-une-u00e9table-perceptions-u00e0-propos-de-la-mobilitu00e9-conducteurs-article-du-ottawa-citizen), [Heather Laird](https://gccollab.ca/profile/Heather.Laird) was "curious to see a perspective that looks at mobility as crunching people into offices, without mentioning that people can work from home.".

**GCmobility wants to hear your perspectives** on these threads and encourage you to explore other topics of interest. These discussion topics will help to direct GCmobility towards issues and solutions of interest to the stakeholders of mobility, you!

During iteration 12 we held the latest instalment of the **GC Mobility Leaders Meetup** where we welcomed some folks from OCHRO into the mix. [Heather Laird](https://gccollab.ca/profile/Heather.Laird) volunteered to host this meeting and encouraged us all to attend virtually to experience what regional workers often experience. The discussion topic was around what tools people use to support virtual meetings and remote teams.

**Next month, GC Mobility Leaders Meetup will be hosted by** [Russel Prouse](https://gccollab.ca/profile/russelprouse) and the Accessibility, Accommodation and Adaptive Computer Technology (AAACT) Program at SSC onsite at their accessibility lab. The first part of the meetup will be a presentation on what AAACT does, followed by Q&A as well as our regular meetup agenda. The onsite visit will also include a tour of their accessibility lab.

If you are interested to **join the GC Mobility Community Leaders meetup**, or would like to host a monthly meetup please reach out to [David Sampson](mailto:david.sampson@canada.ca?subject=Please%20Invite%20me%20to%20the%20GC%20Mobility%20Leaders%20Monthly%20Meetup)

**Iteration Feedback:**
As part of Agile Scrum practices we usually hold an iteration retrospective to gather feedback on project progress. In the spirit of Agilility, mobility and virtual tools we would encourage you to post a comment to this blog post answering one or all of the following questions concerning this iteration:
* What went well, that we should continue doing?
* What did not work, that we should improve or remove?
* What news idea do you have, that we should start doing?

**With your help we have grown this group to 90 members**. Lets continue to connect with the right stakeholders in the mobility space. Here are some new way you can help:
* Join our [Monthly Mobility Leaders Meetups](mailto:david.sampson@canada.ca?subject=Please%20Invite%20me%20to%20the%20GC%20Mobility%20Leaders%20Monthly%20Meetup)
* Click an action for this blog post below
  * Comment On this Post
  * Share this Post
  * Like this Post

  Here are [some other ways you can help](https://gccollab.ca/discussion/view/2924125/enhelp-gcmobility-spread-the-wordfr)

Iteration 13 will start up next week.

We will see you back here for Iteration 12.



## Iteration Goals
### To Do list
This iteration we set out to tackle these issues:
* Stakeholder Engagement

### Completed
Throughout this iteration we worked on and completed these issues:
* See Scrum Questions below

### Deferred
Not all issues are able to be completed in each iteration. These ones have been deferred:
* Various

## Iteration Scrum Questions
### What have we been working on
* **Stakeholder Engagement**
  * Monthly Meetup for GC Mobility Leaders Community #4: Virtual Meetings
  * Planning for discussion with Free Agent Steering Committee engagement next iteration.
  * Met with lead of SSC NSDS Digital Strategy to learn how mobility might fit into the strategy.
  * Held interviews with remaining Free Agent Talent managers to explore mobility issues and key pain points.
  * Met with GCcoworking and GCworkplace leadership to see opportunities for collaboration and partnership.
  * Collaborated on GCmobility Enterprise Architecture with fellow Enterprise Architects.
  * Exploring options for building out a team to support the GCmobility initiative (stay tuned)
  * Met with the SSC group responsible for Active directory which stores employee login credentials and identities to learn how the future might impact or enable mobility at the technology layer (more conversations to come)
  * Trying to develop multiple "value propositions" for various stakeholders groups (eg Employee, Managers, Business)

* **Project management**
  * [Posted a recording of an earlier draft presentation](https://gccollab.ca/file/view/2897473/engcmobility-long-form-presentation-draft-recording-long-37min-uneditedfr). At 37 min it captures that full context of what we are trying to do within GCmobility. Other trimmed down versions are pending as we work with exported Webex videos.

* **IT Issues**
  * 1 of 2 virtual machines for demonstration are working (SSC). Need some time dedicated to testing.
  * PSPC image still has issues. need to work with service desk. Will wait until after upcoming leave.
  * Confirmed that new e-mail system will move away from @canada.ca. Mobility use case is a known issue that has not been designed for at this time.

* **Architecture**
  * Published first version of our ["Enterprise Architecture for Mobility"](https://gccollab.ca/file/view/2757952/enssc-gcmobility-architecturepdffr)

* **Communications**
  * Collaborated on some more work on the GCmobility Stakeholder Engagement and Communication Plan

### What will We work on
* **Stakeholder Engagement**
  * Monthly Meetup for GC Mobility Leaders Community #5: Accessible Technology
  * Engaging with Free Agent Steering Committee in September
  * Engaging with SSC Client Executives
  * Engaging with Free Agent Support Team
* **Communications**
  * Seeking feedback on the GCmobility deck
* **Architecture**
  * Seek feedback on business model canvas to describe GCmobility business model

### What blocks are we facing
* Stakeholder engagement
  * Mobility is a complex problem with lots of people and inter related projects
  * Currently GCmobility has a "yellow" light for full stakeholder engagement. Management wants to know the who/how/what. So we are co-designing this piece with our community, you!(stay tuned)

### New Issues added to the backlog
* Some departments rolling out URL scrubbing for E-mails. This might make collaboration more challenging
* GCmobility needs to develop value propositions for more stakeholder groups to get buy-in

### Interesting things we are learning
* Security: Security is appearing as a primary pain point for talent managers, hiring managers, Free Agents as well as HR practitioners. We are not sure what our investigations will turn up but we have engaged TBS Security Policy team in September.
* AAACT adopts the concept of "accessibility by design" perhaps mobility should be treated the same
* User Centred Design: There are many initiatives that can support and may impact mobility. Not all of these initiatives are applying User Centred Design principles. UCD takes time and energy. GCmobility will continue to keep users at the centre of its process as best as we (aka me) can.


## Agile Metrics
This section is for those hooked on metrics. Let us know what metrics are important to you.
* Iterations: 12
* Weeks: 24
* Total Issues: [90](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/issues?scope=all&utf8=%E2%9C%93&state=all)
  * Open Issues: [84](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/issues?scope=all&utf8=%E2%9C%93&state=opened)
  * Total Closed Issues: [6](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/issues?scope=all&utf8=%E2%9C%93&state=closed)
* GCMobility on GCcollab: [90 Members](https://gccollab.ca/groups/members/2183003)
* Burn Down rate: UNKNOWN (need to estimate and calculate efforts for User Story Points)
* Current Project end Date: September 27, 2019
* Proposed Project End date: March 2024


## GCmobility Resources
*	[Short Deck Introducing GCmobility](https://gccollab.ca/file/view/2696019/engcmobility-introduction-dg-approved-sharedpptxfr)
*	[GCmobility Community on GCcollab](https://gccollab.ca/groups/profile/2183003/engcmobilityfrgcmobilitu00e9)
* ["Enterprise Architecture for Mobility"](https://gccollab.ca/file/view/2757952/enssc-gcmobility-architecturepdffr)
*	[GCmobility Project Site](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility)
  * [Submit a new Mobility Issue](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=)
  * [Our KanBan Board](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/-/boards)
  * [Full Issue Backlog](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/issues)
