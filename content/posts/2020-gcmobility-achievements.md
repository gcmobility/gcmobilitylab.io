Title: Top 10 Achievements for GCmobility 2019-2020
Date: 2020-02-20
Modified: 2020-02-20
Category: Blog
Tags: gcmobility, iteration 24
Slug: gcmobility-2020-achievements
Status: published

GCmobility has been hosted this year by Shared Services Canada (SSC), Within Networks, Security and Digital Services (NSDS) Branch and part of the Workplace Technology Services - Service Evolution (WTSE) directorate. As part of our the annual review and rha-rha process, each team has been asked to highlight its achievements for the year. This blog post is an attempt to highlight what we think are some of the projects highlights.

## Achievements
1. We secured the support of the ADM of SSC NSDS to support the first phase of GCmobility
2. The GCmobility project got off the ground and we started engaging stakeholders
3. We mapped out the cross boarding journey for hyper mobile employees who belong to multiple departments at the same time, using Canada's Free Agents as the test case user population. We identified 28 categories of processes that often occur during the cross boarding journey. After asking users what thei biggest pain points and biggest successes were, we identified the top 15 pain points that need addressing. We also asked users what the minimum and maximum time the steps throughout this journey sometimes take.
4. From the jounrey mapping we learned that cross boarding takes a long time and can cost a lot of money and creates high level of disatisfaction and discomfort in users. The best case scenario we measured based on adding up the minimum times for each step was a process that took over 90 days at a cost of $55k/user.
5. We were able to connect with many stakeholders that have an interest or role in mobility in government. Our stakeholder list grew for a handful of potential partners to over 300 potential collaborators. Our prensence on GCcollab grow to over 180 members, and close to 40 individuals wanted to be involved with monthly face to face or virtual meetups to discuss mobility and the various projects trying to address the issues.
6. We ran a Proof on Concept #1 using virtual machine technology and learned alot about usability and and user expectations. Our conclusion is that making the problem "virtually" disapear will not move us forward enough, but addressing the people, policies and processes may deliver a more sustainable solution.
7. We supported a number of Free Agents navigate the maze of processes as they crossboarded between departments. The main issues that repeated came up included MyKey, E-Mail, Phone and Security related issues. These mapped nicely to the painpoint journey heat map.
8. The Standard Operating Procedure (SOP) to manage MyKey's for Free Agents was created by SSC in 2017. This year we had an opportunity to validate the SOP and further test its effectiveness. We also learned that at least one department has included the SOP in the training of their service desk members, while other departments have not. We found less issues from the department who had incorporated the SOP into their training compared to those who haven't.
9. We learned how the Canadian Digital Service (CDS) treats their onboarding process and decided to test it out on a small scale (see #10). We appreciated their approach of recognizing that two attract top talent to their program, especialy
10. We were able to test out the basic premis of using a consierge type service to support mobile employees such as Free Agents.

To learn more about what we have learned this year check out the following deck "GCmobility - What We Learned" (DRAFT).


Now it is your chance to let us know what you cinsider to be the top achievements for GCMobility over the past fiscal year. Link to Google Form survey.
