Title: GC Mobility Update Iteration 29
Date: 2020-05-07
Modified: 2020-05-21
Category: Blog
Tags: review, demos
Slug: gcmobility-iteration-review-29
Toc_Include_Title: false
Status: published
Summary: Iteration 29 of GCmobility focused on setting up an alpha mobility service as well as exploring the opportunity to stand up a collaborative platform for mobile workers. Our current focus is to ensure that the Free Agent family is well supported as the challenges with crossboarding between departments are magnified during COVID-19.

## Summary
Iteration 29 of GCmobility focused on continuing to set up an alpha mobility service as well as exploring the opportunity to stand up a collaborative platform for mobile workers. Our current focus is to ensure that the Free Agent family is well supported as the challenges with crossboarding between departments are magnified during COVID-19.

## Overview
This post represents the update for Iteration 29 of the GCmobility project.

After reading this update please take some time to complete the [GCmobility Retrospective Questionnaire]({filename}/pages/iteration-feedback.md) after you have viewed the demo and/or checked out the resources on this page. The questionnaire asks three simple questions.

The demo for Iteration 30 has been recorded, however we are running into some video editing issues. One challenge has been that since moving off the GC network and onto a personal device some of our tools have changed. One tool impacted has been [WebEx](https://www.webex.com/) which has limited support for operating systems like [Ubuntu Linux](https://ubuntu.com/), and recently has demonstrated some performance issues, especialy with call backs and in sessions recordings. So we went hunting for an alternative. 

As such our approach to recording demos needed to change. We ran the demo using [Jitsi](http://meet.jit.si) and tried out the recording feature which resulted in an OK recording. We also recorded using [Simple Screen Recorder](https://www.maartenbaert.be/simplescreenrecorder/), which was very high quality. The issue currently is that our video editing software [Open Shot](https://www.openshot.org/) is crashing on export. Only a small of amount of editing is required, however it is concerning protecting privacy so we will continue trying it out. OUr lesson learned is that currently we need to record the whole demo with the expectation of not editing.

Stay Tuned.

[Learn more about the GCMobility initiative]({filename}/pages/about.md), a joint partnership between [Canada's Free Agents](https://www.linkedin.com/company/freeagents-agentslibres/about/) and [Shared Services Canada](https://www.canada.ca/en/shared-services.html).

If you are interested in joining us for future live demonstrations please [reach out to us]({filename}/pages/contact-us.md).

## Demonstration
This section higlights what we will cover in the Iteration Review and Demo.

This iterations demo covers off the following areas:
* Changes to GCmobility as we learn to work offline from the GC network
* Mobility Service: Front Office User Experience (Service clients) 
* Mobility Service: Back Office User Experience (Service support)

## Site Updates: 
This section covers any major changes to the GCmobility site design, layout and content.

* [ ] We moved the [Pivot updates]({filename}/posts/2020-gcmobility-pivots.md) section to its own page

## Mobility Service Updates:

The service is evolving and currently has the following components:

### Front Office Service 

* **NEW!** [Online service request form]({filename}/pages/service-request.md)
* GCmobility [Consierge Service]({filename}/pages/service-about.md#consierge-service)
* GCmobility [Resource Library]({filename}/pages/service-about.md#resource-library)
    * **NEW!** Crossboarding resources
    * **NEW!** Public release of the [GCmobility Manual]({filename}/pages/library-mobility-manual.md)
* GCmobility [Community support]({filename}/pages/service-about.md#community-support)
* GCmobility [Discussion on GCmessage]({filename}/pages/service-about.md#discussion-on-gcmessage)
* **NEW!** GCmobility [Mailing List]({filename}/pages/service-about.md#mailing-list) using Mailchimp
* GCmobility [News Feeds]({filename}/pages/service-about.md#community-support)
* GCmobility on [Twitter]({filename}/pages/service-about.md#twitter)

### Back Office Service 
* **NEW!** [Online service request form]({filename}/pages/service-request.md)
    * Launches Local e-mail Client no server side code 
* Gmail as an e-mail proxy to conceal service desk operational password 
    *  Forward rules


### Recent GCmobility Pivots:

* **Mobility Service**: Running an Alpha mobility service for mobile employees.
* **COVID-19**: How to work effectively "Off line"
* **Technology**: POC 2: 1 Phone, 1 Laptop/Tablet, 1 canonical e-mail)
* **Processes**: Move Focus from onboarding to cross-boarding
* **People**: Hyper Mobile Employees and Executives (rates > 30%)
* **Policies**: Enterprise Architecture Supporting Mobility
* **Services**: Crossboarding as an enterprise, interdepartmental, internal digital service




## Iteration Review
### Index
Check out the embeded Iteration Review video and presentation.

If you are tight on time then try these time stamped video chapters:

<!-- include timesstamped links to sections of video eg (https://youtu.be/U8Q5e5WCg8I?t=1m35s)-->

### Video

Demonstration video </br>

The Video will be posted after the demo has been recorded and edited. We have not had a chance to record a demo for this iteration. Check back soon or subscribe to our [RSS](https://gcmobility.gitlab.io/feeds/all.rss.xml) or [ATOM](https://gcmobility.gitlab.io/feeds/all.atom.xml) feeds to receive real time updates on our site contnet.

<!--
<iframe width="560" height="315" src="https://www.youtube.com/embed/U8Q5e5WCg8I" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/1kZ7j-5MvFk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


-->

### Slide Deck
Demonstration Presentation </br>

The slide deck for this demo will be posted after it has been recorded. We have not had a chance to record a demo for this iteration. Check back soon or subscribe to our [RSS](https://gcmobility.gitlab.io/feeds/all.rss.xml) or [ATOM](https://gcmobility.gitlab.io/feeds/all.atom.xml) feeds to receive real time updates on our site contnet.

<!-- Go into slide deck, select file > publis to web > embed
Do not use share link

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vT0-21U3akusGQyYoutEan2XhEZ-bVaqRv7mRKCppHn5-f1lAunnyaFmj_C3K3T0A/embed?start=false&loop=false&delayms=3000" frameborder="0" width="640" height="389" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

-->

## News and Upcoming Events

**Iteration Schedule**

Iteration   |Start Date |Demo Date  |End Date
------------|-----------|-----------|-----------
30          | 5/11/2020 | 5/21/2020 | 5/22/2020
31          | 5/25/2020 | 6/4/2020  | 6/5/2020
32          | 6/8/2020  | 6/18/2020 | 6/19/2020
33          | 6/22/2020 | 7/2/2020  | 7/3/2020
34          | 7/6/2020  | 7/16/2020 | 7/17/2020
35          | 7/20/2020 | 7/30/2020 | 7/31/2020
36          | 8/3/2020  | 8/13/2020 | 8/14/2020
37          | 8/17/2020 | 8/27/2020 | 8/28/2020
38          | 8/31/2020 | 9/10/2020 | 9/11/2020
39          | 9/14/2020 | 9/24/2020 | 9/25/2020
40          | 9/28/2020 | 10/8/2020 | 10/9/2020

**GC Mobility Community Meetups**

Schedule for 2020-2021: **ON HOLD**

# Additional Resources:

* Be Mobile with [GCmobility!](http://bit.ly/gcmobility-wiki)
* Soyez mobile avec [GCmobility!](http://bit.ly/gcmobility-wiki)

# Next Iteration
We will see you back here in 2 weeks for the next installment of our iteration reviews.

* [Iteration 30 Planning]({filename}/posts/2020-planning-iteration-30.md): May 11, 2020
* Next Demo/Update: May 21, 2020
