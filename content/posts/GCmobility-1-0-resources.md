Title: GCmobility 1.0 Resources
Date: 2020-02-12
Modified: 2020-02-12
Category: Blog
Tags: gcmobility, 2020, reports
Slug: gcmobility-resources-1-0
Status: draft

These resources were produced during the first phase of GCmobility. They are categorized here according to the The Open Group Architecture Framework (TOGAF), Architecture Development Model (ADM)

<link to image here.

Many of these resources link to external and third party platforms such as GCcollab, GCcode, GitLab.com or Google Drive. Please be aware this may cause you to leave this site.

These documents are in various stages of completeness. Unless otherwise stated please consider all documents and resources as draft.

## Preliminary
* Empty

## Requirements
* Empty

## Vision
* Empty

## Business
* [Stakeholder Engagement Strategy](https://docs.google.com/document/d/1WVs3sNnjTrKtMRmdjUr28D0Fm8aOAdOgh-Sx8zXSPFU/edit?usp=sharing)
* [GCmobility Business Model Canvas](https://drive.google.com/open?id=1ifZR8GqEQzGqYLur3XK_Jvk4ZtEIvEHo)
* reports
 * [GCmobility IMpact and Failure Report - Outline and Guide - 2019](https://drive.google.com/open?id=1Sz3B-kQfh64bUZqK5ujNqiTXqbFr8Cn8IYgqyMDXykg)

## Information and Data
* [Measuring Mobility Latency and pain points](https://docs.google.com/spreadsheets/d/18yO6Fsplem-1aWEoUAYN2iE0VMpTwCfDlpn3PKzvmpI/edit?usp=sharing)
* [Public Service Mobility Rates](https://docs.google.com/spreadsheets/d/1AFMu_3yRdiEQ8Oich6RBi31cyugvEvQTELvx8Y1aPgs/edit?usp=sharing)
* [GC FOSS Approvals](https://docs.google.com/spreadsheets/d/1rIkRCLCIEf5v_94Zk3Ty1yz8LqS5TBBzvNwfGa-x6dc/edit?usp=sharing)
* [GCmobility: Free Agents Mobility Boarding Map Analysis](https://docs.google.com/document/d/12h3K50vXwu6K6Om4uI5xckWYHqPZN_hpovMEPUmwjG0/edit?usp=sharing)
* [GCmobility Questionnaires - Design](https://docs.google.com/document/d/1pVCP8kFwsijysOrue_MMa9zRdX7yS4u_X27bF_KGnD8/edit?usp=sharing)

## Technology
* Empty

## Opportunities and Solutions
* [SSC Standard Operating Procedure: Management of MyKey for Free Agents](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/issues/5#note_127897)
* [FreeAgents GCmobility Procedures](https://drive.google.com/open?id=1vE9n0exQVM8JdQ7-HhoZo2A1DcP3AQ87b00EzXYVbzM)

## Migration
* Empty

## Implementation and Governance
* [GCmobility Iteration Reviews and Demonstrations](https://drive.google.com/drive/folders/1SKQUQwp-Nx4f-ssPskv29yiiJe0kTlfx?usp=sharing)
* [GCmobility Community Meetup Schedule 2019-2020](https://drive.google.com/open?id=1FsMkS4EWbSqPXu2eBVBXcs2nlmZMCtKkdJFHglu-Plc)
* [GCmobility Agile Roadmap](https://drive.google.com/open?id=1buBQqbscx25L43P27n_nqd2FiRN8-0N6)
* [GCmobility Crossb boarding Manual For Mobile Workers](https://drive.google.com/open?id=1Cdf2OMh-lgh7DVSzpTJq8804aR5ivomu)

## Change Management
* GCmobility Backlogs
 * [Initial GCmobility Backlog](https://docs.google.com/spreadsheets/d/18uB_JJrw-tSQODHvEtSmTAbx1EaYpuItq0TId36u0m0/edit?usp=sharing)
 * [GCmobility Operational Backlog on GCcode](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/issues)
