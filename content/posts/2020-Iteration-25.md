Title: GC Mobility Update Iteration 25
Date: 2020-03-12
Modified: 2020-03-12
Category: Blog
Tags: review, demos
Slug: gcmobility-iteration-review-25
Toc_Include_Title: false
Status: published
Summary: Iteration 25 of GCmobility focused on dasboarding, data and data visualization. We are also changing the way we deliver our demos. Less slides and more showing.

## Summary
Iteration 25 of GCmobility focused on dasboarding, data and data visualization. We are also changing the way we deliver our demos. Less slides and more showing.

## Overview
This post represents the update for Iteration 25 of the GCmobility project.

First off, we invite you to check out the latest Living Digital Blog post ["Roaming from coast-to-coast: the life of a Free Agent"](https://www.canada.ca/en/government/system/digital-government/living-digital/roaming-coast-to-coast-coast-life-free-agent.html). "What if I were to tell you that as a Government of Canada (GC) employee, there’s an exciting way to change the way you work? A way that lets you move around the GC faster and more often than you could ever imagine? A way to spread your wings and have more freedom in your career?"

What articles have captured your attention in the mobility space recently? [reach out to us]({filename}/pages/contact-us.md) to share resources and articles you find.

After this demo please take some time to complete the [GCmobility Retrospective Questionnaire]({filename}/pages/iteration-feedback.md) after you have viewed the demo and/or checked out the resources on this page. The questionnaire asks three simple questions. What we should:

* Continue Doing
* Stop Doing
* Start Doing

Your input to this questionnaire will help us evolve how we do these demos and what we cover.

[Learn more about the GCMobility initiative]({filename}/pages/about.md), a joint partnership between [Canada's Free Agents](https://www.linkedin.com/company/freeagents-agentslibres/about/) and [Shared Services Canada](https://www.canada.ca/en/shared-services.html).

We have recently broadened our audience for our bi-weekly iteration demos. We continue our trial using [YouTube as a hosting platform for our iteration videos](https://www.youtube.com/channel/UC-mL8ozVrg2pq-x4Cd2ZXtQ/). As we mature the agile processes with our stakeholders we continue to invite interested stakeholders to join us live via webEx. If you are interested in joining us for future live demonstrations please [reach out to us]({filename}/pages/contact-us.md).

## Non-Demonstration Items
* GCmobility Leadership
  * [x] Secured ADM sponsor for Gcmobility September 2020
* Update stakeholder list see:[Stakeholder Metrics]({static}/data/stakeholders.csv)
* Stakeholder Engagements
  * [ ] Engagement with Teams implementing Jarvis (PCH, ECCC)(rescheduled)
  * [x] Mobility Perspectives of Executives (CIO discussion)
    - [ ] Request to produce a GCmobility Deck for executives.
    - [ ] Idea for developing a placemat
  * [x] Remote interactive Workshop - Futures in Government
    - Interesting work by the forsight team at CRA
* Community Participation
  - [x] Identity Management Workshop (SSC)
  - [x] HR Data Discussion with Free Agents
    - [ ] Potential effort to consolidate Free Agent projects related to HR and or HR Data into a single deck.
  - [x] Teleworking presentation by Frank Assu
    - Thanks Frank. We look forward to your next presentation (link?)
  - [x] LeanCoffeeTable and Agile - Agile Community of Enablement (ACE): Virtual Meetup

  **Recuring Activities**
This sub-section highlights the various activities that occur regularly throughout a typical iteration. They are shared here in the spirit of transparency.

  * Weekly Update for the Senior Assistant Deputy Minister of Networks, Security and Digital Services (NSDS), Shared Services Canada (SSC)
  * GCMobility
      * Iteration Planning
      * Itertation Review
      * Open Office Hours (Tues and Thurs: 10-12, 1-3)
  * GCmobility Digital Upskilling Pilot
      * Planning
      * Work Sessions
  * Service Evolution Team Meeting
  * GCmobility Communications Check in
  * GCmobility Bilat with Director of Service Evolution
  * Common Desktop Operation Environment (CDOE) Working Group Meeting

## Living The Pivot
A pivot is a "structured course correction designed to test a new fundamental hypothesis about the product, strategy, and engine of growth.“ (Wikipedia: Lean Startup#pivot)

Alternatively, Steve Blank defines a pivot as "changing (or even firing) the plan instead of the executive”

Recent GCmobility Pivots:

  * Tech: POC 2: 1 Phone, 1 Laptop/Tablet, 1 canonical e-mail)
  * Processes: Move Focus from onboarding to cross-boarding
  * People: Hyper Mobile Employees and Executives (rates > 30%)
  * Policies: Enterprise Architecture Supporting Mobility
  * Services: Crossboarding as an enterprise, interdepartmental, internal digital service

### Demonstrable items
#### General Items
* [x] Post [Iteration 24 Video and slides]({filename}/posts/2020-Iteration-24.md)
* [x] Fix bulleted unorded lists
* [x] Confirm the use of HTTPS
* [x] Create [Iteration Dashboard]({filename}/posts/iteration-dashboard.md) for project stats, and publish raw data:
  * [x] [Iteration Burn Down]({static}/data/burndown.csv)
  * [x] [Stakeholder Metrics]({static}/data/stakeholders.csv)
  * [x] [Web Metrics]({static}/data/web-metrics.csv)
* [x] Move Demonstration content to site contents
  *  [x] Recent pivots
  *  [x] Non Demonstration Items
* [x] [GCmobility Iteration Retrospective Questionnaire]({filename}/pages/iteration-feedback.md)
* [x] [Iteration 25 Planning]({filename}/posts/2020-planning-iteration-25.md)

### Site Release Update
This section hilights some updates you will find on this site since our last iteration.

#### New Features
Welcome back to our continuously evolving site hosted on [GitLab Pages]({filename}/posts/2020-gcmobility-on-gitlab-pages.md). Here are some of the latest feature changes to our site:

* Added some new functionality through [Pelican Plugins](https://github.com/getpelican/pelican-plugins)
    * [pelican-toc](https://github.com/ingwinlu/pelican-toc/tree/b98d89b2cfa857c59b647ef0983a470408d6d8cd): Adds the ability to include a Table of Contents automatically to each new article as it "generates a table of contents for pelican articles and pages, available for themes via article.toc"
* We now manage static files such as images through the [Git Large File Storage Git Extension](https://git-lfs.github.com/)

#### New Pages
Here are some of the latest static content changes (aka pages) to our site:

* [GCmobility Retrospective Questionnaire]({filename}/pages/iteration-feedback.md) embeded form

#### New Posts
Take a look through the [latests blog posts](https://gcmobility.gitlab.io/category/blog.html) to see what else is new.

* [Iteration 25 Planning]({filename}/posts/2020-planning-iteration-25.md)
* [Iteration Dashboard]({filename}/posts/iteration-dashboard.md)
  - Our first three datasets
    - [Iteration Burn Down]({static}/data/burndown.csv)
    - [Stakeholder Metrics]({static}/data/stakeholders.csv)
    - [Web Metrics]({static}/data/web-metrics.csv)

## Iteration Review
### Index
Check out the embeded Iteration Review video and presentation.

If you are tight on time then try these time stamped video chapters:

<!-- include timesstamped links to sections of video eg (https://youtu.be/U8Q5e5WCg8I?t=1m35s)-->

* [GCmobility Overview](https://youtu.be/1kZ7j-5MvFk?t=11)
* [Dashboards]()
* [Upskilling Pilot Update](https://youtu.be/1kZ7j-5MvFk?t=3m15s)
* [Web Based Demo Content](https://youtu.be/1kZ7j-5MvFk?t=3m35s)
* Living Digital: [Life as a Free Agent (Article)](https://youtu.be/1kZ7j-5MvFk?t=6m57s)
* [Iteration Feedback Form](https://youtu.be/1kZ7j-5MvFk?t=7m50s)
* [Contact Us Form](https://youtu.be/1kZ7j-5MvFk?t=9m20s)
* [Non Demonstration Items](https://youtu.be/1kZ7j-5MvFk?t=9m15s)
  * [Robotic Process Automation: Introducing Jarvis](https://youtu.be/1kZ7j-5MvFk?t=9m52s)
  * [Introducing Jarvis](https://youtu.be/1kZ7j-5MvFk?t=9m52s)
  * [Introducing Jarvis](https://youtu.be/1kZ7j-5MvFk?t=9m52s)
  * [Living The Pivot](https://youtu.be/1kZ7j-5MvFk?t=11m57s)
* [General Demo Items](https://youtu.be/1kZ7j-5MvFk?t=12m23s)
  * [Iteration 24 Update](https://youtu.be/1kZ7j-5MvFk?t=12m27s)
  * [Bug Fix: Bulleted Lists](https://youtu.be/1kZ7j-5MvFk?t=12m50s)
  * [Digital Standard - Security: Confirm use of HTTPS for Secured Web Connections](https://youtu.be/1kZ7j-5MvFk?t=12m59s)
  * [Feature: Iteration Dashboards (partial fail)](https://youtu.be/1kZ7j-5MvFk?t=13m58s)
  * [Feature: Iteration Dashboards using D3.js](https://youtu.be/1kZ7j-5MvFk?t=15m48s)
  * [Feature: Table of Contents](https://youtu.be/1kZ7j-5MvFk?t=23m56s)
  * [Feature: Git Large File Storage](https://youtu.be/1kZ7j-5MvFk?t=24m27s)
  * [Digital Standard - Open By Default: Iteration Planning](https://youtu.be/1kZ7j-5MvFk?t=25m50s)
  * [Feature: ATOM Publishing Protocol News Feed (failed)](https://youtu.be/1kZ7j-5MvFk?t=28m00s)
* [Engage GCmobility](https://youtu.be/1kZ7j-5MvFk?t=33m23s)

### Video

Demonstration video </br>

<!--
The Video will be posted after the demo has been recorded. Check back soon.

<iframe width="560" height="315" src="https://www.youtube.com/embed/U8Q5e5WCg8I" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
-->
<iframe width="560" height="315" src="https://www.youtube.com/embed/1kZ7j-5MvFk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Slide Deck
Demonstration Presentation </br>

<!-- Go into slide deck, select file > publis to web > embed
Do not use share link-->

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vT0-21U3akusGQyYoutEan2XhEZ-bVaqRv7mRKCppHn5-f1lAunnyaFmj_C3K3T0A/embed?start=false&loop=false&delayms=3000" frameborder="0" width="640" height="389" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## News and Upcoming Events
Iteration 26 Planning({filename}/posts/2020-planning-iteration-26.md): March 16, 2020
Demo: March 26, 2020

**Upcoming Demos**
March 26, 2020
April to September Schedule TBD

**GC Mobility Community Meetups**
* March 2020 TBD (virtual venue?)


We will see you back here in 2 weeks for the next installment of our iteration reviews.

Be Mobile with [GCmobility!](http://bit.ly/gcmobility-wiki)
Soyez mobile avec [GCmobility!](http://bit.ly/gcmobility-wiki)
