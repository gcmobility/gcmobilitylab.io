Title: GCmobility 2019-2020 Objectives Self Evaluation
Date: 2020-02-20
Modified: 2020-02-20
Category: Blog
Tags: gcmobility, iteration 24
Slug: gcmobility-2020-objectives-evaluation
Status: draft

Well, now that we highlighted our [2019-2020 achievements]({filename}2020-gcmobility-achievements.md), lets take some time to reflect on how well we met our intended objectives.

The primary objective was to "Lead Joint SSC and CFA GCMobility Pilot" as described on GCCode project page (https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility). The various tasks identified under this objective include:

1.	Reduce the onboarding and offboarding time required by Free Agents when they start/end any of their Free Agent Opportunities.
   * While we had high ambitions to be able to pre-meaure, build something, and post-measure, we did not quite meet all of those stages.
   * Soon into the project we decided to measure the "cross boarding" time which included offboarding from dept A and onboarding to dept B
   * We quickly learned
   * We did find some crude ways of measuring
2.	Support rapid deployment of Free Agents with established and authoritative IT solutions that can easily connect to “hosting departments”
3.	Support Free Agents to connect assigned technology to home and host departmental networks.
4.	Experiment with various mobility models as proofs of concept
5.	Coordinate and communicate project activities in the open with the wider GoC community using collaborative tools such as GCCollab and GCCode.
•	Support GoC commitment to a mobile, and modern workforce as stated in the IM/IT Roadmap 2017-2021, including:
o	Principle 6: enable a modern workplace: anywhere, anytime with anyone, and
o	Strategic Goal 4: Agility, "provides a technologically advanced workplace that supports mobility".
•	Setup and manage GC wide advisory committee/working group to discuss policy and technical solutions related to GC Mobility
•	Identify and address organizational policy challenges to GC Mobility
•	Provide advice and support in SSC investigative GC Mobility proof of concept (POC) both from a business and technical lens
•	Lead pilot implementation based on the solution retained from the working group and proof of concept findings
•	Manage engagement with required OGD
