Title: GC Mobility Iteration Planning 27
Date: 2020-03-30
Modified: 2020-03-30
Category: Blog
Tags: planning
Slug: gcmobility-iteration-planning-27
Toc_Include_Title: false
Status: published
Summary: This post outlines the goals for Iteration 27 of the GCmobility initiative

## Intro
GCmobility is managed using the Agile Scrum of iterative delivery. Each iteration occurs over a 2 week period. The Iteration starts with a planning meeting and the Iteration ends with an Iteration review which includes a product demonstration.

## Iteration Goals
This section highlights some of the specific goals we have identified for this iteration. Currently we are transitioning between two product backlogs, on Hosted on GCCode and the other hosted on Gitlab.

**COVID-19 Priorities**
Due to the current situation around COVID-19 GCmobility is focusing towards the following Priorities

* [ ] Pandemic as a Pivot
* [ ] Prototype "GCmobility service desk"
* [ ] Ensure all Free Agents are online and working remote especialy regarding MyKey/VPN/GCSRA
* [ ] Offline access to e-mail.
* [ ] Create a mobility health check or checklist
* [ ] Ensure all Free Agents are supported with remot working
  - [ ] Create cross boarding tracker
  - [ ] Address Parks Canada Issues.
* [ ] clean up backlog
* [ ] Add new issues from white board

**Bug Fixes**

* [ ] Iteration 25 dashboard images

**General Items**

* [ ] Bug Fix: RSS News Feed in Outlook
* [ ] Feature: Share plugin to use social media icons
* [ ] Feature: D3 Dashboard
* [ ] Feature: Comments using Disqus
* [ ] Post Iteration 25 Demo to YouTube
* [ ] Request to produce a GCmobility Deck for executives
* [ ] Idea for developing a placemat
* [ ] Review Beyond 2020 Webcast "What you need to Fuel Public Service Innovation"
* [ ] Upskilling pilot mid-way article
* [ ] Compiling Jarvis in C#
  * [ ] Create a new "hello world" process using Jarvis

**Web Site @ gcmobility.gitlab.io**

  * [ ] Functionality
    * [ ] Add RSS support for integration into MS Outlook and news Readers (Review Atom support)
    * [ ] Add Disqus support for discussion
  * [ ] posts
    - [ ] Digital Upskilling
    - [ ] Iteration 20 Update
    - [ ] GCcollab Discussion Topics
    - [ ] Migrate older Content
    - [ ] How to Work "Off Line"
  * [ ] New Content
    - [ ] Mobility Data: Sources, Metrics and Gaps
    - [ ] GCmobility Office Hours and Bookings (Contact us)


**Stakeholder Engagement**

* [ ] GCmobility twitter account
* [ ] GCmobility mailchimp account
* [ ] Engagement with Teams implementing Jarvis (PCH, ECCC)(rescheduled)

**Project Management**

* [ ] migrate / create backlog
* [ ] Import any issues exported from GCcode after backlog clean up

## Non Demonstration items
These items may include various activities such as recuring events, project management tasks and stakeholder engagements. These items are shared to provide insight into the various types of work that go into GCmobility, beyond the typical "product development" tasks often communicated during an iteration cycle.

**Recuring Activities**

* Weekly Update for the Senior Assistant Deputy Minister of Networks, Security and Digital Services (NSDS), Shared Services Canada (SSC)
* GCMobility
    * Iteration Planning
    * Itertation Review
    * Open Office Hours (Tues and Thurs: 10-12, 1-3)
* GCmobility Digital Upskilling Pilot
    * Planning
    * Work Sessions
* Service Evolution Team Meeting
* GCmobility Communications Check in
* GCmobility Bilat with Director of Service Evolution
* Common Desktop Operation Environment (CDOE) Working Group Meeting

**Iteration Specific Activities**

* [ ] Beyond 2020 Webcast "What you need to Fuel Public Service Innovation"

## Issue Management

GCmobility is currently transitioning from an internal issue management and source code versioning platform (GCcode) to a public and external platform (GitLab.com). Where appropriate issues listed in our demos might be linked to any one of the folowing platforms

### GCcode
These issues are managed on [GCcode](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility) which is internal to the Government of Canada. Some links may only be accessible to the internal GC network during this transition time.

### GitLab.com
### General GCmobility Issues
These issues are managed on [Gitlab.com](https://gitlab.com/gcmobility/gcmobility-project) and is accessible generally to the World Wide Web. If links are blocked, it may be due to your organizations firewall.

### GCmobility Web Site Issues
This section highlights the iteration goals for the [gcmobility.gitlab.io site](https://gcmobility.gitlab.io) and are hosted on Gitlab.com.



### GCmobility Digital Service Issues




## Agile Resources
To learn more about Agile Scrum there are many resources on the web. One resource you may consider checking out are the resources offered by [Mike Cohn](https://www.mountaingoatsoftware.com/company/about-mike-cohn) at [Mountain Goat Software](https://www.mountaingoatsoftware.com/), where you can [Learn About Agile](https://www.mountaingoatsoftware.com/agile).
