Title: Consierge Approach to Cross Boarding
Date: 2020-03-16
Modified: 2020-05-07
Category: Blog
Tags: experiment
Slug: consierge-crossboarding
Toc_Include_Title: false
Status: published
Summary: In this post we share our experiences of running a make shift consierge service for Canada's Free Agents to get them online and productive. THis post will be updated daily as the experiment proceeds.

**NOTE**: This post is a work in progress.

## Summary
In this post we share our experiences of running a make shift consierge service for [Canada's Free Agents](https://wiki.gccollab.ca/Canada%27s_Free_Agents) to get them online and productive. This post will be updated regularly as the experiment proceeds.


## Scenario
Free Agents are Canadian Federal Public Servants who have joined the Canada's Free Agent program. In short Free Agents are indeterminate (aka permanent) employees who prefer to manage their career as a type of gig worker. As such Free Agents tend to move between projects on average between every 4 and 18 months. THis high rate of mobility can cause problems when crossboarding between departments because the internal processes for onboarding and off boarding tend to move slower than the speed these employees move at. This rapid movement can sometimes put additional strain on various departmental corporate services. By establishing a "mobility service", the GCmobility Initiative aims to support mobile workers by helping them navigate the cross boarding process.

"Launched in 2016, the [Canada's Free Agent](https://wiki.gccollab.ca/Canada%27s_Free_Agents) program is a new model for workforce mobilization. It offers public servants the freedom to select work that matches their skills and interests and allows them to make a contribution that they find meaningful. It also supports managers looking to rapidly and easily acquire top talent with emerging and core skills in order to support their short-term project needs. Free Agents are screened for attributes that are beneficial for solving problems and skills that are in demand.

Reimagine mobility with us! " (source:[GCcollab.ca](https://wiki.gccollab.ca/Canada%27s_Free_Agents))

This experiment has been waiting for approval for a number of months, however  with COVID-19 in full swing in Canada, it seemed appropriate to just launch something before approvals were completed and run it as an experiment.Current, most Federal Public Servants are in full Telework mode. And Canada's Free Agents are no exception.

The [Canadian Digital Service](https://digital.canada.ca/) recently shared their approach to onboarding through a Blog post. In ["Making a great first impression: Onboarding matters"](https://digital.canada.ca/2019/07/29/making-a-great-first-impression-onboarding-matters/), the authors hit on 3 key ideas:

   1. Onboarding starts before the first day of the job
   2. You want to make the first day a great day for the new employee
   3. Onboarding continues beyond day 1

So the GCmobility team paid the CDS a visit to learn more about their concierge style service. In the world of Lean Startup, the idea of a [concierge type service is often used as an initial Mimimum Viable Product (MVP)](https://www.shortform.com/blog/concierge-mvp/). After engaging with CDS and exploring how various internal service models for cross boarding operate within departments GCmobility started to explore the concept of Cross Boarding as a Digital Service.

Digital services are a popular subject these days both witin the Canadian Federal Public Service and around the world. Digital Services are often thought of as a key component, or they key deliverable of "[Digital Government](https://www.canada.ca/en/government/system/digital-government.html)" in Canada. However, Digital services take resources such as time, people, salary and operational funding. GCmobility has been shopping around the idea of Cross Boarding as a digital service for a while looking for potential partners, collaborators and funders.

Then [COVID-19](https://www.canada.ca/en/public-health/services/diseases/2019-novel-coronavirus-infection.html) happened, and soon impacted Canadians, government operations and the GCmobility Initiative. Over the course of a week things changed. Up to now, issues of mobility within the federal government had often been the reality for a select few (less than 30%), and where ideas were often captured in slides, presented at thought leader gatherings, explored in individual department pilots and circular discussions around how to manage remote workers to a GCmobility went from a conceptual intiatives. Today, most public servants find themselves at home either able to telework (eg SSC, NRCAN, TC, TBS), needing to share telework infrastructure with others identified as a priority, or told that teleworking solutions are 12-18 months out.

While many Free Agents are comfortable with remote work, telework or a hybrid somewhere in the middle, every department they move to might have a different requirement. This leads to Free Agents experiencing downtime while they wait for business processes to churn through the system.

This week we are trying an experiment. Can GCmobility offer a hands on personal Concierge IT service desk solution to help Free Agents get connected. The challenge is that there are 90 Free Agents and 1 member of the GCmobility team.

## The Experiment

Does a personalized, hands on, concierge approach
Affect the speed and user satisfaction of Free Agents
If I keep the other aspects of how Free Agents move around
The Same.

## The Concierge Process.
Before reaching out for a personalized service, a few key steps were requested of Free Agents. These steps include:


## Concierge Type tasks

## Free Agent Slack Community Support
As a program of 90 employees spread across 4 home departments, Canada's Free Agents leverage extensive use of the free tier of Slack, a community chat solution. One of the channels in active use on the Free Agent slack is #techproblems, where fellow Free Agents help each other with their tech issues. Where the concierge approach works well for the one-off and unique situations that often don't have specific Standard Operating Procedures, slack is great for supporting issues that have been experineced by the whole community. This section will highlight just a few of the issues identified and addressed through the Free Agent Slack #techproblems channel over the course of this experiment

* [ ] How to work offline with outlook.

## Updates
This section highlights some of the ongoing updates throughout this experience. Updates are listed in reverse chronological order so the latest update floats to the top.

### 2020-05-07
* We will continue to provide updates on this Alpha Service through our regular iteration reviews. For our first demo run through check out [Iteration 29 Review]({filename}/posts/2020-iteration-29.md). We now several components of our service cobbled together and being used with Canada's Free Agents as a test case. Service components at this point include.

For a further breakdown of what each component includes, [learn more about the Mobility Service]({filename}/pages/service-about.md)

### 2020-04-22
* Launched phase 2 of the service with targeted test group (90 Free Agents from NRCAN, TBS, TC)
* Developed an about page for the service
* Developed a simple HTML form that generates a pre-formatted e-mail
* Project focused [GCmobility Twitter](https://twitter.com/GCmobility) account 

### 2020-04-20
* Setup a new project space at [Gitlab.com](http://gitlab.com) to house the service.
* Configured module to establish the [GCmobility Service Desk](https://gitlab.com/gcmobility/cross-boarding-service/-/issues/service_desk)
* Created a generic e-mail @gmail to handle communications between the front office and back office. There is no need to publish this e-mail at this time.
* advertised service to Free Agent slack on #techproblems channel.


## TO Do List
* move this to do list to
* Create a web form for submitting Issues
* Create a landing page for the service
