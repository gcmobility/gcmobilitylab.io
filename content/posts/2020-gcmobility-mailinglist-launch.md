Title: GC Mobility Launches its Mailing List
Date: 2020-05-21
Modified: 2020-05-21
Category: Blog
Tags: announcements
Slug: gcmobility-mailinglist-launch
Toc_Include_Title: false
Status: published
Summary: Iteration 30 of GCmobility focused on seeking approvals to run a usability study for mobile employees on M365. COVID-19 social issolation and telework practices has presented a unique opportunity to test out emerging enterprise solutions like M365. The use cases we want to explore are different than what an individual departments may be requiring. 

[Subscribe to our mailing list | Abonnez-vous à notre liste de diffusion]({filename}/pages/mailing-lists.md)

## English
Our Vision:

By 2025,
Canadian Federal Public Servants
have the tools to work effectively and efficiently
From anywhere,
At anytime
For any department
and beyond

Welcome to the GCmobility Mailing List

As a key stakeholder in the GCmobility Initiative, are you interested in being kept up to date on the progress of our project?. GCmobility is moving away from using mailing lists managed by desktop e-mail software and spreadsheets of contacts. We are moving towards a more modern mailing list system. We have chosen to leverage Mailchimp as our e-mail marketing platform.

As a project team at GCmobility, we can remain confident that recipients of our mailings are in fact interested in receiving our updates. It also helps us achieve our goals of working in the open by default by allowing anyone to subscribe.

As a stakeholder Mailchimp allows you to better control what mail lands in your mailbox. If you prefer to unsubscribe from this mailing list, you may do so at any time by following the UNSUBSCRIBE link below. You can also re-subscribe at some point in the future if you wish. You can also UPDATE YOUR PREFERENCES to explore other options for receiving these mailings.

The Frequency of mailings may fluctuate slightly from week to week. At a minimum we will aim to send out an e-mail at the end of each iteration, which is every 2 weeks. We do not anticipate to send out more than 2 mailings in a given week. We anticipate on average 2-4 mailings each month.

Two items of interest for our first mailing are updates to the two most recent project iteration updates.

[Iteration 29]({filename}/posts/2020-iteration-29.md) review of GCmobility focused on setting up an alpha mobility service as well as exploring the opportunity to stand up a collaborative platform for mobile workers. Our current focus is to ensure that the Free Agent family is well supported as the challenges with crossboarding between departments are magnified during COVID-19.

[Iteration 30]({filename}/posts/2020-iteration-30.md) review of GCmobility focused on seeking approvals to run a usability study for mobile employees on M365. COVID-19 social issolation and telework practices has presented a unique opportunity to test out emerging enterprise solutions like M365. The use cases we want to explore are different than what an individual departments may be requiring.

Thank you for your support of the GCmobility Initiative to date. We hope you will continue along this journey with us and remain as a subscriber. However if your interests are no longer aligned to our vision of a mobile Government of Canada then we will be sad to see you go and appreciate all your contributions to date.

Connect and Be Mobile with GCmobility.
| [Bookmark the GCmobility Site](https://gcmobility.gitlab.io) | [Read our Blog](https://gcmobility.gitlab.io/category/blog.html) | [Follow Us On Twitter](https://twitter.com/GCmobility) | [Join Us On GCcollab](https://gccollab.ca/groups/about/2183003) | [Subscribe to Our Mailing List]({filename}/pages/mailing-lists.md) |  

## Francais
Notre vision :

D'ici à 2025,
Fonctionnaires fédéraux canadiens
disposer des outils nécessaires pour travailler de manière efficace et efficiente
De n'importe où,
A tout moment
Pour tout service
et au-delà

Bienvenue sur la liste de diffusion GCmobility

Vous recevez ce courriel parce que vous avez été identifié comme un acteur clé de l'initiative GCmobilité. GCmobility s'éloigne de l'utilisation de listes de diffusion gérées par des logiciels de messagerie électronique et des feuilles de calcul de contacts. Nous nous dirigeons vers un système de listes de diffusion plus moderne. Nous avons choisi d'utiliser Mailchimp comme plate-forme de marketing par courrier électronique.

En tant qu'équipe de projet de GCmobility, nous pouvons être sûrs que les destinataires de nos mailings sont effectivement intéressés par nos mises à jour. Cela nous aide également à atteindre nos objectifs de travail en plein air par défaut en permettant à tout le monde de s'inscrire.

En tant que partie prenante, Mailchimp vous permet de mieux contrôler le courrier qui atterrit dans votre boîte aux lettres. Si vous préférez vous désabonner de cette liste de diffusion, vous pouvez le faire à tout moment en suivant le lien UNSUBSCRIBE ci-dessous. Vous pouvez également vous réabonner à un moment donné si vous le souhaitez. Vous pouvez également mettre à jour vos préférences afin d'explorer d'autres options pour recevoir ces envois.

La fréquence des envois peut varier légèrement d'une semaine à l'autre. Nous nous efforcerons au minimum d'envoyer un courrier électronique à la fin de chaque itération, c'est-à-dire toutes les deux semaines. Nous ne prévoyons pas d'envoyer plus de deux envois au cours d'une semaine donnée. Nous prévoyons en moyenne 2 à 4 envois par mois.

Deux éléments d'intérêt pour notre premier envoi sont les mises à jour des deux dernières itérations du projet.

L'examen de [l'itération 29]({filename}/posts/2020-iteration-29.md) de GCmobility s'est concentré sur la mise en place d'un service de mobilité alpha ainsi que sur l'exploration de l'opportunité de mettre en place une plate-forme de collaboration pour les travailleurs mobiles. Notre objectif actuel est de veiller à ce que la famille des agents libres soit bien soutenue, car les défis posés par le transbordement entre les services sont amplifiés lors de COVID-19.

L'examen de [l'itération 30]({filename}/posts/2020-iteration-30.md) de GCmobility s'est concentré sur la recherche d'approbations pour mener une étude de convivialité pour les employés mobiles sur M365. L'isolement social de COVID-19 et les pratiques de télétravail ont offert une occasion unique de tester des solutions d'entreprise émergentes comme M365. Les cas d'utilisation que nous voulons explorer sont différents de ce dont un service individuel peut avoir besoin.

Nous vous remercions de votre soutien à l'initiative GCmobilité à ce jour. Nous espérons que vous continuerez dans cette voie avec nous et que vous resterez un abonné. Toutefois, si vos intérêts ne correspondent plus à notre vision d'un gouvernement mobile du Canada, nous serons tristes de vous voir partir et d'apprécier toutes vos contributions à ce jour.

Connectez-vous et soyez mobile avec GCmobility.
| [Marquez le site GCmobilité](https://gcmobility.gitlab.io)| [Lisez notre blog](https://gcmobility.gitlab.io/category/blog.html) | [Suivez-nous sur Twitter](https://twitter.com/GCmobility) | [Rejoignez-nous sur GCcollab](https://gccollab.ca/groups/about/2183003) | [Abonnez-vous à notre liste de diffusion]({filename}/pages/mailing-lists.md) |

Traduit avec www.DeepL.com/Translator (version gratuite)
