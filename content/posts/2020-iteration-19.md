Title: GC Mobility Update Iteration 19
Date: 2020-01-15
Modified: 2020-02-27
Category: Blog
Tags: gcmobility, demos, iteration-19
Slug: gcmobility-iteration-review-19
Toc_Include_Title: false
Status: published
Summary: Iteration 19 was focused on closing out our findings of Proof of Concept 1 (POC1). This marks 4 iterations in a row that completed with demonstration.

*UPDATE*: This content was originally published in the [GCmobility group within GCcollab](https://gccollab.ca/groups/profile/2183003/engcmobilityfrgcmobilitu00e9). "Hosted by the Government of Canada, GCcollab is designed to connect you with the information and people you need to work with across Canadian universities, colleges and all levels of government." ([GCcollab Terms and Conditions](https://gccollab.ca/terms)). Some links may lead to internal resources.


## Overview
This post represents the update for Iteration 19 of the GCmobility project.

The [GCMobility initiative](http://bit.ly/gcmobility-wiki) is a joint partnership between [Canada's Free Agents](http://bit.ly/cfa-gcpedia) and [Shared Services Canada](http://bit.ly/ssc-web)

Iteration 19 was focused on closing out our findings of Proof of Concept 1 (POC1). Your can check out the full [Iteration Demo and Review](https://bit.ly/gcmobility-iteration19-video) ([Static Presentation](http://bit.ly/gcmobility-iteration19-presentation)). This marks 4 iterations in a row that completed with demonstration. Our goal was to maintain our cadence of the Agile Scrum method with demonstrations of our progress after each iteration. This was a great success to end off 2019. Some items covered in this demonstration occurred earlier in this initiative and are being shared with the community now. Over the next several iterations we will be covering several other deliverables that have not been demonstrated to the community. In addition to the recorded demo we also posted the static slide deck, which has various interactive elements you can click on and review yourself.

Until now, the iteration demos have been presented to a small and internal group. Today we welcomed some new faces from our stakeholder community as they attended their first live demo. As we mature the agile processes with our stakeholders we continue to invite interested stakeholders to join us in person or virtually via webEx. If you are interested in joining us for future live demonstrations please [reach out to us](mailto:david.sampson@canada.ca).

Note that we have updated our Vision and Objectives in this iteration to be a bit more focused, while still maintaining the overall spirit of mobility within GC. Check out the video and presentation to learn more about our recent pivots.

In the Iteration Review and Demonstration you will learn more about:
* Various non-demonstrable items:
* POC1: Virtual Machines with Hyper-V
* Wiki: Section for Iteration Updates with tracking URL’s
* Notifications: Explore Mailing Lists and Notifications

Although you will notice an item listed for "Digital Workspace: Develop Mobility Personas", unfortunately we failed to address this item and will revisit in the new year.

There will not be an iteration 20, to accommodate the winter festive break, however iteration 21 will start up on January 6, with the next demo on January 16, 2020.


Thanks again for your continued support and encouragement for GCmobility in 2019. We have learned a lot, tested many assumptions, connected with a very diverse group of stakeholders and have a better understanding of the challenges to achieving a mobile workforce in Canadian Federal Government. We look forward to your comments on the demo and how we can improve the experience while keeping formalities light and agile.

On behalf of the GCmobility team, we wish you the very best over the festive winter break  and hope you find time to relax and connect with friends and family. We will see you back here in 2020.

Be Mobile with [GCmobility!](http://bit.ly/gcmobility-wiki) / Soyez mobile avec [GCmobility!](http://bit.ly/gcmobility-wiki)


## Other Resources
For more information please check out the [GCmobility wiki page](http://bit.ly/gcmobility-wiki).
