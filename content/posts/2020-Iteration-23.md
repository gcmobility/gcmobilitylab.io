Title: GC Mobility Update Iteration 23
Date: 2020-02-12
Modified: 2020-02-13
Category: Blog
Tags: gcmobility, demos
Slug: gcmobility-iteration-review-23
Toc_Include_Title: false
Status: published
Summary: Iteration 23 of GCmobility focused on becoming more "digital", in the way we engage our stakeholders, communicate project updates and deliver value.

## Overview
This post represents the update for Iteration 23 of the GCmobility project.

The [GCMobility initiative](https://wiki.gccollab.ca/GCmobility) is a joint partnership between [Canada's Free Agents](https://www.linkedin.com/company/freeagents-agentslibres/about/) and [Shared Services Canada](https://www.canada.ca/en/shared-services.html)

Iteration 23 was focused on GCmobility becoming more "digital", in the way we engage our stakeholders and communicate project updates.

First off welcome to our new web presence. Previously we were using [GCcollab](./gcmobility-on-gccollab.html), and now we are experimenting with [GitLab Pages](./gcmobility-on-gitlab-pages.html). Unlike GCcollab which uses the Elgg content management system, this site uses the Pelican Static Site Generator to convert content files into a static HTML site. Welcome to our latest experiment.

We have recently broadened our audience for our bi-weekly iteration demos. Videos would usually be uploaded to our community group on [GCcollab](./gcmobility-on-gccollab.html). Today we have started a trial  using [YouTube as a hosting platform for our iteration videos](https://www.youtube.com/channel/UC-mL8ozVrg2pq-x4Cd2ZXtQ/). As we mature the agile processes with our stakeholders we continue to invite interested stakeholders to join us live via webEx. If you are interested in joining us for future live demonstrations please [reach out to us](./pages/contact-us.html).

While we trial YouTube for our videos we are also experimenting with [Google Drive for our documents and other files](./pages/gcmobility-files.html). Click the link to learn more about our Public Files section of this site and poke through the directories.

Check out the embeded Iteration Review and Demonstration video (Length: 0:52:49) and presentation (Slides: 30) below.

# Iteration Review Index

If you are tight on time then try these time stamped video chapters:

* [GCmobility Overview](https://youtu.be/U8Q5e5WCg8I?t=35)
* [GCmobility Dashboards](https://youtu.be/U8Q5e5WCg8I?t=243)
* [Recent Pivots](https://youtu.be/U8Q5e5WCg8I?t=454)
* [GCmobility Digital Upskilling Updates](https://youtu.be/U8Q5e5WCg8I?t=837)
  * [Week 2 Update](https://youtu.be/U8Q5e5WCg8I?t=881)
    * GCmobility Self Assessments:
      * [Canada's Digital Standards](https://youtu.be/U8Q5e5WCg8I?t=899)
      * [Canadian Digital Service](https://youtu.be/U8Q5e5WCg8I?t=1077)
 * [Week 3 Update](https://youtu.be/U8Q5e5WCg8I?t=1468)
 * [Upskilling Pilot Progress](https://youtu.be/U8Q5e5WCg8I?t=1953)
* [Working In The Open: Google Drive](https://youtu.be/U8Q5e5WCg8I?t=2011)
* [GCmobility On The Move: New Web Site](https://youtu.be/U8Q5e5WCg8I?t=2293)
* [Cross Boarding Manual For Mobile Workers](https://youtu.be/U8Q5e5WCg8I?t=2699)
* [Upcoming Events](https://youtu.be/U8Q5e5WCg8I?t=3049)
* [How to Engage with GCmobility](https://youtu.be/U8Q5e5WCg8I?t=3068)

# Iteration Review Video

<iframe width="560" height="315" src="https://www.youtube.com/embed/U8Q5e5WCg8I" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# Iteration Review Slide Deck

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSqELv_eYB_YdP9m1wR8gUgz0pgNHI0vaU2vHWRsqrb9WOKgxqQH3-nG14OC8jecSxXv3i2a_XoM3dC/embed?start=false&loop=false&delayms=5000" frameborder="0" width="560" height="315" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

# Next Iteration
Planning: Feb 17, 2020
Demo: Feb 27, 2020

We will see you back here in 2 weeks for the next installment of our iteration reviews.

Be Mobile with [GCmobility!](http://bit.ly/gcmobility-wiki)
Soyez mobile avec [GCmobility!](http://bit.ly/gcmobility-wiki)
