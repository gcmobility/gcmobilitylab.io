Title: GC Mobility Update Iteration 27
Date: 2020-04-09
Modified: 2020-04-16
Category: Blog
Tags: review, demos
Slug: gcmobility-iteration-review-27
Toc_Include_Title: false
Status: published
Summary: Iteration 27 of GCmobility focused on the COVID-19 pivot. The three main pivot options included putting GCmobility on hold indefinitely (pause), closing up phase 1 of GCmobility permanently (perish), pivot within the initiative or persevere and keep going with designing mobile careers. Learn why we are now moving from working on the GC network to working off the GC network.

## Summary
Iteration 27 of GCmobility focused on the COVID-19 pivot. The three main pivot options included putting GCmobility on hold indefinitely (pause), closing up phase 1 of GCmobility permanently (perish), pivot within the initiative or persevere and keep going with designing mobile careers. Learn why we are now moving from working on the GC network to working off the GC network.

## Overview
This post represents the update for Iteration 27 of the GCmobility project. There was no [Iteration 26]({filename}/posts/2020-Iteration-26.md) demonstration or update.

After reading this update please take some time to complete the [GCmobility Retrospective Questionnaire]({filename}/pages/iteration-feedback.md) after you have viewed the demo and/or checked out the resources on this page. The questionnaire asks three simple questions. What we should:

It has been about a month, or two iterations, since COVID-19 has resulted in lock down and [social isolation for many Canadians](https://www.canada.ca/en/public-health/services/publications/diseases-conditions/covid-19-how-to-isolate-at-home.html), Public Servants and the GCmobility team. To say that COVID-19 has been a game changer is to put things midly. For GCmobility it has forced [yet another pivot]({filename}/posts/2020-gcmobility-pivots.md). The three main pivot options included putting GCmobility on hold indefinately (pause), closing up phase 1 of GCmobility permanently (perish), pivot within the initiative or perservere and keep going with designing mobile careers.

The [COVID-19 situation](https://www.canada.ca/en/public-health/services/diseases/2019-novel-coronavirus-infection.html?topic=tilelink) is quite fluid. Maybe even a bit moistly? It continues to be unclear how long our current situation of being locked down and socialy issolating will last. As a project lead, I could decide to hunker down, ride out the disruption and start things back up where we left off on the other side of COVID-19. I am sure many people would support that approach and there would be limited backlash. However, for now we are pivoting into the wave to see what we can learn from this hopefully unique situation.

You may notice two things with this iteration review off the top. First, I am back to storry telling through text. Second, we did not record a demo for this iteration. The story telling comes as a result of wanting to explain how COVID-19 is impacting GCmobility in lieu of having a recorded demo. The lack of recorded demo is due to a change in our project tooling and we have not tested webex recordings with our new toolset yet.

So how has COVID-19 impacted GCMobility? We are actually writting up a separate article on [The COVID-19 Pivot for GCmobility]({filename}/posts/2020-covid-19-pivot.md). However, essentially it is an unplanned experiment for how well the Canadian Public Service, not to mention the whole world, can shift its workforce. As a sponsoring partner, SSC is concerned with how to mobilize their embeded workforce. As a partner that is a key stakeholder, Canada's Free Agents are concerned with execercising their autonomy to move fast and move often, kind of like an internal Gig style workforce.

SSC was already a pretty mobile enabled organization. With the presence of GCworkplace, VPN and Telework, SSC was well positioned to have their workforce respond to a country wide shutdown and social isolation measures. Not all federal departments were as well prepared to respond. While SSC did not have 100% VPN coverage for its complete workforce it had a higher capacity than most other departments. The primary challenge for SSC employees, is that all laptops need to connect through VPN to access a network or the internet in general.

As a Free Agent housed at [NRCAN](https://www.nrcan.gc.ca/) I have been permitted to work in various GCcoworking Locations. However, I still require access to the NRCAN network to book leave on the internal PeopleSoft. Unlike my SSC issued laptop, I can use my NRCAN laptop without needing to connect via VPN. This allows me to connect to resources on my home network such as printers, scanners and Network Accessible Storage (NAS).

Now NRCAN, as well as having VPN to access departmental resources, also offers as Teleworking Portal using Citrix. Citrix is unique in that unlike the GC Secure Remote Access (GCSRA, aka VPN), Citrix can be used from your personal device. As well as a laptop, NRCAN also manages my @canada.ca e-mail as well as a Samsung 8 mobile phone (Thanks so much for upgrading me from my old blackberry. Thank you!). This phone can be used to set up a mobile hotspot or USB tether to either my NRCAN or SSC laptop.

Pre-COVID-19 it was clear that many departments were under resourced for VPN. The VPN infrastructure was already stressed. This meant there was already little room to handle even more connections. SSC has been working with its [42 partners](https://www.canada.ca/en/shared-services/corporate/partner-organizations.html) to increase these capacities. As you can imagine, this process, like many IT projects in the GC, is slow and expensive. Some latest signals suggest we have moved from 10%-30% capacity and moving towards 50% capacity.

Culturaly, there was a general lack of mobility maturity within GC. The policies were there, the processes were in place, however the people still lacked the skills and confidence. While some executives supported telework, flex work and alternative work arrangements, some executives still felt that if you can't see and touch your employees then you can't manage them. Managers ranged in their support of telework, sometimes treating it as a privaledge for some or perceived punishment for others. Some managers felt as though they lacked the tools and resources needed to manage remote employees with specific needs or challenging personalities.

Meanwhile employees who worked remotely, through telework, or flexible work arrangements sometimes felt left out of impromptue office meetings and water cooler chat. As a result, many employees new to teleworking are now experiencing trial by fire. Based on monitoring some unofficial chatter on Reddit, the federal family is adapting quickly and supporting each other, however there is still lots of work to do.

So what happens when you swing from a workforce where 10% of your workforce is enabled to work remotely and then you need to shift your workforce to 90% or 100%? Well, in short, things that once worked start to break. Things that were already stressed, just fail.

The main response by departments was that if you were not an essential worker, you were asked to stay off the network. If you worked on private networks you were either asked to come in to the office, or just stay home. While a lot of work has happened, there is still a long road to travel. TBS has also released some [tips for Teleworking during COVID-19](https://www.canada.ca/en/government/publicservice/covid-19/organization-specific-information/information-for-tbs-employees.html#toc2). Mitigating COVID-19 is a long game, and government processes are designed to accomodate the short game nature of one year fiscal cycles.

So we now have [empty offices with full houses](https://www.bloomberg.com/news/articles/2020-03-06/empty-offices-full-homes-covid-19-might-strain-the-internet), [60% increased newtork usage during the day in Canada](https://edmontonjournal.com/news/national/covid-19-canadian-telecom-companies-beefing-up-networks-as-usage-surges-from-remote-working/) and still the business of government to conduct. However, the pre COVID-19 approach to rolling out VPN had been restricted to support "essential" government services (source:[Nationalpost.com](https://nationalpost.com/news/canada/thousands-of-federal-employees-to-receive-paid-vacation-since-they-cant-work-from-home)).

SSC has been working hard to [build a gateway to mobile workforce](https://www.canada.ca/en/shared-services/campaigns/stories/right-connections.html) on many fronts especialy VPN and GCCollaboration.

[GCcollaboration](http://gccollaboration.ca) is the Government of Canada's response to supporting collaboration between federal employees during COVID-19. For employees who can't access departmental networks, or just wanting to decrease their network demands they can move their non-protected communications to GCcollaboration. GCcollaboration is simply a GC branded Microsoft 365 instance. Many familiar tools are provided including a full office suite in the cloud. Also, this is the first time many employees will be exposed to MS Teams.

In [Lean Startup](https://theleanstartup.com/) we should be considering on a regular basis if we "pivot" or "perservere". In a government context we should also learn how to "Perish" or kill projects. And sometimes you might jsut need to "patch" things up, not a severe as a pivot, but certainly not staying the original course of perserverance.

Over the last several weeks we have been trying to navigate the impacts COVID-19 is having on the GCmobility Initiative. Several potential options were possible. In the end we are attempting yet another pivot. See the section "Living the Pivot" below.

As we pivot, during iteration 27 we started to explore and action some of these initial pivot points including:

* How to work off-line from the GC network
* How to setup a digital mobility service.

We hope to bring back recorded demos in future iterations. Right now we are focusing on ensuring our own oxygen and medical "masks" are on, before getting back to helping others find their "masks".

[Learn more about the GCMobility initiative]({filename}/pages/about.md), a joint partnership between [Canada's Free Agents](https://www.linkedin.com/company/freeagents-agentslibres/about/) and [Shared Services Canada](https://www.canada.ca/en/shared-services.html).

If you are interested in joining us for future live demonstrations please [reach out to us]({filename}/pages/contact-us.md).

## Living The Pivot
A pivot is a "structured course correction designed to test a new fundamental hypothesis about the product, strategy, and engine of growth.“ Alternatively, Steve Blank defines a pivot as "changing (or even firing) the plan instead of the executive”. (source: [Wikipedia: Lean Startup#pivot](https://en.wikipedia.org/wiki/Lean_startup#Pivot))

### COVID-19 Pivot

This section captures some of the potential ways we could pivot GCmobility in light of the ever changing situation regarding COVID-19.

#### The Options
**Perish**:

* Acknowledge that COVID-19 has changed the landscape of workforce mobility (likely for the better).
* Have confidence that the system will course correct on its own and that visionary leaders will step up to the plate once COVID-19 is just a memory of a recent distraction.
* Wrap the project up, complete some remaining deliverables, package it up nicely for when someone decides that mobility in the federal public service is important again and will put resources towards making it so.
* Trhough our hats into the COVID-19 ring and realocate project resources to the greater public service cause.

**Pivot**

* Stop, drop, roll, get up, wash hands, speak moistly, inventory toilet paper and take a deep breath.
* Empathise with other public servants who are working "off line"
* Check in with the Free Agent family
* Assess current needs of mobile workers
* Establish the basic components of a "digital mobility service"


**Patch**

* latch onto COVID-19 as an imediate driver, selling mobility as athe anti-dote to pandemics and unforseen circumstances that will occur at irregular cycles.
* Leverage Fear, Uncertainty and Doubt (FUD) to move GCmobility forward
* Change the existing story lines away from mobile careers and towards

**Perservere**:

* Ignore COVID-19 with wreckless abandonment and trust that mobile carreers in the future will look the same as how we envisioned
* Execute the same "road map" we have been waiting almost 12 months to be approved.
* Approach the scaling issue the same way, with small scale experiments with willing test subjects

So perhaps you can sense we decided to Pivot. I recognize some of the bullets for each option might be a little tounge in cheek, however some other options were equally compelling and nobel. The whole point is that COVID-19 has presented GCmobility with opportunities that would not otherwise materialize, even after a year of proposals, funding asks, trials, experiments, slide decks, briefings and navigating governance channels could not do. In short, no one had a mandate to lead inter-departmental mobility.

It took a pandemic for the GC to decide mobility is important and leaders have emerged to lead transformation that previously was viewed as "not my mandate". It will take more than increased VPN connections, laptops and home offices to move mobility forward. So lets see what else we can do.

#### The Pivot Decision

So far we have decided to pivot with GCmobilityin the following ways. How we will do this or how far we will go still remains to be seen. We will try to keep a broad perspective on the changing landscape.

**Please put on your own oxygen mask (or [cloth medical mask](https://www.canada.ca/en/health-canada/services/drugs-health-products/medical-devices/activities/announcements/covid19-notice-home-made-masks.html)) before helping others**

* Stop, drop, roll
* Get up, Wash your hands, speak moistly,
* Take an inventory of your toilet paper and take a deep breath.
* Start by ensuring that GCmobility can operate as a project "off line" and "in the open"
* Move all project work off of GCnetwork.
* Consider security, tools, software and workflows.

**Please understand for some employees, remote work is hard**
* Empathise with other public servants who are working "off line"
* Check in with the Free Agent family
* Assess current needs of mobile workers
* Move from wide stakeholder engagement to ensuring Free Agents are well supported to work remotely.
* Engage stakeholders through modern tools (social media and mailing lists)
* Engage Free Agent home Department IT service desks
* Connect with stakeholders through "Lean Coffee Table" sessions.

**Find a need and fill it**

* Create a "Mobility health check" questionnaire
* Create a "Mobility health check list" of resources
* Start putting together the pieces of a "Mobility Service" focused on crossboarding.
* Develop a single point service desk for supporting mobile workers during cross boarding
* Provide a generic E-mail

### Recent GCmobility Pivots:

* **COVID-19**: How to work effectively "Off line"
* **Technology**: POC 2: 1 Phone, 1 Laptop/Tablet, 1 canonical e-mail)
* **Processes**: Move Focus from onboarding to cross-boarding
* **People**: Hyper Mobile Employees and Executives (rates > 30%)
* **Policies**: Enterprise Architecture Supporting Mobility
* **Services**: Crossboarding as an enterprise, interdepartmental, internal digital service

## Iteration Results
### Non-Demonstration Items

* Stakeholder Engagements
  * [ ] Engagement with Teams implementing Jarvis
    - [x] ECCC

### Recuring Activities
This sub-section highlights the various activities that occur regularly throughout a typical iteration. They are shared here in the spirit of transparency.

  * [ ] Weekly Update for the Senior Assistant Deputy Minister of Networks, Security and Digital Services (NSDS), Shared Services Canada (SSC) (On Hold due to COVID-19)
  * [x] GCMobility
      * [x] Iteration Planning
      * [x] Itertation Review
      * Open Office Hours (Tues and Thurs: 10-12, 1-3)
  * [x] GCmobility Digital Upskilling Pilot (complete)
  * [x] Service Evolution Team Meeting
  * [x] GCmobility Communications Check in (series complete)
  * [x] GCmobility Bilat with Director of Service Evolution
  * [ ] Common Desktop Operation Environment (CDOE) Working Group Meeting

### Demonstrable items
**Project Maintenance**

* [x] Post [Iteration 26 Video and slides]({filename}/posts/2020-Iteration-26.md)
* [x] [Iteration 27 Planning]({filename}/posts/2020-planning-iteration-25.md)
* [ ] clean up backlog
* [ ] Add new issues from white board

**COVID-19 Priorities**
Due to the current situation around COVID-19 GCmobility is focusing towards the following Priorities

* [x] Pandemic: [COVID-19 as a Pivot]({filename}/posts/2020-covid-19-pivot.md) (comming soon)
* [x] Taking Care Of your Own Oxygen Mask before helping others
  - [x] Setup personal computer with seprate work account
    - [x] Ubuntu Linux (18.04)
  - [x] [GCcollaboration.ca](http://GCcollaboration.ca)
    - [x] GC Instance of Teams
    - [x] GC Instance of M365
  - [x] Offline access to e-mail.
    - [x] Forward Work e-Mail to @gccollaboration.ca (failed)
    - [x] Send encrypted e-mail to @gccollaboration.ca (failed)
    - [x] Send encrypted e-mail to @canada.ca (success: Through M365 web client)
  - [x] Work Applications
    - [x] Thunderbird E-Mail (connect to M365)
    - [x] LibreOffice (interoperable document formats)
    - [x] Atom "The Hackable" Text Editor
    - [x] Firefox Web Browser
  - [x] Collaboration Applications
    - [x] Native Linux App for Teams
    - [x] Webb App for M365 portal
    - [x] Slack (Free Agents, SSC, Digital GC)
    - [x] Zoom (Group Chats, Audio and Video: OneTeamGov Canada)
    - [x] Rocket Chat (GCcollab Message)
    - [x] Discord (Text Chat, Voice and Video: Canadian Public Servants **NEW**)
    - [x] Skype
  - [x] Security Apps and Consideration
    - [x] Keepass2 (password safe)
    - [x] Authy (2 factor authentication)
    - [ ] ClamTK Virus Scanner
    - [ ] [Solo Keys](https://solokeys.com/): "The first open-source FIDO2 security key"
  - [x] File management
    - [x] Drop Box (work related: exchange files between computers)
    - [x] Google Drive (GCmobility Project, Canada's Free Agents)


* [x] Ensure all Free Agents are supported with remote working
  - [x] Prototype "GCmobility service desk"
    - [x] Setup and Launch [Gitlab Service Desk](https://docs.gitlab.com/ee/user/project/service_desk.html) for GCmobility
    - [x] GCmobility Generic e-mail hosted by Gmail
    - [x] Autogeneration of tickets from e-mail
    - [x] Gitlab Service Desk bot replies to original recipient when ever an issue is updated
    - [x] Service Desk admins handle service desk issues in a simmilar way they do for other project issues or product backlog.
  - [x] Address Parks Canada Issues.

**Bug Fixes**

* [x] [Iteration 25 dashboard](http://localhost:8000/category/data.html) images

**Web Site @ gcmobility.gitlab.io**

  * [x] Functionality
    * [x] Add RSS support for integration into MS Outlook and news Readers (Review Atom support)

**Stakeholder Engagement**

* [x] [@GCmobility twitter account](https://twitter.com/GCmobility)
* [x] Engagement with Teams implementing Jarvis
  - [x] ECCC

### Deferred Issues
These items have been deferred to a future iteration

**COVID-19 Priorities**

* [ ] Ensure all Free Agents are supported with remote working
  - [ ] Create cross boarding tracker
  - [ ] Ensure all Free Agents are online and working remote
  - [ ] Offer support regarding MyKey/VPN/GCSRA
  - [ ] Create a mobility health check or checklist

 **General Items**

* [ ] Feature: Share plugin to use social media icons
* [ ] Feature: D3 Dashboard
* [ ] Feature: Comments using Disqus
* [ ] Request to produce a GCmobility Deck for executives
* [ ] Idea for developing a placemat
* [ ] Review Beyond 2020 Webcast "What you need to Fuel Public Service Innovation"
* [ ] Upskilling pilot mid-way article
* [ ] Compiling Jarvis in C#
  - [ ] Create a new "hello world" process using Jarvis

**Web Site @ gcmobility.gitlab.io**

* [ ] Functionality
  * [ ] Add Disqus support for discussion
* [ ] posts
  - [ ] Digital Upskilling
  - [ ] Iteration 20 Update
  - [ ] GCcollab Discussion Topics
  - [ ] Migrate older Content
  - [ ] How to Work "Off Line"
* [ ] New Content
  - [ ] Mobility Data: Sources, Metrics and Gaps
  - [ ] GCmobility Office Hours and Bookings (Contact us)

  **Stakeholder Engagement**

* [ ] GCmobility mailchimp account
* [ ] Engagement with Teams implementing Jarvis
  - [ ] PCH (rescheduled)

**Project Management**

* [ ] migrate / create backlog
* [ ] Import any issues exported from GCcode after backlog clean up

### Site Release Update
This section hilights some updates you will find on this site since our last iteration.

#### New Features
Welcome back to our continuously evolving site hosted on [GitLab Pages]({filename}/posts/2020-gcmobility-on-gitlab-pages.md). Here are some of the latest feature changes to our site:

* Added some new functionality through [Pelican Plugins](https://github.com/getpelican/pelican-plugins)
    * [pelican-toc](https://github.com/ingwinlu/pelican-toc/tree/b98d89b2cfa857c59b647ef0983a470408d6d8cd): Adds the ability to include a Table of Contents automatically to each new article as it "generates a table of contents for pelican articles and pages, available for themes via article.toc"
* We now manage static files such as images through the [Git Large File Storage Git Extension](https://git-lfs.github.com/)

#### New Pages
Here are some of the latest static content changes (aka pages) to our site:

* [GCmobility Retrospective Questionnaire]({filename}/pages/iteration-feedback.md) embeded form

#### New Posts
Take a look through the [latests blog posts](https://gcmobility.gitlab.io/category/blog.html) to see what else is new.

* [Iteration 28 Planning]({filename}/posts/2020-planning-iteration-28.md)
* [Iteration Dashboard]({filename}/posts/iteration-dashboard.md)
  - Our first three datasets
    - [Iteration Burn Down]({static}/data/burndown.csv)
    - [Stakeholder Metrics]({static}/data/stakeholders.csv)
    - [Web Metrics]({static}/data/web-metrics.csv)

## Iteration Review
### Index
Check out the embeded Iteration Review video and presentation.

If you are tight on time then try these time stamped video chapters:

<!-- include timesstamped links to sections of video eg (https://youtu.be/U8Q5e5WCg8I?t=1m35s)-->

* [GCmobility Overview](https://youtu.be/U8Q5e5WCg8I?t=35)
* [Dashboards]

### Video

Demonstration video </br>

The Video will be posted after the demo has been recorded. We have not had a chance to record a demo for this iteration. Check back soon or subscribe to our [RSS](https://gcmobility.gitlab.io/feeds/all.rss.xml) or [ATOM](https://gcmobility.gitlab.io/feeds/all.atom.xml) feeds to receive real time updates on our site contnet.

<!--
<iframe width="560" height="315" src="https://www.youtube.com/embed/U8Q5e5WCg8I" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/1kZ7j-5MvFk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


-->

### Slide Deck
Demonstration Presentation </br>

The slide deck for this demo will be posted after it has been recorded. We have not had a chance to record a demo for this iteration. Check back soon or subscribe to our [RSS](https://gcmobility.gitlab.io/feeds/all.rss.xml) or [ATOM](https://gcmobility.gitlab.io/feeds/all.atom.xml) feeds to receive real time updates on our site contnet.

<!-- Go into slide deck, select file > publis to web > embed
Do not use share link

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vT0-21U3akusGQyYoutEan2XhEZ-bVaqRv7mRKCppHn5-f1lAunnyaFmj_C3K3T0A/embed?start=false&loop=false&delayms=3000" frameborder="0" width="640" height="389" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

-->

## News and Upcoming Events
[Iteration 28 Planning]({filename}/posts/2020-planning-iteration-28.md): April 14, 2020
Demo: April 23, 2020

**Iteration Schedule**

Iteration   |Start Date |Demo Date  |End Date
------------|-----------|-----------|-----------
28          | 4/13/2020 | 4/23/2020 | 4/24/2020
29          | 4/27/2020 | 5/7/2020  | 5/8/2020
30          | 5/11/2020 | 5/21/2020 | 5/22/2020
31          | 5/25/2020 | 6/4/2020  | 6/5/2020
32          | 6/8/2020  | 6/18/2020 | 6/19/2020
33          | 6/22/2020 | 7/2/2020  | 7/3/2020
34          | 7/6/2020  | 7/16/2020 | 7/17/2020
35          | 7/20/2020 | 7/30/2020 | 7/31/2020
36          | 8/3/2020  | 8/13/2020 | 8/14/2020
37          | 8/17/2020 | 8/27/2020 | 8/28/2020
38          | 8/31/2020 | 9/10/2020 | 9/11/2020
39          | 9/14/2020 | 9/24/2020 | 9/25/2020
40          | 9/28/2020 | 10/8/2020 | 10/9/2020

**GC Mobility Community Meetups**

Schedule for 2020-2021: **TBD**

# Additional Resources:

* Be Mobile with [GCmobility!](http://bit.ly/gcmobility-wiki)
* Soyez mobile avec [GCmobility!](http://bit.ly/gcmobility-wiki)

# Next Iteration
We will see you back here in 2 weeks for the next installment of our iteration reviews.
