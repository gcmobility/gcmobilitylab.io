Title: GC Mobility Update Iteration 15
Date: 2019-10-31
Modified: 2020-02-27
Category: Blog
Tags: gcmobility, demos, iteration-15
Slug: gcmobility-iteration-review-15
Toc_Include_Title: false
Status: published
Summary: Iterations 15 of GCmobility focused on an initial Analysis of the onboarding journey map for Free Agents

*UPDATE*: This content was originally published in the [GCmobility group within GCcollab](https://gccollab.ca/groups/profile/2183003/engcmobilityfrgcmobilitu00e9). "Hosted by the Government of Canada, GCcollab is designed to connect you with the information and people you need to work with across Canadian universities, colleges and all levels of government." ([GCcollab Terms and Conditions](https://gccollab.ca/terms)). Some links may lead to internal resources.


## Overview
This post represents the update for Iteration 15 of the GCmobility project.

The [GCMobility initiative](https://gccollab.ca/groups/profile/2183003/engcmobilityfrgcmobilitu00e9) is a joint partnership between Canada's Free Agents and Shared Services Canada


## Iteration Update Items
* Initial Analysis of onboarding journey map for Free Agents
* [GCmobility Community Terms of Reference: Draft](https://gccollab.ca/discussion/view/3166283/engcmobility-community-terms-of-reference-torfr)
* [GCmobility Partnership Agreement: Draft]()

Engagements
o	TBS Security Policy and TBS Cyber Security teams
o	SSC AAACT (accessibility)
o	Owl Labs, Video Conferencing solution
o	GC Common Desktop Operating Environment (CDOE) Committee


* Follow our ongoing [Stakeholder Engagements ](https://gccollab.ca/discussion/view/3136210/engcmobility-stakeholder-engagementsfr)
* Consider joining one of our[GCmobility Meetups](https://gccollab.ca/discussion/view/3136104/engcmobility-community-meetupsfr).
* We are looking for feedback on our ["Enterprise Architecture for Mobility"](https://gccollab.ca/discussion/view/3138937/enenterprise-architecture-in-support-of-mobilityfr)
* Here are [some ways you can help](https://gccollab.ca/discussion/view/2924125/enhelp-gcmobility-spread-the-wordfr)
* Check out our [evolving Communications and Engagement plans](https://gccollab.ca/discussion/view/3136210/engcmobility-stakeholder-engagementsfr)

### GCmobility Discussion Series
The first three part of a multi part discussion series unpacks GCmobility concepts one at a time. THis approach aims to generate conversation while providing healthy debate or confirmation of assumptions that we are making about mobility. We look forward to engaing you in each of these discussions
* [Why the GCmobility Initiative Matters (Part 1)](https://gccollab.ca/discussion/view/2988160/enwhy-the-gcmobility-initiative-matters-part-1fr)
* [The GCmobility Context (Part 2)](https://gccollab.ca/discussion/view/2988899/enthe-gcmobility-context-part-2fr)
* [The GCmobility Problem Statement (Part 3)](https://gccollab.ca/discussion/view/2994859/enthe-gcmobility-problem-statement-part-3fr)

### GCmobility Video Series
* [Recording of an earlier draft presentation](https://gccollab.ca/file/view/2897473/engcmobility-long-form-presentation-draft-recording-long-37min-uneditedfr). At 37 min it captures that full context of what we are trying to do within GCmobility. Other trimmed down versions are pending as we work with exported Webex videos.
* [Accessibility Accommodation Adaptive Computer Technology (AAACT)](https://gccollab.ca/file/view/3135981/engc-mobility-community-meetup-accessibility-accommodation-adaptive-computer-technology-aaact-program-2019-10-04fr)
* [GCmobility conferencing for remote employees workshop and Owl Labs product demonstration](https://gccollab.ca/file/view/3138376/engcmobility-owl-labs-video-conference-demo-2019-10-08fr)


## Iteration Scrum Update
### What have we been working on
* Experimenting with the format of these Iteration updates (some good feedback so far)
* Our initial stage of Stakeholder Engagement is wrapping up
* We have already been working on compiling and analysing some of our findings
* Compiling data from Free Agent Journey Mapping
* Instead of developing a "one pager" we [developed a wiki page](https://wiki.gccollab.ca/GCmobility) to act as a landing page for GCmobility

### What will We work on
* We are looking to grow the GCmobility team through vehicles like micro missions as we currently do not have any salary funding
* While a Wiki is going to be useful, we feel there is a need for an info graphic
* Preparing to present to SSC Enterprise Architecture Council will require some more work on our Enterprise Architecture model.
* Then we would like to start experimenting with business models for GCmobility using the business model canvas

### What blocks are we facing
* Mobility is a complex problem with lots of people and inter related projects
* Currently GCmobility has a "yellow" light for full stakeholder engagement. Management wants to know the who/how/what. So we are co-designing this piece with our community, you!(stay tuned)
* We are in the process of submitting an updated presentation for Executive update and approval.

### New Issues added to the backlog
* GCmobility needs to develop value propositions for more stakeholder groups to get increased buy-in
* We are working on our next 100 Steps. Currently we have a Mind Map, which will evolve into a spreadsheet and eventually get imported into our project tracking system

### Interesting things we are learning
* learned how to [split a screen in windows 10](https://www.digitaltrends.com/computing/how-to-split-your-screen-in-windows-10/)
* learned about https://alpha.canada.ca/en/index.html (Thanks Gray)

## Agile Metrics
This section is for those hooked on metrics. Let us know what metrics are important to you.
* Iterations: 15
* Weeks: 30
* Total Issues: [94](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/issues?scope=all&utf8=%E2%9C%93&state=all)
  * Open Issues: [88](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/issues?scope=all&utf8=%E2%9C%93&state=opened)
  * Total Closed Issues: [6](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/issues?scope=all&utf8=%E2%9C%93&state=closed)
* GCMobility on GCcollab: [135 Members](https://gccollab.ca/groups/members/2183003)
* Burn Down rate: UNKNOWN (need to estimate and calculate efforts for User Story Points)
* Current Project end Date: March, 2020
* Proposed Project End date: March 2024

## GCmobility Resources
*	[Short Deck Introducing GCmobility](https://gccollab.ca/file/view/2696019/engcmobility-introduction-dg-approved-sharedpptxfr)
*	[GCmobility Community on GCcollab](https://gccollab.ca/groups/profile/2183003/engcmobilityfrgcmobilitu00e9)
  * [Blogs and Updates](https://gccollab.ca/blog/group/2183003/all)
  * [Discussion Forums](https://gccollab.ca/discussion/owner/2183003)
* ["Enterprise Architecture for Mobility"](https://gccollab.ca/file/view/2757952/enssc-gcmobility-architecturepdffr)
*	[GCmobility Project Site](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility)
  * [Submit a new Mobility Issue](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=)
  * [Our KanBan Board](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/-/boards)
  * [Full Issue Backlog](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/issues)
