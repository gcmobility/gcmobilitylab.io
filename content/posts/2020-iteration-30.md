Title: GC Mobility Update Iteration 30
Date: 2020-05-21
Modified: 2020-05-21
Category: Blog
Tags: review, demos
Slug: gcmobility-iteration-review-30
Toc_Include_Title: false
Status: published
Summary: Iteration 30 of GCmobility focused on seeking approvals to run a usability study for mobile employees on M365. COVID-19 social issolation and telework practices has presented a unique opportunity to test out emerging enterprise solutions like M365. The use cases we want to explore are different than what an individual departments may be requiring. 

## Summary
Iteration 30 of GCmobility focused on seeking approvals to run a usability study for mobile employees on M365. COVID-19 social issolation and telework practices has presented a unique opportunity to test out emeerging enterprise solutions like M365. The use cases we want to explore are different than what an individual departments may be requiring. 

## Overview
This post represents the update for Iteration 30 of the GCmobility project.

After reading this update please take some time to complete the [GCmobility Retrospective Questionnaire]({filename}/pages/iteration-feedback.md) after you have viewed the demo and/or checked out the resources on this page. The questionnaire asks three simple questions.

GCmobility has been experimenting with M365 since the roll out of [GCcollaboration](http://gccollaboration.ca) to employees at [NRCAN](https://www.nrcan.gc.ca/). We were excited to see a growing number of departments who decided to adopt this multi-departmental [M365](https://www.microsoft.com/en-us/microsoft-365) tenant. Currently we know of [19 departments using GCcollaboration](https://wiki.gccollab.ca/GCcollaboration_Department_Listing). The fact we had 19 departms sharing a M365 deployment meant we could test various Crossboarding and interdepartmental collaboration use cases. 

Since we also know of some departments who opted out of GCcollaboration or stood up their own paralel instance, this also provided some opportunities to test out the Business to Business integration between GCcollaboration and other M365 deployments. Based on various conversations concerning architectural patterns for rolling out M365 it became clear that while decision makers were expecting certain features for the user experinece, these vary widely depending on how they roll out their instance. So far the driving business requirements seem to be driven by intra-departmental user requirements and not intra-departmental requirements such as cross boarding. Therefore, we suspect that some roll outs of M365 across the GC may result in mobility anti-patterns. 

While Microsoft has some initial resources outlining the different [features and functions on intertennant collaboration](https://docs.microsoft.com/en-us/office365/enterprise/office-365-inter-tenant-collaboration), the actual user experience may depend on how these tenants are configured. For instance, here is an article outlining some [Pros and Cons of Single Tenant vs Multiple Tenants in Office 365](https://blogit.create.pt/miguelisidoro/2019/01/07/pros-and-cons-of-single-tenant-vs-multiple-tenants-in-office-365/). GCmobility wants to test the merrits of a single tenant, like GCcollaboration and explore how it may facilitate increasing mobility among mobile workers.

As we all rush to enable our workforce while we increase teleworking and social distancing during COVID-19, we need to also ensure that new solutions being stood up meet the current and future needs of digitally enabling our organizations.

GCmobility is driven by the goals of increasing workforce mobility, decreasing crossboarding times, increasing the value proposition of hiring mobile employees, and increasing the autonomy of mobile works as well as their hiring managers and executives. However, what drives GC mobility, may not drive individual departments. For example, where an individual department may consider itself to be the enterprise, executives may be more concerned with managing their own IT solutions and ensuring information and data flows do not occure between departments. GCmobility, in contrast, is interested in exploring the use cases focused on the Government of Canada as the enterprise and how increasing the data and information flows between departments could help decrease crossboarding times and increase collaboration across the whole enterprise. Two very different perspectives of [enterprise architecture](https://en.wikipedia.org/wiki/Enterprise_architecture).

Over iterations 29 and 30 we have been planning, presenting and seeking approval to run a usability study by way of a Proof of Concept (POC). We are still waiting on a formal response and hope to have a positive report in Iteration 32.

We are still working on editing and releasing the demo video from [Iteration 29]({filename}/posts/2020-iteration-29.md)

There will be no demo video for Iteration 30.

There will be no iteration 31, due to taking some vacation.

We will be back in 4 weeks with an update on Iteration 32.

In the mean time, stay safe, keep your distance and wash your hands.

## Demonstration
This section higlights what we will cover in the Iteration Review and Demo. 

As an iteration focused on planning and 

## Site Updates: 
This section covers any major changes to the GCmobility site design, layout and content.

There are no new site updates.

## Mobility Service Updates:

The service is evolving and currently has the following components:

Service Issues by status:
* Mobility Service Issues Launched: 5
* Mobility Service Issues Open: 4
* Mobility Service Issues Closed: 1

Service Issues by Service Type:
* Data Management: 1
* MyKey: 2
* E-Mail: 1
* Misc: 1

[Learn more about the GCMobility initiative]({filename}/pages/about.md), a joint partnership between [Canada's Free Agents](https://www.linkedin.com/company/freeagents-agentslibres/about/) and [Shared Services Canada](https://www.canada.ca/en/shared-services.html).

If you are interested in joining us for future live demonstrations please [reach out to us]({filename}/pages/contact-us.md).

### Recent GCmobility Pivots:

* **Mobility Service**: Running an Alpha mobility service for mobile employees.
* **COVID-19**: How to work effectively "Off line"
* **Technology**: POC 2: 1 Phone, 1 Laptop/Tablet, 1 canonical e-mail)
* **Processes**: Move Focus from onboarding to cross-boarding
* **People**: Hyper Mobile Employees and Executives (rates > 30%)
* **Policies**: Enterprise Architecture Supporting Mobility
* **Services**: Crossboarding as an enterprise, interdepartmental, internal digital service

## Iteration Results


## Iteration Review
### Index
Check out the embeded Iteration Review video and presentation.

If you are tight on time then try these time stamped video chapters:

<!-- include timesstamped links to sections of video eg (https://youtu.be/U8Q5e5WCg8I?t=1m35s)-->

### Video

Demonstration video </br>

The Video will be posted after the demo has been recorded. We have not had a chance to record a demo for this iteration. Check back soon or subscribe to our [RSS](https://gcmobility.gitlab.io/feeds/all.rss.xml) or [ATOM](https://gcmobility.gitlab.io/feeds/all.atom.xml) feeds to receive real time updates on our site contnet.

<!--
<iframe width="560" height="315" src="https://www.youtube.com/embed/U8Q5e5WCg8I" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/1kZ7j-5MvFk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


-->

### Slide Deck
Demonstration Presentation </br>

The slide deck for this demo will be posted after it has been recorded. We have not had a chance to record a demo for this iteration. Check back soon or subscribe to our [RSS](https://gcmobility.gitlab.io/feeds/all.rss.xml) or [ATOM](https://gcmobility.gitlab.io/feeds/all.atom.xml) feeds to receive real time updates on our site contnet.

<!-- Go into slide deck, select file > publis to web > embed
Do not use share link

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vT0-21U3akusGQyYoutEan2XhEZ-bVaqRv7mRKCppHn5-f1lAunnyaFmj_C3K3T0A/embed?start=false&loop=false&delayms=3000" frameborder="0" width="640" height="389" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

-->

## News and Upcoming Events

**Iteration Schedule**

Iteration   |Start Date |Demo Date  |End Date
------------|-----------|-----------|-----------
31          | 5/25/2020 | 6/4/2020  | 6/5/2020
32          | 6/8/2020  | 6/18/2020 | 6/19/2020
33          | 6/22/2020 | 7/2/2020  | 7/3/2020
34          | 7/6/2020  | 7/16/2020 | 7/17/2020
35          | 7/20/2020 | 7/30/2020 | 7/31/2020
36          | 8/3/2020  | 8/13/2020 | 8/14/2020
37          | 8/17/2020 | 8/27/2020 | 8/28/2020
38          | 8/31/2020 | 9/10/2020 | 9/11/2020
39          | 9/14/2020 | 9/24/2020 | 9/25/2020
40          | 9/28/2020 | 10/8/2020 | 10/9/2020

**GC Mobility Community Meetups**

Schedule for 2020-2021: **ON HOLD**

# Additional Resources:

* Be Mobile with [GCmobility!](http://bit.ly/gcmobility-wiki)
* Soyez mobile avec [GCmobility!](http://bit.ly/gcmobility-wiki)

# Next Iteration
We will see you back here in 2 weeks for the next installment of our iteration reviews.

* Iteration 32 Planning will start June 8, 2020
* Next Demo/Update: May 21, 2020
