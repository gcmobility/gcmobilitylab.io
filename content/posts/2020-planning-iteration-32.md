Title: GC Mobility Iteration Planning 32
Date: 2020-06-08
Modified: 2020-06-08
Category: Blog
Tags: planning
Slug: gcmobility-iteration-planning-32
Toc_Include_Title: false
Status: draft
Summary: This post outlines the goals for Iteration 30 of the GCmobility initiative

## Intro
GCmobility is managed using the Agile Scrum of iterative delivery. Each iteration occurs over a 2 week period. The Iteration starts with a planning meeting and the Iteration ends with an Iteration review which includes a product demonstration.

## Pivot Goals
**Please understand for some employees, remote work is hard**
* [ ] Empathise with other public servants who are working "off line"
* [ ] Check in with the Free Agent family
* [ ] Assess current needs of mobile workers
* [ ] Move from wide stakeholder engagement to ensuring Free Agents are well supported to work remotely.
* [ ] Engage stakeholders through modern tools (social media and mailing lists)
* [ ] Engage Free Agent home Department IT service desks
* [ ] Connect with stakeholders through "Lean Coffee Table" sessions.

**Find a need and fill it**

* [ ] Create a "Mobility health check" questionnaire
* [ ] Create a "Mobility health check list" of resources

## Iteration Goals
This section highlights some of the specific goals we have identified for this iteration. Currently we are transitioning between two product backlogs, on Hosted on GCCode and the other hosted on Gitlab.

**GCCollaboartion.ca**
* [ ] explore solutions for connecting Free Agents across 4 home departments to collaborate on a common MS 365 Platform
* [ ] 

**Recuring Activities**
This sub-section highlights the various activities that occur regularly throughout a typical iteration. They are shared here in the spirit of transparency.

* [ ] Weekly Update for the Senior Assistant Deputy Minister of Networks, Security and Digital Services (NSDS), Shared Services Canada (SSC) (On Hold due to COVID-19)
* [x] GCMobility
    * [x] Iteration Planning
    * [x] Itertation Review
    * Open Office Hours (Tues and Thurs: 10-12, 1-3)
* [x] Service Evolution Team Meeting
* [x] GCmobility Bilat with Director of Service Evolution
* [ ] Common Desktop Operation Environment (CDOE) Working Group Meeting

**Non-Demonstration Items**

* Stakeholder Engagements
  * [ ] Engagement with Teams implementing Jarvis
    - [ ] PCH (Rescheduled)
  * [ ] Mobility Perspectives of Executives (CIO discussion)
    - [ ] Request to produce a GCmobility Deck for executives.
    - [ ] Idea for developing a placemat
* Community Participation
  * [ ] Potential effort to consolidate Free Agent projects related to HR and or HR Data into a single deck.

  **GCmobility Service Priorities**

  * [ ] Ensure all Free Agents are supported with remote working
    - [ ] Ensure all Free Agents are online and working remote
    - [ ] Create a mobility health check or checklist
    - [ ] Create cross boarding tracker

   **General Items**

  * [ ] Feature: Share plugin to use social media icons
  * [ ] Feature: D3 Dashboard
  * [ ] Feature: Comments using Disqus
  * [ ] Request to produce a GCmobility Deck for executives
  * [ ] Idea for developing a placemat
  * [ ] Review Beyond 2020 Webcast "What you need to Fuel Public Service Innovation"
  * [ ] Upskilling pilot mid-way article
  * [ ] Compiling Jarvis in C#
    - [ ] Create a new "hello world" process using Jarvis

  **Web Site @ gcmobility.gitlab.io**

  * [ ] Functionality
    * [ ] Add Disqus support for discussion
  * [ ] posts
    - [ ] Digital Upskilling
    - [ ] Iteration 20 Update
    - [ ] GCcollab Discussion Topics
    - [ ] Migrate older Content
    - [ ] How to Work "Off Line"
  * [ ] New Content
    - [ ] Mobility Data: Sources, Metrics and Gaps
    - [ ] GCmobility Office Hours and Bookings (Contact us)

    **Stakeholder Engagement**

  * [ ] GCmobility mailchimp account
  * [ ] Engagement with Teams implementing Jarvis
    - [ ] PCH (rescheduled)

  **Project Management**

  * [ ] migrate / create backlog
  * [ ] Import any issues exported from GCcode after backlog clean up
  * [ ] clean up backlog
  * [ ] Add new issues from white board

### GCcode
These issues are managed on [GCcode](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility) which is internal to the Government of Canada. Some links may only be accessible to the internal GC network during this transition time.

* [ ] Ensure all Free Agents are supported with remot working
  - [ ] Create cross boarding tracker
  - [ ] Address Parks Canada Issues.
* [ ] clean up backlog
* [ ] Add new issues from white board

### Gitlab
These issues are managed on [Gitlab.com](https://gitlab.com/gcmobility/gcmobility-project) and is accessible generally to the World Wide Web. If links are blocked, it may be due to your organizations firewall.

* [ ] migrate / create backlog
* [ ] Import any issues exported from GCcode after backlog clean up


### Site
This section highlights the iteration goals for the [gcmobility.gitlab.io site](https://gcmobility.gitlab.io) and are hosted on Gitlab.com.

* [ ] Functionality
  * [ ] Add RSS support for integration into MS Outlook and news Readers (Review Atom support)
  * [ ] Add Disqus support for discussion
* [ ] posts
  - [ ] Digital Upskilling
  - [ ] Iteration 20 Update
  - [ ] GCcollab Discussion Topics
  - [ ] Migrate older Content
  - [ ] How to Work "Off Line"
* [ ] New Content
  - [ ] Mobility Data: Sources, Metrics and Gaps
  - [ ] GCmobility Office Hours and Bookings (Contact us)

### Non Demonstration items
These items may include various activities such as recuring events, project management tasks and stakeholder engagements. These items are shared to provide insight into the various types of work that go into GCmobility, beyond the typical "product development" tasks often communicated during an iteration cycle.

**Recuring Activities**

* Weekly Update for the Senior Assistant Deputy Minister of Networks, Security and Digital Services (NSDS), Shared Services Canada (SSC)
* GCMobility
    * Iteration Planning
    * Itertation Review
    * Open Office Hours (Tues and Thurs: 10-12, 1-3)
* GCmobility Digital Upskilling Pilot
    * Planning
    * Work Sessions
* Service Evolution Team Meeting
* GCmobility Communications Check in
* GCmobility Bilat with Director of Service Evolution
* Common Desktop Operation Environment (CDOE) Working Group Meeting

**Iteration Specific Activities**

* [ ] Beyond 2020 Webcast "What you need to Fuel Public Service Innovation"


## Agile Resources
To learn more about Agile Scrum there are many resources on the web. One resource you may consider checking out are the resources offered by [Mike Cohn](https://www.mountaingoatsoftware.com/company/about-mike-cohn) at [Mountain Goat Software](https://www.mountaingoatsoftware.com/), where you can [Learn About Agile](https://www.mountaingoatsoftware.com/agile).
