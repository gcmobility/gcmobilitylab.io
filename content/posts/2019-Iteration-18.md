Title: GC Mobility Update Iteration 18
Date: 2019-12-09
Modified: 2020-02-27
Category: Blog
Tags: gcmobility, demos, iteration-18
Slug: gcmobility-iteration-review-18
Toc_Include_Title: false
Status: published
Summary: Iteration 18 was focused on ensuring we completed the iteration with a recorded [Iteration Demo and Review](http://bit.ly/gcmobility-iteration18-video) ([Static Presentation](http://bit.ly/gcmobility-iteration18-Presentation)). This marks 3 iterations in a row that completed with demonstration

*UPDATE*: This content was originally published in the [GCmobility group within GCcollab](https://gccollab.ca/groups/profile/2183003/engcmobilityfrgcmobilitu00e9). "Hosted by the Government of Canada, GCcollab is designed to connect you with the information and people you need to work with across Canadian universities, colleges and all levels of government." ([GCcollab Terms and Conditions](https://gccollab.ca/terms)). Some links may lead to internal resources.

## Overview
This post represents the update for Iteration 18 of the GCmobility project.

The [GCMobility initiative](http://bit.ly/gcmobility-wiki) is a joint partnership between [Canada's Free Agents](http://bit.ly/cfa-gcpedia) and [Shared Services Canada](http://bit.ly/ssc-web)


Iteration 18 was focused on ensuring we completed the iteration with a recorded [Iteration Demo and Review](http://bit.ly/gcmobility-iteration18-video) ([Static Presentation](http://bit.ly/gcmobility-iteration18-Presentation)). This marks 3 iterations in a row that completed with demonstration . Our goal is to maintain our cadence of the Agile Scrum method with demonstrations of our progress after each iteration. Some items covered in this demonstration occured earlier in this initiative and are being shared with the community now. Over the next several iterations we will be covering several other deliverables that have not been demonstrated to the community. In addition to the recorded demo we also posted the static slide deck, which has various interactive elements you can click on and review yourself.

Until now, the iteration demos have been presented to a small and internal group. As we mature the agile processes with our stakeholders it is our hope to have interested stakeholders join us for the next demo live by webex. If you are interested in joining us for future live demonstrations please [reach out to us](mailto:david.sampson@canada.ca).

As we continue to stream line the content generation and sharing for GCmobility, we have depricated the "Agile Scrum Update" section from this blog and moved it to join the other content on the [GCmobility Wiki](https://wiki.gccollab.ca/GCmobility#Iteration_Scrum_Updates). For those who appreciated the bullet point form approach to this section, we feel you will still be well served by way of the demonstrations by providing a couple of slides that are high level bulleted overviews of what will be presented throughout the demo. As usual we always appreciate your feedback.

Note that we have updated our Vision and Objectives in this iteration. Check out the video and presentation to learn more about our recent pivots.

In the Iteration Review and Demonstration you will learn more about:
* Various non-demonstrable items such as
  * Reporting on stakeholder engagements
  * Highlighting recent research activities
  * Community meetups
  * GCmobility Leadership
* The core of the demonstration addressed the following issues
  * GCmobility Pitch Updated: DG (Issue #[34](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/issues/34), #[20](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/issues/20))
  * Partnership Briefing: SSC ADM Network Security and Digital Services (NSDS) (Issue #[97](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/issues/97))
  * Engagement and Communications Strategy (Issue #[68](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/issues/68))
  * Stakeholder Intake Survey Updates (Issue #[61](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/issues/)61, #[56](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/issues/56))
  * New Look and Feel to GCCollab and GCmobility Group
  * Presentation to PCO: Public Service Renewal – Beyond 2020 (Issue #[96](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/issues/96))
  * Presentation to Free Agent Steering Committee (Issue #[51](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/issues/51), #[57](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/issues/57))
  * Enterprise Architecture Introduction (Issues #[35](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/issues/35), #[59](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/issues/59), #[60](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/issues/60))
  * Zotero: Collaborative research in Mobility


We have also completed our planning session for Iteration 19, so click through to the [planning discussion thread](http://bit.ly/gcmobility-planning) for updates on what is planned for this upcoming iteration.

Thanks again for your continued support and encouragement. We look forward to your comments on the demo format and how we can improve the experience while keeping formalities light and agile.

Be Mobile with [GCmobility!](http://bit.ly/gcmobility-wiki) / Soyez mobile avec [GCmobility!](http://bit.ly/gcmobility-wiki)


## Iteration Scrum Update Format.
This section of the iteration update has been deprecated and moved to the [GCmobility Wiki](https://wiki.gccollab.ca/GCmobility#Iteration_Scrum_Updates)

## Other Resources
For more information please check out the [GCmobility wiki page](http://bit.ly/gcmobility-wiki).
