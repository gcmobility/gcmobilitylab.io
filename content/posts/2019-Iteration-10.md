Title: GC Mobility Update Iteration 10
Date: 2019-08-15
Modified: 2020-02-27
Category: Blog
Tags: gcmobility, demos, iteration-9
Slug: gcmobility-iteration-review-10
Toc_Include_Title: false
Status: published
Summary: Iteration 10 of GCmobility focused on on stakeholder engagement. We are working to connect with various HR mobility projects to learn about their pain points. As we presented in our introductory slide deck, there are many enablers to mobility and each enabler currently faces challenges to enable mobility.

*UPDATE*: This content was originally published in the [GCmobility group within GCcollab](https://gccollab.ca/groups/profile/2183003/engcmobilityfrgcmobilitu00e9). "Hosted by the Government of Canada, GCcollab is designed to connect you with the information and people you need to work with across Canadian universities, colleges and all levels of government." ([GCcollab Terms and Conditions](https://gccollab.ca/terms)). Some links may lead to internal resources.

# GC Mobility Update

## Overview
Please accept this post as the update for Iteration 10.

That's right, 10 iterations and 20 weeks exploring issues around supporting a GC mobile workforce.

Currently our focus is on stakeholder engagement. We are working to connect with various HR mobility projects to learn about their pain points. As we present in our introductory slide deck, there are many enablers to mobility and each enabler currently faces challenges to enable mobility.

We need your help to connect us to the right stakeholders in the mobility space. Here is how you can help:
* Join the [GCmobility Community](https://gccollab.ca/groups/profile/2183003/engcmobilityfrgcmobilitu00e9)
* Follow the [GCmobility Blog Posts](https://gccollab.ca/blog/group/2183003/all)
* Contribute to one the [mobility themed discussion threads](https://gccollab.ca/discussion/owner/2183003)
* Share Mobility resources in our [Bookmarks section](https://gccollab.ca/bookmarks/group/2183003/all)
* Attend our monthly [GC Mobility Leaders Meetups](mailto:david.sampson@canada.ca?subject=Monthly GC Mobility Leaders Meetups&body=I would like to be added to the invite list for the monthly GC Mobility Leaders Meetups.)
* Share this blog post with your colleagues

Iteration 11 will be an empty iteration. The numbers will increment, but no work is planned due to annual leave for the team of 1 (me).

We will see you back here for Iteration 12.

### Our Vision By 2024:
Canadian Federal Public Servants can work
From anywhere,
At Anytime
For any department and beyond

### GCmobility Objectives
The goal of GCmobility is to reduce the onboarding and offboarding time required by Free Agents and other mobile federal employees as they start/end any of their exciting and often short term opportunities.

## Iteration Goals
### To Do list
This iteration we set out to tackle these issues:
*

### Completed
Throughout this iteration we worked on and completed these issues:
*

### Deferred
Not all issues are able to be completed in each iteration. These ones have been deferred:
*

## Iteration Scrum Questions
### What have we been working on
* Stakeholder Engagement
  * Met with a number of collaborators to discuss the evolving GCmobility stakeholder engage strategy (aka. brainstorm in document form)
  * Met with reps from TBS Talent Cloud's Block Cert and leaders in the person's with disabilities space
  * Interviewing Free Agent Talent Managers and Leadership Team
  * 3rd Monthly Meetup for GC Mobility community: This turned out to have an accessibility theme
  * Met with AAACT, the accessible technology group at SSC to explore common goals
  * Met with Beyond2020 to explore collaborations
* Communications
  * received a "yellow" light for stakeholder engagement. Management wants to know the who/how/what. So we are co-designing this piece with you our community, you!
  * Started jotting ideas about communication strategy down (part of Stakeholder engagement strategy)
  * Made this GCmobility group open and public to all. This will decrease my admin time to approve join requests. Opener is easier.
* Project management
  * With the recent changes to webex, we took the time to export all videos (including GCmobility) for offline access
* IT Issues
  * Never a shortage of IT issues. I have had password issues for months. They are now solved (Thanks Lael)
  * 1 of 2 virtual machines for demonstration are working (SSC).
  * PSPC image still has issues. need to work with service desk. Will wait until after upcoming leave.
* Architecture
  * Published first version of our "Enterprise Architecture for Mobility"
  * Took a stab at creating a business model canvas to describe

### What will We work on
* Stakeholder Engagement
  * Engaging with Free Agent Steering Committee in September
* Communications
  * Updating the GCmobility deck based on some recent feedback

### What blocks are we facing
* Complex problems with lots of people and projects

### New Issues added to the backlog
* New e-mail system might replace @canada.ca in 1-2 years. Trying to confirm.
* Some departments rolling our URL scrubbing for E-mails. This might make collaboration more challenging
* GCmobility needs to develop value propositions for more stakeholder groups to get buy-in

### Interesting things we are learning
* Security: Security is appearing as a primary pain point for talent managers, hiring managers, Free Agents as well as HR practisioners. We are not sure what our investigations will turn up but we have engaged TBS Security Policy team in September.
* AAACT adopts the concept of "accessibility by design" perhaps mobility should be treated the same
* User Centred Design: There are many initiatives that can support and may impact mobility. Not all of these initiatives are applying User Centred Design principles. UCD takes time and energy. GCmobility will continue to keep users at the centre of its process as best as we (aka me) can.


## Agile Metrics
This section is for those hooked on metrics. Let us know what metrics are important to you.
* Iterations: 10
* Weeks: 20
* Total Issues:
* Total Closed Issues:
* Burn Down rate: UNKNOWN (need to estimate and calculate efforts for User Story Points)
* Current Project end Date: September 27, 2019
* Proposed Project End date: March 2024


## GCmobility Resources
*	[Short Deck Introducing GCmobility](https://gccollab.ca/file/view/2696019/engcmobility-introduction-dg-approved-sharedpptxfr)
*	[GCmobility Community on GCcollab](https://gccollab.ca/groups/profile/2183003/engcmobilityfrgcmobilitu00e9)
*	[GCmobility Project Site](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility)
  * [Submit a new Mobility Issue](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=)
  * [Our KanBan Board](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/-/boards)
  * [Full Issue Backlog](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/issues)
