Title: GCmobility on GCcollab
Date: 2020-02-05
Category: Blog
Tags: gccollab, gcmobility
Slug: gcmobility-on-gccollab
Status: published

This content on this site is supported by the GCmobility community hosted on [GCcollab](https://gccollab.ca)

According to the GCcollab [Terms and Conditions](https://gccollab.ca/terms), "Hosted by the Government of Canada, GCcollab is designed to connect you with the information and people you need to work with across Canadian universities, colleges and all levels of government."

The source code of this site is at <https://gitlab.com/pages/pelican>.

Learn about GitLab Pages at <https://pages.gitlab.io>.
