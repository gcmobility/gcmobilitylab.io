Title: Iteration Dashboard
Date: 2020-02-26
Modified: 2020-04-14
Category: Data
Status: published
Tags: dashboard
Slug: iteration-dashboard
Toc_Include_Title: false
Javascripts: https://d3js.org/d3.v4.min.js, burndown.js
Stylesheets: burndown.css
Summary: This post is that start of GCmobility's experimentation with dynamic data visualization in support of communicating mobility within the Federal Public Service. This post also demonstrates the integration of Data Driven Documents (D3) with the Pelican Static Site Generator. We are using the pelican_javascript plugin developed by Mortada Mehyar

## Iteration 26 Dashboards
### Project Chart

Key Performance Indicators  | Current Metric    | Previous Metric
----------------------------|-------------------|----------------
**Project Metrics**         | **-------------** | **-----------**
  Iterations                  | 27                | 26
  Total issues                |                   |
  Open issues                 |                   |
  Closed Issues               |                   |
  Community Meetups           | 7                 |
  Mobility rating             | NA                |
  Project End Date            | September 2020    | September 2020
  Proposed End Date           | March 2025        | March 2025
**Engagement Metrics**      | **-------------** | **-----------**
GCcollab Community (members)| 191               |189
Community Meetup members    | 25                | 25
GCmobility ADM/DM Steering Committee    | 0     | 0
GCmobility Partners         | 2                 | 2
**Stakeholder Inventory**   | **-------------** | **-----------**
total                       |                   |
Active                      | 83                | 83
Engaged                     | 138               | 138
Potential                   | 77                | 77
Opted-Out                   | 1                 | 1
**Site Analytics**          | **-------------** | **-----------**
Site Pages                  | 10                | 10
Site Posts                  | 28                | 25
Site Discussion             | 0                 | 0
**Web Ananlytics**          | **-------------** | **-----------**
web_users                   | 37                | 4
web_new_users               | 32                | 3
web_sessions                | 50                | 6
web_page_views              | 91                | 11
web_bounce_rate             | 70                | 50

**Resources:**

* [Download Project Data]({static}/data/burndown.csv)
* [Download Stakeholder Data]({static}/data/stakeholders.csv)
* [Download Engagement Data]({static}/data/web-metrics.csv)

## Iteration 26 Dashboards
### Project Chart

Key Performance Indicators | Current Metric | Previous Metric
---------------------------|----------------|----------------

### Stakeholder Chart

Key Performance Indicators|Current Metric | Previous Metric
--------------------------|---------------|----------------

### Engagement Chart

Key Performance Indicators|Current Metric | Previous Metric
--------------------------|---------------|----------------


## Iteration 25 Dashboards
### Project Chart
![Iteration 25 project Chart]({static}/charts/iteration-25-project-chart.png)
[View Full Chart]({static}/charts/iteration-25-project-chart.png)
[Download Project Data]({static}/data/burndown.csv)

### Stakeholder Chart
![Iteration 25 project Chart]({static}/charts/iteration-25-stakeholder-chart.png)
[View Full Chart]({static}/charts/iteration-25-stakeholder-chart.png)
[Download Stakeholder Data]({static}/data/stakeholders.csv)

### Engagement Chart
![Iteration 25 project Chart]({static}/charts/iteration-25-engagement-chart.png)
[View Full Chart]({static}/charts/iteration-25-engagement-chart.png)
[Download Engagement Data]({static}/data/web-metrics.csv)

### Slide deck
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTicRCPJmRgwrrnSVYvlOd761xEuUyCYdIRrX7Ayek7pIgw3LM80zHhtSXkeqpGTQjKB7vECPEgIoCb/embed?start=false&loop=false&delayms=3000" frameborder="0" width="760" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

<!-- <div id="burndown"></div> -->



## To Do
This is a running To Do list for improving this post:

* [ ] Develop Iteration burndown chart using D3 and Javascript
* [ ]
