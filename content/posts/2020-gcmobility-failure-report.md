Title: GCmobility Impact and Failure Report 2020: Call For Submissions
Date: 2020-02-06
Modified: 2020-02-06
Category: Blog
Tags: gcmobility, 2020, reports
Slug: gcmobility-impact-and-failure-report-2020
Status: published

# Executive Overview
Welcome our first GCmobility Impact and Failure Report. This section provides a brief overview of the GCmobility Failure Report

## Who is the Report for?
The Impact and Failure Report is meant to address several audiences:

* *Employees*: The primary audience for this report are for employees who want to share their projects and discoveries that have made a impacts or who have had experienced failures personally while trying to be mobile.
* *Hiring Managers*: We want to hear from hiring managers who have had success, impacts or failures when trying to hire mobile employees. This could be the challenges of managing remote workers, establishing telework agreements or the pains and delays involved with building a new team from scratch, trying to recruit talent from other departments.
* *Executives*: Deputy heads and their executive teams have huge mandates to deliver on, and they need employees to help them achieve certain goals and execute on their vision. We want to hear from executives on their experiences moving around government. Is it easier as an executive to move around, harder, or just different. For executives who own the various corporate services involved in cross boarding, what challenges do your staff face, and what solutions have been put in place that have delivered meaningful impacts.

## What Is In an Impact and Failure report
To learn more about failure reports please see the associated section below.

* *Failure Stories*:
  * What mini or epic fails have an enduring meaning on your experience as a public servant?
* *Impact Stories*:
  * What initiatives have you done or been involved with that resulted in a measurable or meaningful impact on yourself or other federal employees that made it easier to move throughout government?

## When Can I Provide My Submission?
NOW! We will release something by the end of March 2020. Start jotting down your ideas and grab a copy of our template (coming soon). We are accepting drafts and proposals as of now. Check the timelines below.
## Where Can I Learn More
Keep reading this article, and check out the [GCmobility Impact and Failure Reports Page]({filename}/pages/gcmobility-impact-and-failure-reports.md) for general guidance and additional resources.
We plan to communicate the release of this report in a number of places including:

* [GCmobility Website](https://gcmobility.gitlab.io)
* [GCmobility Community Group](https://gccollab.ca/groups/profile/2183003/engcmobilityfrgcmobilitu00e9)
* [GCmobility Wiki](https://wiki.gccollab.ca/GCmobility)

## Why an Impact and Failure report
We believe there is value in learning from our failures and celebrating our impacts. This report is a chance for all stakeholders to share in telling their stories to a shared audience so we can empathize with our users and provide evidence to decision makers that further actions are still required.
## How To Submit your Stories
Check out the GCmobility Failure Report page for information on Failure Reports in general and guidelines for this particular report.

# GCmobility Impact and Failure Report 2020: Call For Stories

Welcome to the first open call to all Canadian Federal Public Servants to share their experiences moving around within the Canadian Government. If you consider yourself a "mobile" employee we especially want to hear from you as you move around government more than most. To our knowledge this is the first impact and failure report to look at mobility within the Canadian Federal Public Service.  

The Government of Canada is a mobile employer. What this means is that there are opportunities as a public servant to be hired (on boarding), promoted (career mobility), and retired (off boarded). While working as a public servant there are many opportunities to also move laterally between projects as well as between departments (cross boarding).

There is various data and statistics that measure various types of mobility within government. For example how many people are hired and retired each year. We also know the mobility rates of certain job classifications at various snapshots in time. What these numbers don't provide is a sense as to the user experience of public servants as they move around government. This report aims to provide a glimpse into the experiences individuals have had trying to move around government.

The GCmobility Impact and Failure Report is made by Canadian Federal Public servants, for Canadian Federal Public Servants. We are looking for submissions from all classification levels, from ADM executives down to the most junior of public servants. If you are retired from the public service or are a student who contributed your time and energy to serving Canadians through the public service, please reach and share your stories.  

The management of onboarding and offboarding processes are managed at the department level. As might be expected, not all departments and agencies within the federal government are created equal, so we also expect individual experiences will also vary. While some departments have mature on boarding "frameworks" and processes, other departments place the majority of the process on hiring managers and employees themselves. Some departments have also established VIP services for their executives to make their transitions between departments smoother.

Cross boarding is the combined activities required to off board from one department and on board to a new department. Currently there is limited to no collaboration to coordinate these activities as public servants move between departments. As a result, there can be conflicts between the offboarding processes of one department and the on boarding of another department. In some cases, such as security and MyKey (Entrust secured credential management), other service departments might be included in the mix.

GCmobility is looking for mobility stories of two types under many different themes. As the name of the report suggests, GCmobility are looking for stories about impacts and failures. For those familiar with the concept of a failure report, popularized by Engineers Without Borders (Canadian Chapter), it is important to highlight not only our failures but more importantly our lessons learned from those failures. While this report certainly wants to highlight mobility failures we also wanted to shed light on the work that many public servants have worked on and continue to work on to address some issues with mobility.

Failure stories are meant to openly acknowledge and admit that something failed. As humans we fail a lot. While learning to walk we often fell to our knees. When learning to swim we sometimes sank like a rock. But many of us learned from these failures and went on to be bipedal and enjoy hot summer days in and around Canada's many water ways, beaches and lakes. Although failure stories will shed light on what went wrong, they are also meant to provide a diagnosis and highlight lessons learned so others can learn from our epic fails.

Impact stories are an opportunity to share all the great work that is going on in the mobility space. Some may argue that strong impact stories are born from constructive failure stories. To us, they are two sides to the same coin. Impact stories are more than just highlighting all the great work that is going on. They are also stories of battles that were hard to win, unexpected discoveries, novel approaches and innovation. In the current era of Digital Government and Digital Services, these stories will likely highlight a focus on designing processes that work for a positive human experience and moving services beyond a digitized process to meet policy requirements.

Since this is the first failure report we know of we are keeping the themes broad. There are many themes to choose from, however if you have a story to share that does not fit in these themes please feel free to propose them. The themes are outlined bellow and generally follow the processes that are followed when cross boarding between departments. While this report does not aim to focus on stories of being hired, fired, or retired, if the story is appropriate we will consider it.   

Timelines for submission are tight (what else did we honestly expect from a government project?). So if you are interested in submitting an impact or failure story start jotting your ideas down now and follow the timelines listed below. Our goal is to publish something by the end of March 2020. This could range from an "outline" and "table of contents" to a slide deck of short stories or a more "report" like something. We are pretty sure that the final result will not be a hard cover, string bound coffee table book highlighting pan-Canadian art and poems, with an accompanying podcast series. However if you can make this happen let us know.

Please take some time to review the other sections listed below and visit this page often. This page will likely be updated frequently as we iteratively learn what is needed to produce a failure report.

Thanks for helping us build a collection of stories that can be used by future efforts to effectively mobilize the Canadian Public service. Lets see if this report will itself become a failure to learn from or an impact that helps move mobility forward.




# Impact and Failure Themes

* coming soon



# Timelines
These timelines are ambitious targets and may slip. We generally know where we are and where we want to go. The actual journey is quite uncertain. Check back often as we stumble through this process.

* [ ] February 6, 2020: Public Call for Stories
* [ ] February ?, 2020: Draft Report Outline
* [ ] ???? : Draft report (language of submission)
* [ ] ???? : Revised Draft for comment
* [ ] ???? : Translation (???)
* [ ] ???? : Layout and Design
* [ ] March 23, 2020: Publish Whatever we have (minimum viable product)

# Volunteers Needed
So we already admitted that we have a team of only 1 to pull off this report. Some would say that not having a large team with diversified skills and a large O&M budget has set the stage for failure. I am glad we all agree that a larger team, an established budget and diversified skills would increase the potential success of this first report. So please join us.

We need help with:

* Peer reviewers
* Screening submissions
* Editorial support
* Design and Layout
* Communications
* Project management
* Executive Sponsorship
* Anything that might help.
