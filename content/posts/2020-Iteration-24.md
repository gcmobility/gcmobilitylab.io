Title: GC Mobility Update Iteration 24
Date: 2020-02-27
Modified: 2020-02-27
Category: Blog
Tags: gcmobility, demos
Slug: gcmobility-iteration-review-24
Toc_Include_Title: false
Status: published
Summary: Iteration 24 of GCmobility focused on some more stakeholder engagement, some self reflection at the half way mark of the GCmobility Digital Upskilling project and continued efforts to work in the open through our new Gitlab pages site.

## Overview
This post represents the update for Iteration 24 of the GCmobility project.

[Learn more about the GCMobility initiative]({filename}/pages/about.md), a joint partnership between [Canada's Free Agents](https://www.linkedin.com/company/freeagents-agentslibres/about/) and [Shared Services Canada](https://www.canada.ca/en/shared-services.html).

We continue our trial of using [YouTube as a hosting platform for our iteration videos](https://www.youtube.com/channel/UC-mL8ozVrg2pq-x4Cd2ZXtQ/) to broaden our stakeholder audience. As we contineau to mature the agile processes with our stakeholders we continue to invite interested stakeholders to join us live via webEx. If you are interested in joining us for future live demonstrations please [reach out to us]({filename}/pages/contact-us.md).

## Site Release Update

### New Features
Welcome back to our continuously evolving site hosted on [GitLab Pages]({filename}/posts/2020-gcmobility-on-gitlab-pages.md). Here are some of the latest feature changes to our site:

* Added some new functionality through Plugins
    * neighbors: Provides links to previous and next articles
    * deadlinks: looks for and reports deadline during build process
    * share_post: Provides links to popular social media share sites without proprietary trackers
    * sitemap: generates a sitemap for submission to search engines

### New Pages
Here are some of the latest static content changes (aka pages) to our site:

* Posted some historical blog posts for iterations 1-19. [Start Here]({tag}demos)
* Added a contact form to the [contact us]({filename}/pages/contact-us.md) pages
* Information page for [GCmobility Impact and Failure report]({filename}/pages/gcmobility-impact-and-failure-reports.md)
* [GCmobility Objectives]({filename}/pages/gcmobility-objectives.md)
* [GCmobility Vision]({filename}/pages/gcmobility-vision.md)
* [Privacy Policy (DRAFT)]({filename}/pages/privacy-policy.md)

### New Posts
Take a look through the [latests blog posts](https://gcmobility.gitlab.io/category/blog.html) to see what else is new.

## Iteration Review
Please take a few minutes after watching the video or checking out the slide deck to [provide us some feedback on this iteration]({filename}/pages/iteration-feedback.md).

### Index
Check out the embeded Iteration Review video and presentation.

If you are tight on time then try these time stamped video chapters:

* Start: [0:07](https://youtu.be/U8Q5e5WCg8I?t=7)
* About GCmobility:
    * Why GCmobility: [1:14](https://youtu.be/U8Q5e5WCg8I?t=1m14s)
    * Problem Statement: [1:48](https://youtu.be/U8Q5e5WCg8I?t=1m48s)
    * Our Vision: [2:05](https://youtu.be/U8Q5e5WCg8I?t=2m05s)
    * Value Proposition: [2:36](https://youtu.be/U8Q5e5WCg8I?t=2m36s)
* Dashboards:
    * Project Metrics: [2:37](https://youtu.be/U8Q5e5WCg8I?t=2m37s)
    * Stakeholder Metrics: [2:51](https://youtu.be/U8Q5e5WCg8I?t=2m51s)
    * Engagement Metrics: [2:53](https://youtu.be/U8Q5e5WCg8I?t=2m53s)
* Pivots: [2:58](https://youtu.be/U8Q5e5WCg8I?t=2m58s)
* Non Demonstration items: [3:41](https://youtu.be/U8Q5e5WCg8I?t=3m41s)
* Up Skilling Pilot Update: [4:42](https://youtu.be/U8Q5e5WCg8I?t=4m42s)
    * Week 4: [5:02](https://youtu.be/U8Q5e5WCg8I?t=5m02s)
    * Week 5: [7:14](https://youtu.be/U8Q5e5WCg8I?t=7m14s)
    * Coming Up Next: [10:19](https://youtu.be/U8Q5e5WCg8I?t=10m19s)
* Upskilling Pilot Overview: [11:52](https://youtu.be/U8Q5e5WCg8I?t=11m52s)
* Demonstration Items:
* Site Update - gcmobility.gitlab.io: [13:11](https://youtu.be/U8Q5e5WCg8I?t=13m11s)
* News and Upcoming Events: [25:26](https://youtu.be/U8Q5e5WCg8I?t=25m26s)
* Engage The GCmobility Community: [26:25](https://youtu.be/U8Q5e5WCg8I?t=26m25s)
* Iteration Feedback: [27:15](https://youtu.be/U8Q5e5WCg8I?t=27m15s)


# Iteration Review Video

Demonstration video <br>

<iframe width="560" height="315" src="https://www.youtube.com/embed/WHywzqAvFjc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# Iteration Review Slide Deck

Demonstration presentation </br>

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRal-KxAKem0__t51nmC8vE4CgDkbgmQkDYzrv645mP00wBK0k1VxKhc-rJuiC0Pw/embed?start=false&loop=false&delayms=60000" frameborder="0" width="560" height="315" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

# Next Iteration
*Iteration Planning*: March 2, 2020
*Iteration Demo*: March 12, 2020

We will see you back here in 2 weeks for the next installment of our iteration reviews.

Be Mobile with [GCmobility!](http://bit.ly/gcmobility-wiki)
Soyez mobile avec [GCmobility!](http://bit.ly/gcmobility-wiki)
