Title: GC Mobility Update Iteration 13
Date: 2019-09-26
Modified: 2020-02-27
Category: Blog
Tags: gcmobility, demos,, iteration-13
Slug: gcmobility-iteration-review-13
Toc_Include_Title: false
Status: published
Summary: Iteration 13 of GCmobility focused on completing all the paperwork required to continue working on GCmobility, at least until March 2020. By April, my goal is to have an initial partnership agreement in place between the three home departments of Canada's Free Agent Program (NRCAN, TBS, TC) and SSC. So if you are willing to come along for the ride I look forward to continue leading GCmobility at least until March.

*UPDATE*: This content was originally published in the [GCmobility group within GCcollab](https://gccollab.ca/groups/profile/2183003/engcmobilityfrgcmobilitu00e9). "Hosted by the Government of Canada, GCcollab is designed to connect you with the information and people you need to work with across Canadian universities, colleges and all levels of government." ([GCcollab Terms and Conditions](https://gccollab.ca/terms)). Some links may lead to internal resources.


## Overview
This post represents the update for Iteration 13 of the GCmobility project.

The [GCMobility initiative](https://gccollab.ca/groups/profile/2183003/engcmobilityfrgcmobilitu00e9) is a joint partnership between Canada's Free Agents and Shared Services Canada

Wow! **Today did not go at all as I had planned**. But I am excited to get this update pumped out to you folks. I bet you'll find some editorial errors in my excitement :)

First, as a Free Agent, our gigs tend to be short. Often 6-18 months. The Free Agent agreement for GCmobility started in April for a period of 6 months to see what we could learn. Today I am happy to report that all the paperwork has been completed to continue working on GCmobility, at least until March 2020. By April, my goal is to have an initial partnership agreement in place between the three home departments of Canada's Free Agent Program (NRCAN, TBS, TC) and SSC. So if you are willing to come along for the ride I look forward to continue leading GCmobility at least until March.

Next, I have to say **Thank you to the community for helping us reach even more stakeholders**. In our Iteration 12 update we asked you to continue connecting us with stakeholders and spreading the word about mobility. You did not disappoint. We now have 111 members in the community (that's a neat number). I wonder how long it will take to get to 200, 300 or 600? How many stakeholders SHOULD we reach anyway?

**In this update we will cover**:
* How do you as a stakeholder want to receive iteration updates?
* What's happening in the discussion forums
* We share progress on our interviewing of Canada's Free Agents support team!
* Lets look at what it takes to gather some mobility data using Participatory Action Research (PAR).
* To get more data and feedback we need more views, but what should the target stakeholder community size be for GCmobility?
* GC Mobility Community Meetup for October

### Iteration Updates
I have been thinking a lot lately about **what format you might find most useful for Iteration Updates**. In this fast paced knowledge economy we have so many options it is almost overwhelming. Some folks might like a long narrative like the beginning of these updates have seemed to become. Some folks might like the shorter 5 question for mat I add to the end of these updates. Internally I actually add GCmobility as part of a dashboard of multiple projects, and that is super short. Or maybe people want to plug into to a 5 minute verbal brief, or maybe they want a long form podcast. So far it has been a mix of narrative and the five questions of scrum below. What do you want? What would most serve you? What would best serve the community? Either way, we want to hear from you in the comments.

### Discussion Forums
The first three part of a multi part discussion series unpacks GCmobility concepts one at a time. THis approach aims to generate conversation while providing helathy debate or confirmation of asumptions that we are making about mobility. We look forward to engaing you in each of these discussions
* [Why the GCmobility Initiative Matters (Part 1)](https://gccollab.ca/discussion/view/2988160/enwhy-the-gcmobility-initiative-matters-part-1fr)
* [The GCmobility Context (Part 2)](https://gccollab.ca/discussion/view/2988899/enthe-gcmobility-context-part-2fr)
* [The GCmobility Problem Statement (Part 3)](https://gccollab.ca/discussion/view/2994859/enthe-gcmobility-problem-statement-part-3fr)

Thanks to those who al already providing comments in the threads.

### Free Agent Engagement
As **Agile Scrum tends to be fast paced and constantly adapting** to the changing landscape this iteration was not different. We finished up a series of interviews with the Free Agent support team including 4 Talent Managers, the business manager, the program designer, and the program talent development designer. This group helped map out the typical steps Free Agents follow as we jump from Gig to Gig. This week was our regular (quarterly-ish) Free Agent Training session here in the NCR. Most of the NCR and regional Free Agents were in town to train, chat, catch up, and collaborate. GCmobility took advantage of this opportunity to gather data on the pain points

<p><img alt="Journey Mapping Before: Image of  white sheets of paper without any red or green dots on them." height="175" src="https://gccollab.ca/file/download/3035944" style="width: 700px; height: 175px;" width="700" /></p>

Once the full process was posted on the wall, it became apparent that no matter what data we would find, there is something wrong with the fact that at least 27 Big processes need to occur for most Free Agents any time they move. This knowledge leads us to think some Government Leaning can probably be done.

<p><img alt="Journey Mapping During: Image of people staring at white sheets of paper while placing red and green dots on them." height="525" src="https://gccollab.ca/file/download/3035853" style="width: 700px; height: 525px;" width="700" /></p>

Participatory Action Research, like "Dotmocracy" is a quick and great way to gather data to find signals amongst the noise. We also provided some BLANK sheet where stakeholders could add MISSING steps. And sure enough we had a new step added. For each step stakeholders identified which were the most painful or least painful and what was the minimum or maximum time each step took.

<p><img alt="Journey Mapping After: Images of white sheets of paper with red and green dots." height="216" src="https://gccollab.ca/file/download/3035763" style="width: 700px; height: 216px;" width="700" /></p>

And there we go, by the end of the day we have some data. No analysis yet, or at least ready to share, however I look forward to sharing the results soon.

Are you curious about what the steps are? Are you curious what other Participatory Action Research we might do or want to learn more about PAR? Are you a manager of an innovative HR problem focusing on mobility? Then lets talk, we would like to run this activity for more mobile public servants. Drop us a comment below.

### GCmobility Community Size
**What is the significance of the size of a community?**. Why are we interested in increasing the size of our community? Well, community members are our primary stakeholders, this includes hiring managers wanting to hire unique talent, mobile employees wanting to move often and quickly to share their talents, program designers innovating various HR models wanting to test new approaches and of course, department heads wanting to quickly mobilize talent to address their continually evolving mandate. Also, lets not forget all those folks who support mobile employees including: Security officers, HR and classification staff, IT service desks and many, many more we have not learned about. But why are numbers important? Because, if we seek input from only a small pool of stakeholders we might not hit the mark. If we only ask decision makers for their perspective we only see the problem through one lens. So we need a diverse selection of views and input.

**The power of 10's in community building**. If you have built a community before, especially an online community, you may have noticed what I call the **power of 10's**. This pattern starts to emerge as a community reaches the 100 member mark. The power of 10's suggest that for a group of 100 members, you will have 90 members that observe from a distance, 10 members that respond to existing posts and 1 member that will create new posts and content for others to comment on or observe from a distance. Currently we have 111 members, so lets look at how this pattern might emerge. The GCmobility group has 6 blog posts (by one person), 13 discussion posts (by 5 separate people) and 15 replies to those discussions (of which 7 individuals had not written a post). So the 111 members, leads to approximately 10 active engagers, 5 active contributors, 1 regular contributor. These numbers are actually really good in my experience. However, in order to have 5 regular contributors, the GCmobility community might need to grow to about 500 or so. Another way of looking at community size is to consider the population as a whole. But surely we don't need to engage all public servants to learn what is currently wrong and what could potentially fix the situation.

**If the Federal Public Service is to be representative of the Canadian Population**, lets consider "[Randstad Canada’s Workforce 2025 report](http://content.randstad.ca/hubfs/workforce2025/Workforce-2025-Randstad-Part1.pdf), where nontraditional workers (i.e., contractors, consultants, remote and/or freelance workers) currently make up 20 to 30 per cent of Canada’s workforce." ([IT World](https://www.itworldcanada.com/article/is-the-gig-economy-right-for-your-business/415036)).  So lets consider for a moment, a public service of the future where up to 30% of us are "mobile" workers. Given a current public service population of approximately 288k, means that approximately 86k of us would be regularly mobile, as internal consultants, working gigs, working remotely, teleworking etc. So what size of community might we aim for? 5%, 10%? Well, the end goal of GCmobility is to find ways (Technology solutions?) to decrease the onboarding and offboarding times of mobile workers (eg Free Agents) and we are exploring which metrics are key to measure mobility and mobility efficiency. But the question will still stand as to how many people's experiences we will need to measure or at least consult to know if we are moving the metre stick.

To determine a target community size, **lets compare a User Experience (UX) approach vs an approach to measure statistical opinions**. There are many schools of thought out in the wild of how many users need to be observed to get good UX intel. The general consensus is that the as you increase the number of test users, you get diminishing rates of return. [This article  from NN Group suggests](https://www.nngroup.com/articles/how-many-test-users/) that usability testing can be done, "With 5 users, you almost always get close to user testing's maximum benefit-cost ratio". Noting Quantitative Studies (20 users), Card Sorting (15 users) and Eyetracking (39 Users) as exceptions.  So, OK, the UX studies approach may not be directly comparable to building a community, but lets pick a number between 5 and 40 members on the low end. On the statistical side lets look at what a [statistical sample population calculator suggests](https://www.surveysystem.com/sscalc.htm#one). In answering the question "Have the time delays in your onboarding experience been significantly reduced", given a population of 86k, a confidence level of 95% and a confidence interval of +/- 4 (46% - 54%), then we are looking at a sample population of 596.  So lets pick a target around 600 or approximately 0.7%. Working with the **power of 10's** principle, 600 community members would give us 60 active commenters and 6 primary contributors. Interestingly 6 contributors is pretty close to the 5 users suggested in the UX scenario. So maybe 600 is a decent target. Now can someone do some rogue math on how many people we need to survey and interview, and then tell me how big of a team we need to accomplish our goals in a decent time frame?

So while we have a growing list of ways members can help this group grow in size, in this post I would like to **challenge the community** to see how much new content can add for others to comment on and share. Content does not need to be verbose to be valuable, so even a small tidbit can be quite valuable. Here are some doubling targets for the community to aspire to for iteration 14.

* New Blog Postings by new contributors: 2
* New Discussion threads started by new contributors: 4
* New Commenters: 4
* Total Likes: 4,  Just read through some of the content, and click the thumbs up to like something.
* Total Shares on the wire: 4, Did you like what you found and read and want to share it with others? Just click the share icon under the content and post it to The Wire. I don't actually know how to measure this yet, but lets just share a bunch.

Here are [some other ways you can help](https://gccollab.ca/discussion/view/2924125/enhelp-gcmobility-spread-the-wordfr)

## Iteration Scrum Update
### What have we been working on
* **Stakeholder Engagement**
  * Presented to Free Agent Steering Committee.
  * Stakeholder engagement within the Free Agents community to identify mobility pain points
  * Completed interviews with remaining Free Agent support team to explore mobility issues and key pain points.
  * Met with the SSC group responsible for Active directory which stores employee login credentials and identities to learn how the future might impact or enable mobility at the technology layer (more conversations to come)
  * Trying to develop multiple "value propositions" for various stakeholders groups (eg Employee, Managers, Business)
  * Engaged with stakeholders from Office of Chief Human Resources Officer (OCHRO), and the GC HR Council
  * Engaged with stakeholders from GCworkplace and GCcoworking to explore collaborations
  * Engaged with SSC group responsible for Active Directory to explore mobility scenarios

* **Project management**
  * [Posted a recording of an earlier draft presentation](https://gccollab.ca/file/view/2897473/engcmobility-long-form-presentation-draft-recording-long-37min-uneditedfr). At 37 min it captures that full context of what we are trying to do within GCmobility. Other trimmed down versions are pending as we work with exported Webex videos.
  * Exploring options for building out a team to support the GCmobility initiative (stay tuned)

* **IT Issues**
  * 1 of 2 virtual machines for demonstration are working (SSC). Need some time dedicated to testing.
  * PSPC image still has issues. need to work with service desk. Will wait until after upcoming leave.
  * Confirmed that new e-mail system will move away from @canada.ca. Mobility use case is a known issue that has not been designed for at this time.

* **Architecture**
  * Published first version of our ["Enterprise Architecture for Mobility"](https://gccollab.ca/file/view/2757952/enssc-gcmobility-architecturepdffr)

* **Communications**
  * Further developed the GCmobility Stakeholder Engagement and Communication Plan

### What will We work on
* **Stakeholder Engagement**
  * Monthly Meetup for GC Mobility Leaders Community #5: Accessible Technology
  * Engaging with Free Agent Steering Committee in September
  * Engaging with SSC Client Executives
  * Engaging with Free Agent Support Team
  * Facilitating and growing GC mobility monthly meetings
  * Tracking down consolidated HR related data to examine mobility rates in GC between 1990-2020
  * Waiting for further details on SSC’s decision to move from Hyper-V to VMWare which will impact POC-1 testing

* **Project Management**
  * Discussion between GCmobility director and another SS manager to explore securing more project resources.

* **Communications**
  * Seeking feedback on the GCmobility deck
  * Seeding multiple discussion threads on GCmobility deck in GCcollab discussion forums

* **Architecture**
  * Prepare to present to SSC Enterprise Architecture Council
  * Seek feedback on business model canvas to describe GCmobility business model

### What blocks are we facing
* Stakeholder engagement
  * Mobility is a complex problem with lots of people and inter related projects
  * Currently GCmobility has a "yellow" light for full stakeholder engagement. Management wants to know the who/how/what. So we are co-designing this piece with our community, you!(stay tuned)

### New Issues added to the backlog
* GCmobility needs to develop value propositions for more stakeholder groups to get buy-in

### Interesting things we are learning
* Everything is so interesting right now. Hard to avoid fun rabbit holes to pop down into. Trying to stay above the field for now.

## Agile Metrics
This section is for those hooked on metrics. Let us know what metrics are important to you.
* Iterations: 13
* Weeks: 26
* Total Issues: [93](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/issues?scope=all&utf8=%E2%9C%93&state=all)
  * Open Issues: [87](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/issues?scope=all&utf8=%E2%9C%93&state=opened)
  * Total Closed Issues: [6](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/issues?scope=all&utf8=%E2%9C%93&state=closed)
* GCMobility on GCcollab: [111 Members](https://gccollab.ca/groups/members/2183003)
* Burn Down rate: UNKNOWN (need to estimate and calculate efforts for User Story Points)
* Current Project end Date: March, 2019
* Proposed Project End date: March 2024


## GCmobility Resources
*	[Short Deck Introducing GCmobility](https://gccollab.ca/file/view/2696019/engcmobility-introduction-dg-approved-sharedpptxfr)
*	[GCmobility Community on GCcollab](https://gccollab.ca/groups/profile/2183003/engcmobilityfrgcmobilitu00e9)
  * [Blogs and Updates](https://gccollab.ca/blog/group/2183003/all)
  * [Discussion Forums](https://gccollab.ca/discussion/owner/2183003)
* ["Enterprise Architecture for Mobility"](https://gccollab.ca/file/view/2757952/enssc-gcmobility-architecturepdffr)
*	[GCmobility Project Site](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility)
  * [Submit a new Mobility Issue](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=)
  * [Our KanBan Board](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/-/boards)
  * [Full Issue Backlog](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/issues)
