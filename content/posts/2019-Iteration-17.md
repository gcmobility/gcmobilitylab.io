Title: GC Mobility Update Iteration 17
Date: 2019-11-27
Modified: 2020-02-27
Category: Blog
Tags: gcmobility, demos, , iteration-17
Slug: gcmobility-iteration-review-17
Toc_Include_Title: false
Status: published
Summary: Iteration 17 was another packed iteration. We continued to reflect on what we have learned and are compiling some of the data we have gathered. In the upcoming iterations we will be releasing some of what we have learned.

*UPDATE*: This content was originally published in the [GCmobility group within GCcollab](https://gccollab.ca/groups/profile/2183003/engcmobilityfrgcmobilitu00e9). "Hosted by the Government of Canada, GCcollab is designed to connect you with the information and people you need to work with across Canadian universities, colleges and all levels of government." ([GCcollab Terms and Conditions](https://gccollab.ca/terms)). Some links may lead to internal resources.

## Overview
This post represents the update for Iteration 17 of the GCmobility project.

The [GCMobility initiative](http://bit.ly/gcmobility-wiki) is a joint partnership between [Canada's Free Agents](http://bit.ly/cfa-gcpedia) and [Shared Services Canada](http://bit.ly/ssc-web)

Iteration 17 was another packed iteration. We continued to reflect on what we have learned and are compiling some of the data we have gathered. In the upcoming iterations we will be releasing some of what we have learned.

As promised, we ended this iteration with a recorded [Iteration 17 Iteration Review](http://bit.ly/gcmobility-iteration17-video) ([Static Presentation](http://bit.ly/gcmobility-iteration17-presentation)). This was a success for GCmobility as it helps us maintain our cadence of the Agile Scrum method. While we did not record in front of a live studio audience, it is our hope to have interested stakeholders join us for future demos live. In addition to the recorded demo we have also be posting the static slide deck, which has various interactive elements you can click on and review yourself. If you are interested in joining for future live demonstrations please ["reach out to us"](mailto:david.sampson@canada.ca)

As we move towards more demos and reviews, we will be shifting our energy towards the demos and away from the long form blog updates. Both endeavours take a measurable amount of time, and we feel there is increased value in the live and recorded demonstrations. We can also measure stakeholder engagement through how many people join us live vs recorded. Therefore, much of the long form content from these iteration updates are now part of our demonstrations. We have also redirected some of the recurring content found in these blog posts to our wiki page. You will find links below.

In the Iteration Review and Demonstration you will learn more about:
* Project Metrics
* Our Project site, where we manage our project in the open
* The GCmobility Wiki, where we are developing our documentation
* The GCCollab group, which you should already know about if you are reading this blog post
* Have a sneak peak at our stakeholder Intake Survey as we address some real time user feedback
* Check out our new Engagement request form. Maybe you want to test it out
* Learn how we are gathering data through some link tracking metrics
* See how we are overcoming siloed e-mail and calendaring systems using a "book me" service online
* Learn about some upcoming events and activities

We have also completed our planning session for Iteration 18, so click through to the [planning discussion thread](http://bit.ly/gcmobility-planning) for updates on what is planned for this upcoming iteration.

Thanks again for your continued support and encouragement. We look forward to your comments on the demo format and how we can improve the experience while keeping formalities light and agile.

Be Mobile with [GCmobility!](http://bit.ly/gcmobility-wiki) / Soyez mobile avec [GCmobility!](http://bit.ly/gcmobility-wiki)


## Iteration Scrum Update
### What have we been working on
* Accessible technology and ergonomics for mobile workers (eg mobile office)
* CSPS Rethinking the Moonshot: Mobility may be a moonshot, however is it the right problem? How do we define the problem?
* Confirmed MyKey procedures for FreeAgents from SSC team
* Met with CSPS Digital Academy: Exploring partnership options
* Followed up with Public Service Renewal, invited to present Nov 27
* Visited with Canadian Digital Service: Dedicated resources to handle onboarding processes. User satisfaction key for Onboarding experience to attract and retain talent from private sector.
* Met with Active Directory team: Pervasive technology that can support/block mobility. Current architecture moving away from mobility.
* Compiling data from Free Agent Journey Mapping
* GCmobility Stakeholder Intake Questionnaire

### What will We work on
* GCmobility Meetup # 7 https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/issues/103
* Review SSC 3.0 materials
* Team Composition and skill requirements
* While a Wiki is going to be useful, we feel there is a need for an info graphic
* Preparing to present to SSC Enterprise Architecture Council will require some more work on our Enterprise Architecture model.
* Then we would like to start experimenting with business models for GCmobility using the business model canvas

### What blocks are we facing
* Mobility is a complex problem with lots of people and inter related projects
* Currently GCmobility has a "yellow" light for full stakeholder engagement. Management wants to know the who/how/what. So we are co-designing this piece with our community, you!(stay tuned)
* We are in the process of submitting an updated presentation for Executive update and approval.
* We are looking to grow the GCmobility team through vehicles like micro missions as we currently do not have any salary funding

### New Issues added to the backlog
* GCmobility needs to develop value propositions for more stakeholder groups to get increased buy-in
* We are working on our next 100 Steps. Currently we have a Mind Map, which will evolve into a spreadsheet and eventually get imported into our project tracking system
* Developing profiles for Hyper Mobile Employees

### Interesting things we are learning
* Working on developing better habits for tracking metrics. This is slow and tedious work, but seems to be delivering some decent insights

## Other Resources
Instead of developing a "one pager" we [developed a wiki page](http://bit.ly/gcmobility-wiki) to act as a landing page for GCmobility. A lot of recurring content found in previous iteration updates can now be found there.

* Agile Metrics: You can now find these in our Iteration Review presentation (Deck)(Recording)
* GCmobility Resources: This section has been moved to our wiki under [Key Resources for GCmobility](https://wiki.gccollab.ca/GCmobility?#Key_Resources_for_GCmobility)
* Engagements: This section has moved to our wiki under [How to Get Involved With GCmobility](https://wiki.gccollab.ca/GCmobility#How_to_Get_involved_with_GCmobility)
* GCmobility Discussion Series: This section has been removed. Please click through to the discussion are to catch up on discussions.
* GCmobility Video Series: This section has been moved to our wiki under [GCmobility Video Series](https://wiki.gccollab.ca/GCmobility?#GCmobility_Video_Series)s
