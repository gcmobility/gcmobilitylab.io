Title: GC Mobility Update Iteration 14
Date: 2019-10-10
Modified: 2020-02-27
Category: Blog
Tags: gcmobility, demos, iteration-14
Slug: gcmobility-iteration-review-14
Toc_Include_Title: false
Status: published
Summary: Iterations 14 of GCmobility secured an agreement between [Shared Services Canada (SSC)](https://www.canada.ca/en/shared-services.html) and [Canada's Free Agents](https://www.gcpedia.gc.ca/wiki/Free_Agents) has been extended to March 2020. This Free Agent Agreement is what makes it possible for the GCmobility efforts to happen. Thanks to NRCAN (my home department) and SSC (my host department) for extending this agreement.

*UPDATE*: This content was originally published in the [GCmobility group within GCcollab](https://gccollab.ca/groups/profile/2183003/engcmobilityfrgcmobilitu00e9). "Hosted by the Government of Canada, GCcollab is designed to connect you with the information and people you need to work with across Canadian universities, colleges and all levels of government." ([GCcollab Terms and Conditions](https://gccollab.ca/terms)). Some links may lead to internal resources.


## Overview
This post represents the update for Iteration 14 of the GCmobility project.

The [GCMobility initiative](https://gccollab.ca/groups/profile/2183003/engcmobilityfrgcmobilitu00e9) is a joint partnership between Canada's Free Agents and Shared Services Canada

Woot Woot!

I am excited to announce that the agreement between [Shared Services Canada (SSC)](https://www.canada.ca/en/shared-services.html) and [Canada's Free Agents](https://www.gcpedia.gc.ca/wiki/Free_Agents) has been extended to March 2020. This Free Agent Agreement is what makes it possible for the GCmobility efforts to happen. Thanks to NRCAN (my home department) and SSC (my host department) for extending this agreement.

Iteration 14 focused on more stakeholder engagement. I am humbled by the support and interest I find in the stakeholders that I am engaging. The general messages I continue to receive include:
* GCmobility is tackling an important issue concerning mobility.
* Many projects have tackled mobility in the past with varying levels of success.
* There should be more coordination between all these projects to maximize impact.
* Issues concerning mobility are real and impact us all
* There is no clear accountability as to who owns a mandate for mobility and who should resource projects trying to solve these problems
* although mobility is an issue understood intuitively, we lack the data and metrics to measure its benefits and costs.

The more stakeholders we meet with, the more stakeholders we learn about. We often leave one stakeholder engagement with a list of even more stakeholders to go and talk to.

As a project lead I find myself constantly "putting myself out there", sharing ideas, visions and trying to mobilize communities to solve problems. For the most part this is fun and rewarding. Of course there are many challenges along the way. These challenges, if handled appropriately can lead to both personal and professional development. During times of reflection in the past 13 years within the Federal Government I feel as though I have grown a lot as a public servant, project lead, son, friend and husband.

GCmobility, like so many project before it has given me more opportunities to reflect. Reflection does not always lead to action, and sometimes my main take away from reflection is that I need more.. wait for it... reflection.

My latest reflection for GCmobility is related to these Iteration Update blogs. In the past I have kept them short and punchy addressing the 5 questions of Agile Scrum:

1. What did we accomplish this iteration
2. What will we work on next iteration
3. What Blocks are we facing
4. What New issues have been added to the backlog
5. What interesting things have we learned along the way.

In the [Iteration 13 update](https://gccollab.ca/blog/view/3035672/engcmobility-update-iteration-13fr) I asked you, the stakeholders, **what format you might find most useful for Iteration Updates**. The result was no feedback in the comments, and a single share (by me). So I got to thinking if these updates are useful or if they are even being read. Unfortunately I don't have access to fancy web analytics as a group owner other than [GCmobility Group Statistics](https://gccollab.ca/groups/stats/2183003) for membership size stats.

I have noticed however that the discussion topics generate some more comments, dialogue and responses. So, in the name of experimentation this iteration update will be a slightly different format. I will leverage the discussion forums more to update recurring sections of the iteration updates. For those who are interested in the full update I will provide links here. For those interested in a more narrow facet of updates you can track individual discussion threads.

I have never tried this approach before so I will try this approach for a couple of iterations and then re-assess. In short, I have split off many recurring portions of this iteration update to separate discussion threads. My Key Performance Indicators to be tracked will include how many comments, likes and shares emerge from the blogs and discussions. Also, of the links found below, how many get clicked and the number of comments that are generated.

So here is a shortened update to point you to more resources.

##Iteration Update Items
* Follow our ongoing [Stakeholder Engagements ](https://gccollab.ca/discussion/view/3136210/engcmobility-stakeholder-engagementsfr)
* Consider joining one of our[GCmobility Meetups](https://gccollab.ca/discussion/view/3136104/engcmobility-community-meetupsfr).
* We are looking for feedback on our ["Enterprise Architecture for Mobility"](https://gccollab.ca/discussion/view/3138937/enenterprise-architecture-in-support-of-mobilityfr)
* Here are [some ways you can help](https://gccollab.ca/discussion/view/2924125/enhelp-gcmobility-spread-the-wordfr)
* Check out our [evolving Communications and Engagement plans](https://gccollab.ca/discussion/view/3136210/engcmobility-stakeholder-engagementsfr)

### GCmobility Discussion Series
The first three part of a multi part discussion series unpacks GCmobility concepts one at a time. THis approach aims to generate conversation while providing healthy debate or confirmation of assumptions that we are making about mobility. We look forward to engaing you in each of these discussions
* [Why the GCmobility Initiative Matters (Part 1)](https://gccollab.ca/discussion/view/2988160/enwhy-the-gcmobility-initiative-matters-part-1fr)
* [The GCmobility Context (Part 2)](https://gccollab.ca/discussion/view/2988899/enthe-gcmobility-context-part-2fr)
* [The GCmobility Problem Statement (Part 3)](https://gccollab.ca/discussion/view/2994859/enthe-gcmobility-problem-statement-part-3fr)

### GCmobility Video Series
* [Recording of an earlier draft presentation](https://gccollab.ca/file/view/2897473/engcmobility-long-form-presentation-draft-recording-long-37min-uneditedfr). At 37 min it captures that full context of what we are trying to do within GCmobility. Other trimmed down versions are pending as we work with exported Webex videos.
* [Accessibility Accommodation Adaptive Computer Technology (AAACT)](https://gccollab.ca/file/view/3135981/engc-mobility-community-meetup-accessibility-accommodation-adaptive-computer-technology-aaact-program-2019-10-04fr)
* [GCmobility conferencing for remote employees workshop and Owl Labs product demonstration](https://gccollab.ca/file/view/3138376/engcmobility-owl-labs-video-conference-demo-2019-10-08fr)


## Iteration Scrum Update
### What have we been working on
* Experimenting with the format of these Iteration updates

### What will We work on
* Stakeholder Engagement will continue
* We are looking to grow the GCmobility team
* Work on communication products such as a one pager and an info graphic
* Prepare to present to SSC Enterprise Architecture Council
* Seek feedback on business model canvas to describe GCmobility business model

### What blocks are we facing
* Mobility is a complex problem with lots of people and inter related projects
* Currently GCmobility has a "yellow" light for full stakeholder engagement. Management wants to know the who/how/what. So we are co-designing this piece with our community, you!(stay tuned)

### New Issues added to the backlog
* GCmobility needs to develop value propositions for more stakeholder groups to get increased buy-in

### Interesting things we are learning
* learned how to [split a screen in windows 10](https://www.digitaltrends.com/computing/how-to-split-your-screen-in-windows-10/)
* learned about https://alpha.canada.ca/en/index.html (Thanks Gray)

## Agile Metrics
This section is for those hooked on metrics. Let us know what metrics are important to you.
* Iterations: 14
* Weeks: 28
* Total Issues: [93](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/issues?scope=all&utf8=%E2%9C%93&state=all)
  * Open Issues: [87](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/issues?scope=all&utf8=%E2%9C%93&state=opened)
  * Total Closed Issues: [6](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/issues?scope=all&utf8=%E2%9C%93&state=closed)
* GCMobility on GCcollab: [122 Members](https://gccollab.ca/groups/members/2183003)
* Burn Down rate: UNKNOWN (need to estimate and calculate efforts for User Story Points)
* Current Project end Date: March, 2020
* Proposed Project End date: March 2024

## GCmobility Resources
*	[Short Deck Introducing GCmobility](https://gccollab.ca/file/view/2696019/engcmobility-introduction-dg-approved-sharedpptxfr)
*	[GCmobility Community on GCcollab](https://gccollab.ca/groups/profile/2183003/engcmobilityfrgcmobilitu00e9)
  * [Blogs and Updates](https://gccollab.ca/blog/group/2183003/all)
  * [Discussion Forums](https://gccollab.ca/discussion/owner/2183003)
* ["Enterprise Architecture for Mobility"](https://gccollab.ca/file/view/2757952/enssc-gcmobility-architecturepdffr)
*	[GCmobility Project Site](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility)
  * [Submit a new Mobility Issue](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=)
  * [Our KanBan Board](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/-/boards)
  * [Full Issue Backlog](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility/issues)
