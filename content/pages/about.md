Title: About
Date: 2020-02-05
Modified: 2020-02-05
Tags: about, gcmobility
Slug: about-gcmobility
Toc_Include_Title: false

The GCMobility initiative is a joint partnership between Canada's Free Agents and Shared Services Canada. As the initiative grows so will the list of partners.

# Our Vision:

By 2025, Canadian Federal Public Servants    
have the tools to work effectively and efficiently    
From anywhere,    
At anytime    
For any department and beyond     

# Our Value Proposition:

The GCMobility initiative is for **Canadian Public Servants**,    
who want the autonomy and technology support to **manage their mobile career**.    
We will develop and test new service models for **modern and mobile office workflows**,    
That are easily transferable **within, between and beyond departments**.    
**Unlike the current model** offered by individual departmental silos,    
Which creates disparate policies, technology services, supports and security models.    
GCmobility will iteratively and collaboratively develop and test **new inter-departmental mobility service models**,    
That public servants and departments alike can **rely on and trust**.    

# Objectives:

The primary objectives of this partnership are 2 fold:

1. Decrease the time it takes for mobile employees (eg. Free Agents) to cross board (onboard and off board) between departments during their short term and rapid assignments throughout the Canadian Federal Government
2. Decrease the technology footprint of embedded employees that identify with both a home department, to whom they belong; and a host or partner department, to whom they provide services (eg. SSC employees); who are issued multiple devices (eg 5-20 laptops) by each partner or host department for which they provide services

# GCMobility Partner Organizations

* Canada's Free Agents ([GCCollab](https://gccollab.ca/groups/profile/240597/canadas-free-agents-agents-libres-du-canada) and [GCPedia](http://www.gcpedia.gc.ca/wiki/Free_Agents))
* Shared Services Canada ([World Wide Web](https://www.canada.ca/en/shared-services.html))

# Project Overview

GCMobility is an initiative lead in partnership between Shared Services Canada (SSC) and Canada’s Free Agents (CFA). Both SSC and CFA are committed to supporting mobility in the Canadian Public Service. Where SSC supports mobility of technology, CFA supports mobility of talented public servants.

The GC Mobility Pilot repository is used to manage information related to a joint pilot between Canada's Free Agents and Shared Services Canada (SSC) to enable the free flow of interdepartmental IT between departments.

The goal of this project is to reduce the onboarding and offboarding time required by Free Agents when they start/end any of their Free Agent Opportunities.

There have been many Free Agents tackling various service-lines (eg e-mail, mykey, wifi, laptops, blackberry's etc). Through the GCMobility project we can consolidate all these issues, documents, SSC contacts and the like.

The GoC is commited to a mobile, and modern workforce. The [Government of Canada Strategic Plan for Information Management and Information Technology 2017 to 2021](https://www.canada.ca/en/treasury-board-secretariat/services/information-technology/strategic-plan-2017-2021.html) provides a high level roadmap of how to get there by outlining key Principles and Goals.

* [Principle 6: enable a modern workplace: anywhere, anytime with anyone](https://www.canada.ca/en/treasury-board-secretariat/services/information-technology/strategic-plan-2017-2021.html#toc6-6), "provides its employees with modern technology that supports information retrieval, use, sharing and collaboration by making information and data accessible when and where needed".
* [Strategic Goal 4: Agility](https://www.canada.ca/en/treasury-board-secretariat/services/information-technology/strategic-plan-2017-2021.html#toc7-4), "provides a technologically advanced workplace that supports mobility".

This project is not meant to handle day to day IT issues for Free Agents. Day to Day IT issues should still be handled by either your home department or host department. This project will track issues not covered by regular IT support (such as connecting an NRCAN laptop to a host department network)


Read more about [Canada's Free Agents here:](http://www.gcpedia.gc.ca/wiki/Free_Agents)
Read more about [SSC Mandate here:](https://www.canada.ca/en/shared-services.html)
Read more about [Government of Canada Strategic Plan for Information Management and Information Technology 2017 to 2021](https://www.canada.ca/en/treasury-board-secretariat/services/information-technology/strategic-plan-2017-2021.html)

# Key Resources:

* Community Group ([GCCollab](https://gccollab.ca/groups/profile/2183003/engcmobilityfrgcmobilitu00e9))
* Internal Project Site and issue tracker ([GCCode](https://gccode.ssc-spc.gc.ca/canadas-free-agents/gcmobility))
* Document Repository ([Google Drive](https://drive.google.com/open?id=1DSsFIPXMJJ2vPNT_KJHSrb0NDoygXyPS))
