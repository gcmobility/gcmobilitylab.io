Title: GC Mobility Proof of Concept: M365/Teams Pilot for Mobile employees
Date: 2020-05-21
Modified: 2020-06-09
Category: Blog
Tags: pilot, proof of concept 
Slug: gcmobility-poc-m365-mobility
Toc_Include_Title: false
Status: hidden
lang:fr 

Summary: Résumé : GCmobility offre aux agents libres du Canada une nouvelle occasion de participer à des recherches sur la mobilité. Cette fois, il s'agit d'une étude exploratoire de convivialité utilisant Microsoft 365. Nous allons piloter l'utilisation de [GCcollaboration.ca] (http://gccollaboration.ca), la réponse du gouvernement du Canada pour permettre aux fonctionnaires fédéraux de rester connectés et productifs pendant la quarantaine COVID-19 pour soutenir le travail à distance et le télétravail.

## Résumé

* Si vous voulez juste commencer et rejoindre le pilote, veuillez [remplir le formulaire d'inscription ci-dessous] (#formulaire de demande pilote).
* L'un des principaux objectifs de GCmobility est de réduire le temps de passage entre les ministères.
* Ce projet pilote est la dernière occasion pour les agents libres du Canada de participer à des recherches sur la mobilité. 
* Ce pilote est une étude d'utilisation exploratoire à distance non supervisée utilisant Microsoft 365. 
* Le projet pilote utilisera [GCcollaboration.ca] (http://gccollaboration.ca), la réponse du gouvernement du Canada pour permettre aux fonctionnaires fédéraux de rester connectés et productifs pendant la quarantaine COVID-19 pour soutenir le travail à distance et le télétravail. 
* La période d'étude initiale durera environ 4 semaines
* Les utilisateurs qui souhaitent continuer à utiliser le système et participer à des recherches sur les utilisateurs ultérieurs peuvent continuer à utiliser le système après le projet pilote initial.
* Le système GCcollaboration.ca sera fermé en septembre 2020, vous devez vous assurer que vos données et informations sont gérées de manière appropriée.
* Ce système n'est approuvé que pour les informations et les données non classifiées et non protégées.
* Si vous voulez un peu plus de contexte, alors n'hésitez pas à poursuivre votre lecture.
* Au fur et à mesure de l'avancement du projet pilote, nous ajouterons à la liste des [Foire aux questions] (#foire aux questions)
* Si vous êtes prêt et disposé à participer à ce pilote tardif, veuillez passer directement à [remplir le formulaire d'inscription] (#formulaire de demande pilote). Le formulaire d'inscription devrait prendre de 5 à 10 minutes à remplir.

Merci de votre soutien continu pour accroître la mobilité de la carrière et de la main-d'œuvre au sein du gouvernement du Canada.

## Notes importantes

Afin de recueillir quelques informations de base sur les utilisateurs, nous avons élaboré un formulaire d'inscription qui comprend une série de questions liées au projet pilote. Il est nécessaire de remplir ce formulaire pour participer à ce projet pilote. Le formulaire devrait vous prendre de 5 à 10 minutes à remplir. A la fin du projet pilote, nous demanderons aux utilisateurs de remplir un questionnaire de sortie. Les résultats du projet pilote seront en partie basés sur les résultats obtenus en comparant les pré et post questionnaires. En tant qu'utilisateur test, vous pourrez être invité à remplir d'autres enquêtes et questionnaires plus courts tout au long de la période d'étude.

La période d'étude principale durera 4 semaines une fois que la majorité des utilisateurs auront embarqué dans le système. Ce délai devrait être suffisant pour permettre aux utilisateurs de se connecter au système, d'explorer certains des outils et d'expérimenter l'intégration de ces outils dans leur travail quotidien. Les utilisateurs sont invités à continuer à utiliser le système après la période pilote initiale.

**IMPORTANT** GCcollaboration.ca est une solution temporaire pour résoudre certains problèmes d'accès pendant la période COVID-19. En tant que tel, ce système ne devrait pas être considéré comme de la production. Toute l'information sur le système devrait être considérée comme non classifiée. Ce système n'est pas considéré comme un dépôt officiel pour les ressources d'information à valeur commerciale, alors veuillez utiliser les systèmes de gestion des documents de votre ministère au besoin. Encore une fois, GCcollaboration.ca n'est que temporaire et sa fermeture est prévue pour septembre 2020. Veuillez vous assurer que tout le matériel ou la correspondance développé au cours de ce projet pilote est correctement géré, retiré ou supprimé d'ici la fin du mois d'août 2020.  

## Contexte

L'initiative GCmobilité, avec l'aide et le soutien de Services partagés Canada (SPC), du Centre numérique pour la collaboration et d'autres agents libres ont lancé le projet pilote M365/Teams pour les employés mobiles.

L'objectif principal de GCmobility est de réduire le temps de passage entre les ministères. De nombreux "systèmes" sont impliqués dans cette longue aventure de crossboarding. Des personnes et des politiques aux processus et aux technologies. Le problème peut parfois sembler si important que la seule solution est de tout court-circuiter. Mais il existe peut-être une meilleure solution. 

Une solution potentielle au problème de la transversalité est de réduire au minimum le nombre de systèmes de GI/TI que nous devons déplacer lorsque nous changeons de poste. GCmobility n'est certainement pas le seul projet en ville à avoir cette idée. Un nombre croissant d'agents libres travaillent actuellement à la résolution de problèmes similaires dans leurs propres activités, et nous avons essayé d'atteindre le plus grand nombre possible de ces initiatives. Bien que la demande officielle soit venue de GCmobility à la SSC, ce n'est pas uniquement le fait de GCmobility, un certain nombre d'agents libres ont également été essentiels pour obtenir le feu vert de la SSC. 

Pour ceux qui utilisent déjà GCcollaboration.ca, vous ne remarquerez peut-être pas beaucoup de changement, sauf peut-être encore une autre connexion (je sais que ce sera ennuyeux). Pour ceux qui sont confinés à une autre instance ministérielle d'équipes, vous pourriez trouver un public plus large avec lequel collaborer pendant ce projet pilote. [Environ 20 ministères utilisent déjà GCcollaboration.ca] (https://wiki.gccollab.ca/GCcollaboration_Department_Listing). 

## Le défi d'une boîte à outils de mobilité croissante 

Nous savons que vous avez déjà vos outils et que vous pourriez être submergés par tant d'outils défendus par tant de groupes uniques. Cette situation peut vous avoir obligé à gérer un nombre croissant de canaux de communication. Nous ne vous demandons pas d'abandonner les outils qui vous servent bien. Cependant, nous avons besoin de votre aide ! 

Grâce à ce projet pilote, vous nous aiderez à saisir l'occasion de recueillir les commentaires des utilisateurs sur m365/Teams, un système qui est de toute façon déployé dans toute l'administration. Nous voulons connaître l'avis de tous les agents libres ainsi que d'autres parties intéressées telles que les divers programmes de mobilité des RH qui continuent à offrir une forte valeur ajoutée dans l'ensemble du gouvernement. Nous avons besoin d'un large éventail de voix pour nous faire entendre et éclairer les décisions futures. 

Nous reconnaissons que Slack, Google Drive et Zoom fonctionnent pour de nombreuses personnes. Ces outils ont été adoptés par de nombreuses personnes, en partie en raison du manque d'autres options. Notre plan n'est pas d'empêcher l'utilisation de vos outils existants. Cependant, il semble y avoir une proposition de valeur croissante dans le fait de pouvoir disposer d'un "système de collaboration approuvé par le GC" stable et cohérent que nous pouvons utiliser pendant notre affectation dans n'importe quel ministère. Notre objectif est de pouvoir accéder aux personnes et aux ressources dont nous avons besoin, sans avoir à changer constamment de courrier électronique, à déplacer les Mykeys, à accéder aux VPN individuels, à apprendre de nouvelles pratiques de gestion des documents en interne, à ajouter à notre liste croissante de mots de passe et à attendre 6 à 10 semaines pour obtenir un ordinateur portable ou un téléphone approuvé. 

 

### Groupes d'essai

Les agents libres, en tant que groupe croissant de travailleurs mobiles, ont été identifiés (encore) comme un groupe test clé pour ce projet pilote, cependant nous voulons concevoir ce projet pilote pour qu'il ait un potentiel d'échelle au-delà des FAZZ pour inclure le personnel intégré des CSP, d'autres programmes d'innovation en matière de RH ainsi que d'autres groupes moins formellement reconnus (restez à l'écoute). 

Dans le cadre de notre processus de planification pour ce projet pilote, nous avons déjà engagé une sélection d'agents libres dans un chat d'ambiance pour les mettre au courant de cette opportunité pour les agents libres de gérer leur propre coin de GCcollaboration.ca. Ce projet pilote nous donnera l'occasion de tester des fonctionnalités que certains ministères pourraient avoir verrouillées ou ne pas avoir activées. Nous aurions l'occasion de gérer notre propre zone "Équipe" et d'explorer le fonctionnement des conversations en fil de discussion. Nous pourrions inviter un certain nombre de personnes à explorer les fonctions et l'accès dont elles disposeraient. Nous souhaitons en particulier voir comment fonctionne la collaboration avec les autres locataires. Toutes ces zones sont de grandes inconnues pour beaucoup de personnes au sein de la GC, et les agents libres auraient la possibilité de jeter un coup d'œil derrière les rideaux s'ils le souhaitent. 

Bien que l'équipe de collaboration numérique de la SSC nous ait donné le feu vert pour embarquer tous les agents libres, nous savons que cela a peu de chances de se produire. Notre objectif est d'obtenir 20 à 30 agents libres dans les trois départements d'origine. Pour tester les choses, remplissez un questionnaire après un mois d'utilisation et participez peut-être à une discussion en tête à tête. Les résultats seront rassemblés, compilés, analysés et partagés avec différents groupes au sein de la SSC. Nous avons également l'intention de partager les résultats et notre expérience au grand jour. 

Si vous connaissez un autre programme de mobilité des ressources humaines qui pourrait bénéficier d'une participation à ce projet pilote, veuillez [nous contacter et nous en informer]({nom du fichier}/pages/contact-us.md)
  
### Priorités concurrentes

On a compris. Les temps sont durs, les niveaux de stress sont élevés, tout le monde partage ses solutions dans un flot de ressources et nous pouvons avoir du mal à réserver du temps pour des discussions de bureau. Ajoutez à cela le fait de se laver les mains, d'essayer de ne pas parler de manière trop moite, de divertir les enfants, de laisser les chiens sortir pour aller faire pipi, de câliner nos chats, de rester en contact avec nos amis et notre famille tout en essayant de rester en sécurité et de rester sain d'esprit. 

Nous espérons malgré tout que vous rejoindrez le projet pilote, ou du moins que vous remplirez le formulaire ci-dessous et que vous nous ferez part de vos réflexions sur les raisons pour lesquelles cette solution pourrait ou non vous convenir. 

### Étude de convivialité pour la mobilité 
Ce projet pilote sera mené en partie pour recueillir des données sur la facilité d'utilisation. La partie de ce pilote consacrée à la convivialité sera menée sous la forme d'un test exploratoire à distance non modéré ((https://www.hotjar.com/usability-testing/methods/). La période d'essai principale sera de 4 semaines, sans supervision directe, pendant que les participants intégreront les équipes MS dans leur cycle de travail quotidien en travaillant à distance, généralement depuis leur domicile. La nature exploratoire du test laisse l'expérience utilisateur ouverte, permettant aux participants d'explorer toutes les offres d'outils qu'ils pensent être bénéfiques.

Ce pilote comprendra de nombreux "pilotes dans un pilote". L'offre d'outils et d'applications au sein de M365/Teams sur Gccollaboration.ca est assez importante. Une partie du pilote consistera à utiliser, tester et pousser ces outils à fond pour voir ce qui fonctionne et ce qui ne fonctionne pas. En cours de route, nous espérons trouver quelques diamants bruts. L'un de ces projets pilotes est l'utilisation de ce formulaire conçu à l'aide de "Formulaires pour Excel". Le formulaire est donc conçu en partie pour faciliter une étude de convivialité, mais nous avons également voulu tester de multiples fonctions de l'outil. La conception pourrait donc certainement être améliorée, mais parfois nous devons juste essayer des choses.

À la fin de la période de test de 4 semaines, vous serez invité à répondre à une autre enquête. Certaines questions seront similaires pour mesurer les résultats avant et après l'étude. Certaines questions seront uniques à chaque phase de l'étude.

Après la période d'étude, les participants au projet pilote seront invités à continuer à utiliser la plateforme GCcollaboration.ca tant que le système restera en ligne. Le système gccollaboration.ca est une solution temporaire mise en place par la SSC dans le cadre de la réponse du gouvernement du Canada à la pandémie COVID-19. À ce titre, le système doit être considéré comme temporaire et son utilisation pourrait être interrompue sans avertissement. 

### Bénévole pour rejoindre l'équipe pilote

Pour que ce projet pilote soit couronné de succès, nous avons besoin de l'aide de quelques volontaires de chaque ministère participant. Le projet pilote initial se concentrera sur les ministères d'origine des agents libres du Canada, notamment RNCAN, TC, SCT et ISED. Les volontaires devront utiliser, tester, commenter et donner leur avis sur la sélection des outils MS365 qui seront utilisés pour soutenir le travail des volontaires. Votre contribution en tant que volontaire dans ce projet pilote est aussi précieuse que les utilisateurs qui testent le système en général pendant ce projet pilote.

Notre exigence minimale est d'avoir un volontaire de chacun des 4 ministères d'origine pour mener le projet pilote. L'état final opérationnel idéal du pilote, s'il est bien adapté, est d'avoir un individu pour chaque rôle de chaque département d'origine. Avec 4 cohortes complètes de chaque ministère ou 120 agents libres et 4 rôles pour chaque ministère (4 x 4 = 16), nous pourrions avoir besoin de 16 volontaires au maximum, soit 120 agents libres. Dans le cadre de ce projet pilote, si la capacité de bénévolat est très élevée, nous n'aurons peut-être pas besoin de tous les bénévoles. Cependant, si la capacité est trop faible, nous ne pourrons pas gérer tous les aspects du projet pilote et nous n'aurons peut-être pas la capacité de fournir des services d'assistance supplémentaires aux utilisateurs. 

**Veuillez être averti** : Nous ne savons pas à quoi les volontaires doivent s'attendre. Nous n'avons pas de nombre minimum d'heures prévu pour le bénévolat, contribuez ce que vous pouvez. Il n'y a pas de formation officielle à offrir pour ces rôles de bénévoles. La documentation sera principalement issue de ce que vous pouvez trouver en ligne. Vous devrez travailler au sein de l'équipe globale des administrateurs du système. Ces rôles seront incertains et flexibles.  Google est votre ami.   Tout ce qui va au-delà est un bonus. Si vous changez d'avis dans un sens ou dans l'autre à l'avenir, ce n'est pas grave.

## Foire aux questions

### Que signifie rejoindre ce pilote ?
**Question** : Pour une personne moins familière avec ce qu'implique une nouvelle instance de MS365/équipes MS, aurai-je besoin d'un autre compte, d'un autre e-mail ? qu'est-ce qui est possible ?

**Réponse** : Bien que le résultat de ce pilote puisse être un nouveau compte et un nouveau courriel, etc. L'objectif est de diminuer le nombre de connexions, de comptes et de courriels. C'est quelque chose que nous espérons tester. Imaginez un seul compte et un seul e-mail pour accéder à toutes les différentes équipes auxquelles vous appartenez déjà.

### Prolifération des MS365/Equipes
**Question** : Certains sont beaucoup plus familiers avec les équipes MS, et ont jusqu'à 4 comptes d'équipes existants. Avons-nous vraiment besoin d'une autre instance ? 

**Réponse** : Imaginez si vous pouviez accéder à 4 instances M365/Teams via un seul compte ? Et comme vous entrez et sortez des différentes équipes, vos identifiants et votre identité restent les mêmes. C'est quelque chose que nous voulons tester.

### Qualité des communications
**Question** : La qualité des appels des équipes MS a été très bonne jusqu'à présent, par rapport à d'autres options que nous avons essayées comme Webex. Maintenant, on peut voir jusqu'à 9 personnes sur le panel de vidéos (plutôt Zoom). Ce pilote aura-t-il la même qualité de connexions ?

**Réponse** : Puisque nous utilisons l'infrastructure déjà mise en place pour gccollaboration.ca, la qualité devrait être la même. Nous voulons également tester comment nous pouvons accueillir des utilisateurs non seulement d'autres ministères, mais aussi de l'extérieur de GC. Nous aimerions savoir si la qualité est affectée par ces scénarios.

### Bonnes pratiques de GI
**Question** : L'utilisation d'un autre compte de messagerie électronique n'est-elle pas une pratique bizarre de la messagerie instantanée ? Les ministères encouragent déjà cette pratique en fournissant des courriels séparés pour les équipes MS. Votre projet pilote abordera-t-il cette question ?

**Réponse** :  Les comptes et les courriels multiples, les partages de fichiers, etc. sont en fait une pratique de GI médiocre, surtout si vous considérez le gouvernement du Canada comme une "entreprise" plutôt que chaque ministère. Cependant, pour le projet pilote, il est probable qu'il y aura davantage de courriels et de comptes. Nous voulons cependant démontrer que des pratiques de GI plus solides sont possibles pour les travailleurs mobiles, intégrés et collaboratifs. L'état actuel est que de nombreux agents libres ont déjà plusieurs courriels et mettent peu de messages hors du bureau pour diriger les gens vers leur nouveau courriel. Nous sommes conscients que c'est un problème et nous espérons pouvoir présenter des solutions positives après ce projet pilote.

### Soutien au personnel d'appui des employés mobiles
**Question** : La mobilité est moins préoccupante pour certains employés comme les responsables de l'embauche et les gestionnaires de talents. Qu'est-ce que ce projet pilote offrira à ces parties prenantes ?

**Réponse** : Si nous sommes d'accord sur le fait que la mobilité professionnelle est moins préoccupante pour les équipes de soutien, les responsables de l'embauche et les gestionnaires de talents, l'hypothèse actuelle est que ces parties prenantes sont elles-mêmes dans le domaine de la mobilité de la main-d'œuvre. Par exemple, imaginez un jour qu'un gestionnaire de talent du SCT ou de TC puisse jouer un rôle de soutien à un gestionnaire de talent de RNCan en congé ? Actuellement, il serait difficile de jouer ce rôle dans tous les ministères pour une MT et de jouer le rôle de remplaçant pour une autre MT alors que toutes les parties ne peuvent même pas partager un calendrier pour réserver un bilat, ou que la MT ne peut pas accéder à certaines informations de base sur l'agent libre. Les équipes MS ne règlent peut-être pas tous les problèmes de GI, mais d'un point de vue humain, cela devrait aider le programme Free Agent à être un peu plus cohérent pour l'ensemble des parties prenantes.

### Des outils adaptés à l'objectif
**Question** : Une des principales préoccupations est de savoir combien d'outils nous utilisons, et la raison pour laquelle nous utilisons ces outils. Il y a une limite entre la productivité et le fait d'utiliser trop d'outils. Réticent à consulter un autre courriel. Pourquoi risqueriez-vous d'introduire un nouvel outil ? 

**Répondez : Grâce à ce projet pilote, nous devrions être en mesure d'élaborer des conseils sur la gestion de plusieurs choses, tout en nous efforçant de mettre en place les meilleures pratiques en matière de gestion de l'information. La réalité est que les équipes MS sont là pour rester pour le prochain cycle d'acquisition de technologies de toute façon. D'autres déploiements d'équipes MS365/Teams auront lieu au sein du GC à l'avenir. Les organisateurs de ce projet pilote reconnaissent que de nombreuses "hypothèses" ont été émises sur la façon dont les choses vont fonctionner, mais peu de tests ou d'explications ont été faits pour valider ces hypothèses. Nous espérons que les résultats de notre petit projet pilote pourront aider à informer les décideurs du Conseil d'administration sur la façon dont ces outils seront déployés à l'avenir. Nous nous réjouissons donc de vos commentaires.

### Saturation des outils
**Question** : Les employés souffrent d'une certaine saturation des outils (4 Slacks, 4 comptes équipe MS, plus email, Twitter, test, téléphone...). L'objectif du pilote est-il de supprimer nos outils existants ?  

**Réponse** : Je suis d'accord, même les utilisateurs habitués aux outils de collaboration en ligne commencent à ressentir la saturation. Nous pensons également que si l'équipe MS est un outil utile, même s'il n'est pas toujours le meilleur outil pour répondre aux besoins de chacun. Notre objectif n'est pas de déterminer si l'équipe MS365 doit être utilisée pour aller de l'avant, mais plutôt de savoir si le navire a pris la mer. Notre objectif est de déterminer la meilleure façon pour les agents libres et le programme des agents libres de tirer parti des fonctionnalités de l'outil pour favoriser la mobilité de la carrière et de la main-d'œuvre. En même temps, nos premières recherches anecdotiques suggèrent que certaines personnes ont rejeté le MS365/Teams avant de réaliser qu'il pourrait en fait résoudre certains des problèmes dont les gens se plaignent régulièrement. Comme il s'agit d'un projet pilote, nous ne sommes pas en mesure de forcer l'utilisation de cet outil, mais nous offrons la possibilité de découvrir comment il peut servir au mieux les parties prenantes.

### Adresses électroniques génériques
**Question** : Est-il possible qu'à l'avenir, un courriel concernant l'ensemble du programme ne soit pas hébergé par un ministère d'origine ? (Actuellement, la boîte de réception de l'AF ne peut être hébergée qu'à RNCan ou à l'EFPC, puisqu'il s'agit de canada.ca) Question pour Dave. 

**Réponse** : Une excellente question. La façon dont le pilote sera déployé traitera l'"instance" comme un ministère distinct. Cependant, le principe de base du projet pilote est que les équipes MS365 doivent être agnostiques quant au programme et au département. Cela signifie que nous voulons que les agents libres ressemblent à n'importe quel autre fonctionnaire, et non à un membre d'un ministère particulier. Cela ne veut pas dire que les agents libres du Canada (CFA) ne peuvent pas voir leurs besoins en matière de programmes satisfaits, mais nous pensons qu'une identité unique pour tous les fonctionnaires vaut mieux que 250 identités distinctes réparties sur des lignes de codage financier comme dans les ministères. Les frais généraux liés à la gestion de l'embarquement croisé entre tous les systèmes informatiques sont une source de retards importants lors de la procédure d'embarquement croisé. Ces exigences du programme peuvent inclure la possibilité de mettre en place des courriers électroniques génériques, mais cela reste à confirmer. Du point de vue de la durabilité, si le programme de l'Autorité de surveillance voulait une instance MS 365/Teams perpétuelle, nous devons penser à son financement et à sa gestion (peut-être avec des contributions des ministères nationaux). L'avantage du projet pilote est la possibilité offerte par COVID-19 de tester ces caractéristiques et d'influencer les modèles que nous utilisons pour l'organiser. Tout cela sera possible en travaillant en dehors du feu de l'action du GC qui présente actuellement des défis pour de nombreux fonctionnaires, y compris les agents libres. Une fois que le pilote est en plein vol, nous pouvons envisager de mettre en place des mini-pilotes plus petits au sein du pilote, par exemple en créant un e-mail générique.

### Équipes multiples 
**Question** : J'ai déjà des équipes actives sur un ou plusieurs e-mails. Puis-je encore faire partie du pilote ? 

**Réponse** : Oui. Dans le cadre du projet pilote, tous les utilisateurs recevront un nouveau courrier électronique selon le modèle de first.last.canada@gccollaboration.ca . Tout ce dont vous avez besoin pour vous inscrire est une adresse e-mail primaire et une adresse e-mail de récupération auxquelles vous pouvez accéder au moment de l'inscription. L'adresse électronique principale sera utilisée pour envoyer l'invitation et les informations de connexion initiales. Une fois que vous vous serez connecté pour la première fois, nous pourrons alors expérimenter la demande d'accès à des équipes supplémentaires dans différents ministères.

### Quels sont les appareils qui fonctionnent 
**Question** : Devrais-je utiliser une tablette du gouvernement pendant ce projet pilote, ou mon [Choisir un appareil] serait-il réalisable ? 

**Réponse** : Au cours de ce projet pilote, la plateforme GCcollaboration.ca utilise une instance de M365/Teams en face-à-face avec le public. Ainsi, les types de dispositifs suivants devraient fonctionner pendant le pilote.

* Dispositifs appartenant à l'hôte : PC / Ordinateur portable / Téléphone / Tablette (pour autant que l'appareil puisse accéder à l'internet public)
* Appareils domestiques : PC / Ordinateur portable / Téléphone / Tablette (pour autant que l'appareil puisse accéder à l'internet public)
* Dispositifs personnels : PC / Ordinateur portable / Téléphone / Tablette (pour autant que l'appareil puisse accéder à l'internet public)

Traduit avec www.DeepL.com/Translator (version gratuite)


## Formulaire de demande pilote 
([Lien direct vers le formulaire](https://forms.office.com/Pages/ResponsePage.aspx?id=QstjKPSk1UG4g5fEh-5B_5xcdsxvxRZLq3fPW6MfLYdURjI3SjU0RzlDMlBVVzdaUEdOTlM5WDk3RC4u&lang=fr-CA) si elle ne se charge pas en dessous)

<iframe width="100%" height= "1600px" src= "https://forms.office.com/Pages/ResponsePage.aspx?id=QstjKPSk1UG4g5fEh-5B_5xcdsxvxRZLq3fPW6MfLYdURjI3SjU0RzlDMlBVVzdaUEdOTlM5WDk3RC4u&embed=true&lang=fr-CA" frameborder= "0" marginwidth= "0" marginheight= "0" style= "border: none; max-width:100%; max-height:100vh" allowfullscreen webkitallowfullscreen mozallowfullscreen msallowfullscreen> </iframe>
