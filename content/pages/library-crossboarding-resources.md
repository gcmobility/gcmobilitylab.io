Title: Crossboarding Resources
Date: 2020-04-29
Modified: 2020-04-29
Tags: pages, library
Slug: library-crossboarding-resources 
Toc_Include_Title: false
Status: hidden

## On Boarding 

* [Two virtual onboarding kits](https://gcconnex.gc.ca/file/group/4880212/all#62720645)(Internal Link: GCconnex) [1]

## Off Boarding 

## Cross Boarding.

## Managing a Mobile Career as a Mobile Worker 

* It might be worth looking into joing [GCcoworking](https://www.canada.ca/en/public-services-procurement/news/2019/06/gccoworking-new-flexible-alternative-workplaces-for-government-of-canada-employees0.html) and accessing a co working location near your home or office.

## Resources for Remote Workers

* Check to see if you want to know if "Is This Blocked In My Department](https://isthisblockedinmydepartment.ca/)
*  Going Remote Guide on Busrides.ca mentioned earlier.  Here is the link: https://busrides-trajetsenbus.ca/
*  Gitlabs's [Guide to All-Remote](https://about.gitlab.com/company/culture/all-remote/guide/)

## Working in the Open
Sometimes it is hard to be mobile when all of your work is locked down behind corporate firewalls. The [TBS Digital Standards](https://www.canada.ca/en/government/system/digital-government/government-canada-digital-standards.html) are the most recent in a line of policies that further enable public servants to work in the open. This section highlights some resources for those of you wanting to increase your mobility by increasing the amount you wokr in the open.

1. [Google's Teach From Home](https://teachfromhome.google) initiative demonstrates how the google suite of tools can be used together to help teachers. While we don't expect you to all become teachers, some of their mini courses are great to learn the tools.
2. Use the Google Suite of Tools in general to work in the open.
    1. [What is google Drive?](https://teachercenter.withgoogle.com/gettingstarted/week1)
    2. [What is Google docs](https://teachercenter.withgoogle.com/gettingstarted/week2)
    3. [What is Google Forms](https://teachercenter.withgoogle.com/gettingstarted/week4)
    4. [What is Google Slides](https://teachercenter.withgoogle.com/gettingstarted/week5)
    5. [What is Google Sheets](https://teachercenter.withgoogle.com/gettingstarted/week6)
    6. [What is Google Drawings](https://teachercenter.withgoogle.com/gettingstarted/week7)
    7. [What is Google Sites](https://teachercenter.withgoogle.com/gettingstarted/week8)
    8. 
2. GCCollab: 

## Notes 


    Dave (me): I am leading GCmobility and focusing on how we can decreasing crossboarding between departments. I am experimenting with the begining of a "digital service" of sorts that might one day span across departments. I suggest we have a one on one chat soon so I can better learn how we might collaborate
    Ericka (aka ESR) is with FlexGC at CSPS, and we were talking today about collaboration and sharing resources. Perhaps she has some resources to support virtual onboarding, as well as a growing network of busy beavers who can also pull together some resources.
    Lily is the Free Agent Business manager. She might be able to cobble together some resources used to onboard Free Agents. If required Lily might pull in Laura the Free Agent program designer.
    Helen has been both a Free Agent talent manager as well as the lead for training and education for Free Agents as one of her gigs. She is well connected in the learning space and has likely developed many on boarding resources or resources in general that could help new students and public servants.
    Aaron is currently working on a project with SSC CIO to streamline the on boarding process from a business process perspective. I am not sure if he has resources directly, but I think there might be an opportunity to explore the process side of virtual onboarding, especially since it would be something relatively new to explore.
    Jessica and Alexa are both with the Canadian Digital service. They have some great on boarding tools and resources and are the authors of this great CDS blogpost about the subject: https://digital.canada.ca/2019/07/29/making-a-great-first-impression-onboarding-matters/
    Thom Kearney tends to know of someone or some resource about many subjects. Perhaps he has some leads for virtual on boarding
    Jeffrey S and Russel are both with the accessible technology group at SSC. From a virtual perspective I am sure some students may be requiring some assistive technology while working with the GC. They have a full in-person service offering. I would be interested to learn if they have started to virtualize some of the accessible services. Perhaps a partnership or collaboration opportunity with PCO?
    Gray and Valerie are with TBS Talent Cloud and deal with on boarding related issues regularly. They might have some interesting tools to leverage for bringing students on virtually.
    Amanda leads XFN out of CSPS a development program for policy folks (?) Anyhow, XFN participants experience new on-boarding every 6 months for a total of three cyces. Amanada may have some key resources for XFN or be able to connect you with the resources her participants leverage as they move around GC.
    Etienne has spent a fair bit of time in the HR space and may have some additional resources for you. He has experience with streamlining hiring process. One that might be interesting for students to consider would be “How to Work like a Free Agent, Even If you Are Not” (http://www.etiennelaliberte.ca/2018/03/how-to-manage-your-career-like-free.html)

Perfect! we'll keep the list to 10 (ish). I hope some of these folks can help connect you further.

 

And here are some resources from the GCmobility side of the fence:

    Feel free browse out project files here. take and reuse what you wish. https://gcmobility.gitlab.io/pages/gcmobility-files.html
    Here is the evergreen Manual for Mobile Employees. Lots of content, links and ideas. Lots of holes too. This document is based on the idea that the mobile employee needs to be self-empowered to make onboarding and crossboarding happen. If you do nothing, nothing will likely happen. https://docs.google.com/presentation/d/1lXzeoGss76uODJxg8oDXz5Uz-5CbX2wCpMa3-9dd0cA/edit?usp=sharing
    also check out the Free Agent toolbox on GCpedia here: https://www.gcpedia.gc.ca/wiki/Free_Agents/Tool_Box

## Contributors
This section highlights the Contributors of some of these resources. Contributors may not be the original authors, but rather they pointed us towards the resources.

[1]:


Something that you think we should add? Contact us with a description of the resource and a public link to the resource.
