Title: GCmobility Mailing Lists
Date: 2020-04-27
Modified: 2020-04-27
Tags: pages, mailinglist
Slug: gcmobility-mailing-lists
Toc_Include_Title: false
Status: hidden


Welcome to the GCmobility Mailing List

<!-- Begin Mailchimp Signup Form -->
<div id="mc_embed_signup">
<form action="https://gitlab.us8.list-manage.com/subscribe/post?u=392c9bd75a4f1ba26b9bffe0a&amp;id=cdcf56eed1" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">
	<h2>Subscribe</h2>
<div class="indicates-required"><span class="asterisk">*</span> indicates required</div>
<div class="mc-field-group">
	<label for="mce-EMAIL">Email Address  <span class="asterisk">*</span>
</label>
	<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
</div>
<div class="mc-field-group">
	<label for="mce-FNAME">First Name </label>
	<input type="text" value="" name="FNAME" class="" id="mce-FNAME">
</div>
<div class="mc-field-group">
	<label for="mce-LNAME">Last Name </label>
	<input type="text" value="" name="LNAME" class="" id="mce-LNAME">
</div>
<div class="mc-field-group input-group">
    <strong>Email Format </strong>
    <ul><li><input type="radio" value="html" name="EMAILTYPE" id="mce-EMAILTYPE-0"><label for="mce-EMAILTYPE-0">html</label></li>
<li><input type="radio" value="text" name="EMAILTYPE" id="mce-EMAILTYPE-1"><label for="mce-EMAILTYPE-1">text</label></li>
</ul>
</div>
<p><a href="https://us8.campaign-archive.com/home/?u=392c9bd75a4f1ba26b9bffe0a&id=cdcf56eed1" title="View previous campaigns">View previous campaigns.</a></p>
	<div id="mce-responses" class="clear">
		<div class="response" id="mce-error-response" style="display:none"></div>
		<div class="response" id="mce-success-response" style="display:none"></div>
	</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_392c9bd75a4f1ba26b9bffe0a_cdcf56eed1" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
    </div>
</form>
</div>

<!--End mc_embed_signup-->





## About the GCmobility Mailing List

GCmobility is moving away from using mailing lists managed by desktop e-mail software and spreadsheets of contacts. We are moving towards a more modern mailing list system. We have chosen to leverage Mailchimp as our e-mail marketing platform.

As a project team at GCmobility, we can remain confident that recipients of our mailings are in fact interested in receiving our updates. It also helps us achieve our goals of working in the open by default by allowing anyone to subscribe.

As a stakeholder Mailchimp allows you to better control what mail lands in your mailbox. If you prefer to unsubscribe from this mailing list, you may do so at any time by following the UNSUBSCRIBE link below. You can also re-subscribe at some point in the future if you wish. You can also UPDATE YOUR PREFERENCES to explore other options for receiving these mailings.

The Frequency of mailings may fluctuate slightly from week to week. At a minimum we will aim to send out an e-mail at the end of each iteration, which is every 2 weeks. We do not anticipate to send out more than 2 mailings in a given week. We anticipate on average 2-4 mailings each month.

Thank you for your support of the GCmobility Initiative to date. We hope you will continue along this journey with us and remain as a subscriber. However if your interests are no longer aligned to our vision of a mobile Government of Canada then we will be sad to see you go and appreciate all your contributions to date.
