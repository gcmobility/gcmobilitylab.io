Title: Reports
Date: 2020-02-06
Modified: 2020-02-06
Category: Pages
Tags: pages, reports
Slug: gcmobility-impact-and-failure-reports
Toc_Include_Title: false
Status: published

# GCmobility Impact and Failure Reports

Welcome to the GCmobility Impact and Failure Reports Page

This page is meant to provide some guidance and resources concerning the GCmobility Impact and Failure Reports. This page may change frequently over time so check back often.

Some of the resources here link to external sites.

Some of the content here is copied from external sources. We have tried our best to attribute and provide links where appropriate.

# GCmobility Reports
This section highlights current and past GCmobility Impact and Failure Reports

##Current Report
* GCmobility Impact and Failure Report 2020
  * [Call for Stories]({filename}/posts/2020-gcmobility-failure-report.md)
  * [Draft Outline](https://docs.google.com/document/d/1Sz3B-kQfh64bUZqK5ujNqiTXqbFr8Cn8IYgqyMDXykg/edit?usp=sharing)
  * Impact and Failure Report Minimum Viable Product (MVP)
  * Draft Report (pending)

## Previous Reports
* GCmobility Impact and Failure Report 2020 (pending)

# Guidelines

## Language of your choice

Submit in the official Canadian language of your choosing (Cette équipe est composée d'un fonctionnaire anglophone qui a une capacité modérée de fonctionner dans ma deuxième langue, avec un budget et des ressources limités. Cela dit, n'hésitez pas à m'aider à pratiquer en résumant des histoires en français)

## Writing Guidelines

* Translation: Translation capacity is limited, if you have the capacity to submit translated works that would help us out.
* Proposals: Before investing too much time into a full story, please consider submitting a proposal of your story. This will help us achieve our goal of having a balance of stories across multiple themes.
* Drafts:
* Final Edits:

## Submission Guidelines
We will be developing a few mechanisms to submit Impact and Failure stories. This section will evolve in time.

To better focus our attention on what mechanism you might want to submit your story through, please complete our submission survey (pending):
* download the Word Doc template (pending)
  * Open the Template
  * save a new version of it locally
  * complete all sections
  * e-mail us your completed story.
* Copy The Google Doc Story Template (pending)
* Submit a story through a google form (pending)
* submit a story through Gitlab (pending)
* e-mail us you story (pending)

# About Impact and Failure Reports
Learn more about Impact and Failure Reports
* Engineers Without Borders
  * [2015 Failure Report](http://reports.ewb.ca/failure/)
  * [2016 Failure Report](http://reports.ewb.ca/category/2016-failure-report/)
  * [2017 Failure Report](https://www.ewb.ca/wp-content/uploads/2018/08/EWB_FAILURE-REPORT_EN_03-08-2018-pages.pdf)

# Frequently Asked Questions
## Where are is your FAQ?
* You found it. Please feel free to submit a question to our FAQ through our [Contact Us]({filename}/pages/contact-us.md).
