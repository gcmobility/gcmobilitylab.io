Title: Privacy Policy
Date: 2020-02-17
Modified: 2020-04-29
Tags: pages
Slug: privacy-policy
Toc_Include_Title: false
Status: draft


*This Page is considered DRAFT*

Welcome to our privacy policy page. This page represents a series of statements that will help provide context around the nature and type of content found on this site.

## Note to Reader:
This privacy policy may reference various Government of Canada policy instruments

  The Directive on Service and Digital will take effect on April 1, 2020. It will replace the Directive on Management of Information Technology, the Directive on Information Management Roles and Responsibilities, and the Directive on Recordkeeping.

  The Directive on Management of Information Technology, the Directive on Information Management Roles and Responsibilities, and the Directive on Recordkeeping remain in effect until April 1, 2020.

## Privacy Notice
This privacy notice is to comply with [Appendix B: of the Standard on Privacy and Web Analytics](https://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=26761).

Some personal information may be requested or exchanged as you interact with this site.

### A statement setting out the legislative authority for the collection of this information.

The GCmobility initiative does not currently have any expressed authority for the collection of personal information to be collected for web analytics. the GCmobility initiative uses Web Analytics to better understand its users. Users are free to choose if they interact with http://gcmobility.gitlab.io.

While GCmobility leverages third part platforms to support web analytics, GCmobility does not currently operate any web servers where web logs are management.

Third party platforms (eg Google and GitLab) may be gathering personal information in support of their services.

### An explanation of what Web analytics is and the purposes for use of Web analytics tools by the institution.

[Wikipedia describes web analytics](https://en.wikipedia.org/wiki/Web_analytics) as "the measurement, collection, analysis and reporting of web data for purposes of understanding and optimizing web usage.[1] However, Web analytics is not just a process for measuring web traffic but can be used as a tool for business and market research, and to assess and improve the effectiveness of a website. Web analytics applications can also help companies measure the results of traditional print or broadcast advertising campaigns. It helps one to estimate how traffic to a website changes after the launch of a new advertising campaign. Web analytics provides information about the number of visitors to a website and the number of page views. It helps gauge traffic and popularity trends which is useful for market research."

The [Standard on Web Usability](https://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=24227) highlights the requirement "that users can effectively and efficiently find, understand and use the information and services provided through websites and Web applications.". Web analytics supports in part this requirement.

The purpose of web analytics for GCmobility is to provide a passive tool in support of continuous improvement and iterative design of the site and the services associated with the site.

### A statement as to what specific personal information, including the IP address, is being automatically collected from visitors by the government institution.

To better understand what information is being gathered for the purpose of web analytics please consult these sources:
* Google analytics
  * [GA Privacy Policy](https://policies.google.com/privacy?hl=en)
  * [Demographics and Interest Reports](https://support.google.com/analytics/answer/2700409?hl=en-GB&utm_id=ad)
  *  [Demographic Targeting](https://support.google.com/google-ads/answer/2580383?hl=en-GB&utm_id=ad)
  * [Audience Targeting](https://support.google.com/google-ads/answer/2497941?hl=en-GB&utm_id=ad)

To better understand any personal information potentially gathered by the web hosting platform (gitlab.com) please consult the [GitLab Privacy Policy](https://about.gitlab.com/privacy/).

### A statement advising visitors as to whether the Internet Protocol (IP) address and other data in digital markers is being collected and used internally by the institution for the purpose of Web analytics or is being disclosed or transmitted externally to a third party for that purpose.

* Internet Protocol (IP) address and other data in digital markers are NOT being collected and used internally by GCmobility. However, they may be collected and used by third parties.
* No personal information is being disclosed or transmitted externally to a third party for the purpose of web analytics by GCmobility.
* IP address, digital markers, and personal information MAY be collected, disclosed or transmitted BETWEEN the web hosting platform (gitlab.com) and the web analytics platform (Google Analytics). GCmobility does not broker these transactions, however may benefit from them in the form of web analytics reports.

### In cases where the IP address and other data in digital markers is disclosed or transmitted to a third party, an explanation of how the privacy of visitors to Government of Canada websites is being safeguarded through, at a minimum, the activation of the third-party anonymization feature whereby the third party depersonalizes the IP address.

For more details on how [IP Anonymization (or IP masking) in Google Analytics works](https://support.google.com/analytics/answer/2763052?hl=en) please consult their documentation.

### If data disclosed or transmitted for Web analytics is going outside of Canada, for example to the United States, a statement to that effect along with reference to any governing legislation that the information might be subject to, for example the USA Patriot Act.

Data disclosed or transmitted for Web analytics MAY be going outside of Canada. This might expose users of this site to non Canadian governing legislation such as the [USA Patriot Act](https://www.justice.gov/archive/ll/highlights.htm) and the  [Digital Mellennium Copyright Act](https://www.copyright.gov/reports/studies/dmca/dmca_executive.html)


### A statement as to the maximum retention period for any personal information collected in relation to Web analytics.

No decision has been made as to the maximum retention period for any personal information for purposes of web analytics have been made for GCmobility.

For information is provided by Google analytics on retention period for specific types of data. Check out the [GA Data Retention information page](https://support.google.com/analytics/answer/7667196?hl=en)

## GCmobility Site Ownership
GCmobility is an internal initiative to the Government of Canada. The site http://gcmobility.gitlab.ca is a concept site currently under development experimenting with concepts related to working in the open by default. The current official community site for GCmobility is hosted on GCcollab and requires an account for accessing these resources. Both this site and the GCcollab sites are managed by the GCmobility initiative team.

## Expressed Opinions
By accessing this site you may be exposed to information, analysis and opions ralated to the content of this site. Because we are practicing being open by default and moving fast, not all information, analysis or opinions found on this site will have been fully vetted. please consider any opinions expressed in this content to represent those of the GCmobility initiative team or individual authors contributing content. Opinions expressed may not be representative of the Government of Canada, or the Federal Public Service as a whole. We do our best to keep content and opions appropriate. Thanks for your support while we experiment with being open with the information we share, analysis we conduct and opinions that may develop from this initiative.

## Content Ownership
Content appearing on this site belongs primarily to the authors of the content. The content of this site is publicly available through the GCmobility Git repository hosted on http://gitlab.com. Content that appears on this site may also be referenced through other platforms. GCmobility is not able to guarantee the pedigree or appropriate stewardship of information posted on this site beyond this site. For example, if content on this site is shared via Twitter, then that shared content falls under the privacy policies of that platform.

## Information Resource of Business Value
Most content on this site is considered transitory and may not be considered Information Resources of Business Value according to the [TBS directive on Record Keeping](https://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=16552).

## Privacy Policy Context
Although this site is not part of the official family of http://[dept].gc.ca or http://canada.ca domains, as a prototype site we are working towards transparency including iteratively developing this privacy policy page.

## Content and Data Sharing
Content on this site may reference additional information and data resources such as supporting documents, references, reports or datasets. Where possible we work to provide appropriate attribution. Maintainers of this site as well as individual authors, readers or others may share content, information and data found on this site with third party platforms. Once artifacts are posted to these other networks the privacy policies of those networks take over.

## Content Licensing
Unless otherwise noted, all content, information and data found on this site is dual licensed between the authors and the Government of Canada.  Authors can choose to licence their own content how they wish, while acknowledging it will also be licensed under [The Government of Canada Open Government Licence – Canada agreement](https://open.canada.ca/en/open-government-licence-canada).

### You are free to:

   Copy, modify, publish, translate, adapt, distribute or otherwise use the Information in any medium, mode or format for any lawful purpose.

### You must, where you do any of the above:
       * Acknowledge the source of the Information by including any attribution statement specified by the Information Provider(s) and, where possible, provide a link to this licence.
       * If the Information Provider does not provide a specific attribution statement, or if you are using Information from several information providers and multiple attributions are not practical for your product or application, you must use the following attribution statement:
   "Contains information licensed under the Open Government Licence – Canada."

   The terms of this licence are important, and if you fail to comply with any of them, the rights granted to you under this licence, or any similar licence granted by the Information Provider, will end automatically.

## Standard on Privacy and Web Analytics
Source: Standard on Privacy and Web Analytics

[The Standard on Privacy and Web Analytics](https://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=26761) "Aims to facilitate the use of web analytics in accordance with sound privacy practices that safeguard the privacy of visitors to Government of Canada websites." The "Privacy Notice" at the begining of this page addresses the requirements of this standard.

Please note that third party privacy policies are at play for visitors to this site.

### Third Party Privacy Policies

* [GitLab Privacy Policy](https://about.gitlab.com/privacy/).
* [Google Drive Terms of Service](https://www.google.com/intl/en-GB_ALL/drive/terms-of-service/)
* [YouTube Terms of Service](https://www.youtube.com/static?gl=CA&template=terms)
* [YouTube Privacy Policy](https://www.youtube.com/t/privacy)
* [Google Analytics Privacy Policy](https://policies.google.com/privacy?hl=en)


## Known
