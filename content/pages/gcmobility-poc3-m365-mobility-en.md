Title: GC Mobility Proof of Concept: M365/Teams Pilot for Mobile employees
Date: 2020-05-21
Modified: 2020-06-09
Category: Blog
Tags: pilot, proof of concept 
Slug: gcmobility-poc-m365-mobility
Toc_Include_Title: false
Status: hidden
lang: en
Summary: GCmobility is offering another opportunity to Canada's Free Agents to participate in some mobility research. This time it is a exploratory usability study using Microsoft 365. We will pilot the use of [GCcollaboration.ca](http://gccollaboration.ca), the government of Canada's response to enabling federal public servants to remain connected and productive during the COVID-19 quarantine in support of remote and telework. Thwe study period will be 4 weeks. Users are required to complete a pilot registration form and are also expected to complete an exit survey at the end of the 4 weeks. Additional survey's and questionnaires may be requested during and after the study period.

## Summary

* If you just want to get started and join the pilot please [complete the registration form below](#pilot application form_1).
* A primary objective of GCmobility is to decrease crossboarding time between departments.
* This pilot is the latest opportunity for Canada's Free Agents to participate in some mobility research. 
* This pilot is a unsupervised-remote-exploratory usability study using Microsoft 365. 
* The pilot will make use of [GCcollaboration.ca](http://gccollaboration.ca), the government of Canada's response to enabling federal public servants to remain connected and productive during the COVID-19 quarantine in support of remote and telework. 
* The initial study period will last for aproximately 4 weeks
* Users wishing to continue using the system and participating in furth user research may continue using the system after the initial pilot.
* GCcollaboration.ca as a system will be shutdown in September 2020, you must ensure your data and information is managed appropriateley.
* This system is only approved for unclassified and non protected infomration and data.
* If you want some more context then by all means keep reading.
* As the pilot progresses, we will be adding to the list of [Frequently Asked Questions](#frequently asked questions)
* If you are ready and willing to participate in this lates pilot then please skip ahead to [complete the registration form](#pilot application form). The registration form should take 5-10 minutes to complete.

Thanks for your continued support for increasing career and workforce mobility within the Government of Canada.

## Important Notes

In order to gather some basic user information we have developed a registration form which includes a series of questions related to the pilot. Completing this form is required to particiapte in this pilot. The form should take you 5 to 10 minutes to complete. At the end of the pilot we will ask users to complete an exit questionnaire. The results of the pilot will rely in part on the findings developed by comparing the pre and post questionnaires. As a test user you may be asked to complete other shorter surveys and questionnaire throughout the study period.

The main study period will last 4 weeks once the bulk of users have onboarded to the system. This should be enough time to allow users to log into the system, explore some of the tools and experiment with including the tools into their every day work. Users are welcome to continue to use the system after the initial pilot period.

**IMPORTANT** GCcollaboration.ca is a temporary solution to solving some access issues during COVID-19. As asuch, this system should not be considered production. All information on the system should be considered unclassified. This system is not considered an official repository for Information Resources of Business Value (IRBV), so please use your regular departmental document management systems as required. Again, GCcollaboration.ca is only temporary and is planned to be shutdown in September 2020. Please ensure all material or corespondence developed during this pilot is properly managed, removed or deleted by the end of August 2020.  

## Context

The GCmobility initiative with the help and support of Shared Services Canada (SCC), Digital Centre For Collaboration (DCC) and other Free Agents have launched the M365/Teams Pilot for Mobile employees.

GCmobility's primary objective is to decrease crossboarding time between departments. There are many "systems" implicated in this long crossboarding adventure. From People and policies to processes and technologies. The problem can sometimes seem so big that the only solution is to short circuit everything. But maybe there is a better way. 

One potential solution to the crossboarding problem is to minimize the number of IM/IT systems we need to shuffle between when switching gigs. GCmobility is certainly not the only project in town with this idea. A growing number of Free Agents are actually working to solve simmilar problems in their own current gigs, and we have tried to reach out to as many of these initiatives as possible. While the official ask has come from GCmobility to SSC, this is not purely being driven by GCmobility, a number of Free Agents have also been critical to get a green light from SSC. 

For those already using GCcollaboration.ca you may not notice much change, except maybe yet another log in (I know that will be annoying). For those confined to another departmental instance of Teams you might find a wider audience to collaborate with during this pilot. [Aproximately 20 depts are already using GCcollaboration.ca](https://wiki.gccollab.ca/GCcollaboration_Department_Listing). 

### The Challenge of a Growing Mobility Toolbox 

We know you already have your tools and may be overwhelmed with so many tools championed by so many unique groups. This situation may have forced you to manage a growing number of communication channels. We are not asking you to abandon any tools that are serving you well. However, we do need your help! 

Through this pilot, you will help us seize an opporunity to gather some real user input and feedback about m365/Teams, a system that is being rolled out across government anyhow. We want to hear from all Free Agents as well as other interested parties such as the various HR mobility programs that continue to deliver strong value throughout the GC. We need a broad selection of voices to be heard to inform future decisions. 

We recognize that Slack, Google Drive, and Zoom are working for many people. These tools have been adopted by many in part due to a lack of other options. Our plan is not to prevent the use of your existing tools. However, there seems to be a growing value proposition in being able to have a stable and consistant "GC approved collaboration system" that we can use while posted to any department. Our goal is to be able to accessing the people and resources we need, without having to constantly change e-mails, move Mykeys, gain access to individual VPN's, learn new in-house document management practices, add to our growing list of passwords and wait 6-10 weeks for an approved laptop or phone. 

### Testing Groups

Free Agents, as a growing group of mobile workers have been identified (again) as a key test group for this pilot, however we are wanting to design this pilot to have scaling potential beyond FAZZ to include SSC embedded staff, other HR Innovation programs as well as others less formally recognized groups (stay tuned). 

As part of our planning process for this pilot we have already engaged a selection of Free agents in an infomral chat to bring them up to speed on this opportunity for Free Agents to manage their own corner of GCcollaboration.ca. This pilot will give us a chance to test features that some departments may have locked down or not enabled. We would have the opportunity to manage our own "Team" area and explore how threaded conversations work. We would be able to invite a certain number of guests to explore what functions and access they would have. Of particular interest we want to see how collaboration works with other tennants. All of these areas are big unknowns for many throughout the GC, and Free Agents would get a chance to peak behind the curtains if they wish. 

While we have a green light from the SSC Digital Collaboration team to onboard all Free Agents we know this is unlikely to happen. Our goal is to get 20-30 Free agents from the 3 home depts. To test things out, complete a questionnaire after a month of use and maybe particpate in some one on one discussion. The findings would be gathered, compiled, analyzed and shared with various groups within SSC. We also intend to share findings and our experience in the open throughout. 

If you know of another HR mobility program that might benefit from participating in this pilot please [contact us and lets us know]({filename}/pages/contact-us.md)
  
### Competing Priorities

We get it. Times are tough, stress levels are high, everyone is sharing their solutions in a flood of resources and we may find it hard to carve out time for side of desk discussions. Add to that washing our hands, trying to NOT to speak too moistly, entertaining kids, letting dogs out to go pee, cuddling our cats, staying connected with friends and family all while trying to stay safe and stay sane. 

We hope despite all this you join the pilot, or at least complete the form below and share your thoughts on why this solution may or may not work for you. 

### Usability Study for Mobility 
This pilot will be conducted in part to gather some usability data. The usability portion of this pilot will be conducted as an unmoderated-remote-exploratative test ((https://www.hotjar.com/usability-testing/methods/). The core testing period will be for a period of 4 weeks, done without direct supervision, while the participants integrated MS teams into their daily work cycles working remotely, typically from home. The exploratory nature of the testing keeps the user experience open ended, allowing participants to explore all the tool offerings they think will be beneficial.

This pilot will include many "pilots within a pilot". The offering of tools and applications within M365/Teams on Gccollaboration.ca is quite large. Part of the pilot will be to use, test and push these tools hard to see what works and what breaks. Along the way we hope to find some diamonds in the rough. One such pilot within a pilot is the use of this form designed using "Forms For Excel". So the form is designed in part to facilitate a usability study but we also wanted to test multiple functions of the tool. So the design could certainly be improved, but sometimes we just need to try things out.

At the end of the 4 week test period you will be asked to complete another survey. Some questions will be similar to measure pre and post study outcomes. Some questions will be unique to each phase of the study.

After the study period, pilot participants will be invited to continue using the GCcollaboration.ca platform as long as the system remains live. The gccollaboration.ca system is a temporary solution implemented by SSC as part of the Government of Canada's response to the COVID-19 pandemic. As such the system should be considered temporary and its use could be discontinued without warning. 

### Volunteer to Join the Pilot Team

In order for this pilot to be successful we require some volunteers from each participating department to help out. The initial pilot will focus on Canada's Free Agents home departments including NRCAN, TC, TBS and ISED. Volunteers will be expected to use, test, comment and provide feedback on the selection of MS365 tools that will be used to support volunteer work. Your input as a volunteer in this pilot is as valuable as the users testing the system in general during this pilot.

Our minimum volunteer requirement is one volunteer from each of the 4 home departments to run the pilot. The ideal operational end state of the pilot, if it scales well, is to have one individual for each role from each home dept. Given 4 full cohorts from each department or 120 Free Agents and 4 roles frome each department (4 x 4 = 16), we could requires up to 16 volunteers our of 120 Free Agents. For the purpose of this pilot if the volunteer capacity is very high we may not need all volunteers. However, if the capacity is too low we may not be able to run all aspects of the pilot, and may not have the capacity to provide additional user supports. 

**Please Be Warned**: We do not know what volunteers should expect. We do not have an expected minimum number of hours for volunteering, contribute what you can. There is no official training to be offered for these volunteer roles. Documentation will be mainly from what you can find online. You will need to work as part of the overall team of system administrators. These roles will be uncertain and flexible.  Google is your friend.   Anything beyond that is a bonus. If you change your mind either way in the future that is OK.

## Frequently Asked Questions

### What does joining this pilot mean?
**Question**: For someone less familiar with what a new instance of MS365/Teams entails, will I require another account, another email? what's possible?

**Answer**: While the result of this pilot may be a new account and email etc. The goal is to decrease the number of logins and accounts and emails. This is something we hope to test. Imagaine a single account and email to access all the various Teams you already belong to.

### Proliforation of MS365/Teams
**Question**: Some are much more familiar with MS Teams, and have up to 4 existing Teams accounts. Do we really need another instance? 

**Answer**: Imagine if you could access 4 M365/Teams instances through a single account? And as you float in and out of various teams your credentials and identity stay the same. This is something we want to test.

### Quality of Communications
**Question**: Call quality on MS Teams has been very good so far, compared to other options we've tried like Webex. Now up to 9 people can be seen on the panel of videos.(More like Zoom.). Will this pilot have the same quality of connections?

**Answer**: Becasue we are using the infrastructure already setup for gccollaboration.ca, the quality should be the same. We  also want to test how we can accomodate users not only from other departments, but also external to GC. We would be interested to know if the quality is impacted under these scenarios.

### Good IM Practices
**Question**: Is using another email account not be a weird IM practice? Departments are already encouraging this by providing separate emails for MS Teams. Will your pilot address this?

**Answer**:  Multiple accounts and emails, and file shares etc is in fact a POOR IM practice, especialy if you consider the Government of Canada as the "enterprise" instead of each department. However, for the pilot more emails and accounts will likely be the case. We do however want to demonstrate that stronger IM practices are possible for mobile, embeded and collaborative workers. The current state is that many Free Agents already have multiple e-mails and put little out of office messages pointing people to their new e-mail. We are aware this is a concern anf hope to report on some positiive solutions after this pilot.

### Supprting Support Staff of Mobile Employees
**Question**: Mobility is less of a concern for some employees such as hiring managers and Talent Managers. What will this pilot offer these stakeholders?

**Answer**: While we agree that career mobility may be less of a concern for support teams, hiring managers and talent managers (TM), the current hypothesis would argue these stakeholders themselves are in the business of workforce mobility. For example, imagine a day that a TM from TBS or TC could play a backup role to an NRCAN TM on leave? Currently it would be dificult to play that role across departments for a TM tp play backup for another TM when all parties can not even share a calendar to book a bilat, or the TM can not access some basic staffing information about the Free Agent. MS Teams may not fix all the IM issues, however, from a nhuman perspective it should help the Free Agent program be a bit more coherent across the Free Agent stakeholder population.

### Tools Fit For Purpose
**Question**: A principal worry is about how many tools we’re using, and the reasoning for using those tools. There's a line between productivity and taking on too many tools. Reluctant to check another email. Why would you risk introducing yet another tool? 

**Answer**: As a result of this pilot we should be able to develop some guidance on managing multiple THINGS, while striving for best practices in IM. The reality is that MS teams is here to stay for the next technology procurement cycle anyhow. Even more roll outs of MS365/Teams will occur within the GC moving forward. The organizers of this pilot recognize that many "assumptions" have been made about how things will work, however little testing or explaoration has been done to validate this. We hope the findings from our small pilot can help inform decision makers across the GC of how the future roll outs of these tools will occur. So we welcome the feedback.

### Tool Saturation
**Question**: Employees are suffering from some tool saturation (4 Slacks, 4 MS Teams accounts, plus email, Twitter, test, phone...). Is the goal of the pilot to remove our existing tools?  

**Answer**: Agreed, even tech savy users acustomed to online collaboration tools are starting to feel the saturation. We also feel that while MS Teams is a useful tool even if it may not be the best tool for everyones needs all the time. Our goal is not to determine if MS365/Teams should be used moving forward, that ship has sailed. Our goal is to figure out the best way that Free Agents and the Free Agent program can leverage the tool features to support carreer and workforce mobility. At the same time, our initial anecdotal research suggests some people have rejected MS365/Teams before realizing it may actually fix some of the issues that people complain about regularly. Since this is a pilot, we are in no position to force the use of the tool, but we are providing an opportunity to discover how it can best serve the stakeholders.

### Generic E-Mail Addresses
**Question**: Is it possible to have a program-wide email not housed at a home department in future? (Currently FA inbox can only be housed at NRCan or CSPS, since it's canada.ca) Question for Dave. 

**Answer**: A great question. The way the pilot is being rolled out will treat the "instance" like a separate department. However, the main premis of the pilot is that MS365/teams should be program and departmental agnostic. This means that we want Free Agents to look like any other public servant, and not a member of any particular department. This is not to say that Canada's Free Agents (CFA) can not have its program needs met, however we believe a single identity for all public servants is better than 250 separate identies split down financial coding lines like departments. The overhead in managing the cross boarding between all of the IT systems is a source of large delays during the crossboarding procee. Such program requirements may include the ability to set up generic e-mails, but this is yet to be confirmed. From a sustainability perspective, if the FA program wanted a perpetual MS 365/Teams instance moving forward then we need to think about financing and governing it (perhaps contributions from home depts). The benefit of the pilot is the opportunity presented by COVID-19 to test out these features and influence the patterns we use to organize it. All of this will be possible by working outside the GC firewal which currently present challenges to many public servants including Free Agents. Once the pilot is in full flight we can look at running smaller mini pilots within the pilot for instance, setting up a generic e-mail.

### Multiple Teams 
**Question**: I already have teams active on one or multiple e-mails. Can I still be in the pilot? 

**Answer**: Yes. As part of the pilot all users will get a new E-mail in the patterns of first.last.canada@gccollaboration.ca . All you need to register is a primary and recovery e-mail address you can access at the time of signing up. The primary e-mail wil be used to send the invite and initial login details. Once you have logged in for your first time we can then experiment with requesting access to addition Teams at various departments.

### What Devices Work 
**Question**: Would I need to be using a government tablet during this pilot, or would my [Choose Device] be feasible? 

**Answer**: During this pilot, the GCcollaboration.ca platform uses a public facing instance of M365/Teams. As such the following types of devices should work during the pilot.

* Host Owned Devices: PC / Laptop / Phone / Tablet  (as long as the device can access the public internet)
* Home Owned Devices: PC / Laptop / Phone / Tablet  (as long as the device can access the public internet)
* Personal Devices: PC / Laptop / Phone / Tablet  (as long as the device can access the public internet)





## Pilot Application Form 
([Direct Link to the form](https://forms.office.com/Pages/ResponsePage.aspx?id=QstjKPSk1UG4g5fEh-5B_5xcdsxvxRZLq3fPW6MfLYdURjI3SjU0RzlDMlBVVzdaUEdOTlM5WDk3RC4u) if it fails to load below)

<iframe width="100%" height= "1600px" src= "https://forms.office.com/Pages/ResponsePage.aspx?id=QstjKPSk1UG4g5fEh-5B_5xcdsxvxRZLq3fPW6MfLYdURjI3SjU0RzlDMlBVVzdaUEdOTlM5WDk3RC4u&embed=true" frameborder= "0" marginwidth= "0" marginheight= "0" style= "border: none; max-width:100%; max-height:100vh" allowfullscreen webkitallowfullscreen mozallowfullscreen msallowfullscreen> </iframe>
