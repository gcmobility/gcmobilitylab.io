Title: GCmobility Service Requests(Alpha)
Date: 2020-04-20
Modified: 2020-04-20
Tags: pages, service
Slug: service-request
Toc_Include_Title: false
Status: hidden


By completing the form below and selecting submit, your local e-mail client will open with a templated e-mail. Please do not modify the content or format of the e-mail before sending.

If you are a new user to our service please read more [About the GCmobility Service]({filename}/pages/service-about.md)

## Service Request Form

<form id="request-form" action="mailto:gcmobility.canada.ca@gmail.com?subject=Service Desk Request" method="post" enctype="text/plain">


<fieldset>
<legend><b>Client Details</b></legend>
<input type="hidden" name="## Client Details" id="markdown-heading" value="    ">
<textarea id="blank-line" style="visibility:hidden" rows="1">
</textarea>
<p>Please provide us some basic contact details</p>
<ul>
<li><label for="first-name">First Name:</label>  <input type="text" name="* [ ] first-name"></li>
<li><label for="last-name">Last Name:</label>  <input type="text" name="* [ ] last-name"></li>
<li><label for="primary-email">Primary E-mail:</label>  <input type="text" name="* [ ] primary-email"></li>
<li><label for="alt-email">Alternate E-mail:</label>  <input type="text" name="* [ ] alt-email"></li>
<li><label for="phone">Phone (optional):</label>  <input type="text" name="* [ ] phone"></li>
</ul>

</fieldset>

<fieldset>
<legend><b>Pre-Flight Checklist</b></legend>
<input type="hidden" name="## Pre-Flight Checklist" id="markdown-heading" value="    ">
<textarea id="blank-line" style="visibility:hidden"></textarea>
<p>Before launching a new ticket please ensure that you have consulted the following resources. <br><br> click the checkbox(<input type="checkbox" name="sample" id="sample" value="sample" checked="checked" >) for all that apply:</p>
<ul>
<li><input type="checkbox" name="* [x] toolbox" id="toolbox" value="yes" ><label for="toolbox">Free Agent Toolbox (GCpedia)</label> </li>
<li><input type="checkbox" name="* [x] manual" id="manual" value="yes" ><label for="manual">Cross Boarding Manual for Canadian Federal
Mobile Workers</label> </li>
<li><input type="checkbox" name="* [x] mykey" id="mykey" value="yes" ><label for="mykey">MyKey Standard Operating Procedure for Free Agents (GCpedia)</label> </li>
<li><input type="checkbox" name="* [x] tech-problems" id="tech-problems" value="yes"><label for="tech-problems"> #techproblems channel in the Free Agent Slack Environment</label> </li>
<li><input type="checkbox" name="* [x] other" id="other" value="yes"> <label for="other">Other Resources</label> </li>
<li><label for="resources">Other Resources:</label> <textarea name="resources" id="resources" cols="25" rows="3"></textarea></li>
</ul>
</fieldset>

<fieldset>
<legend><b>Mobility Details</b></legend>
<input type="hidden" name="## Mobility Details" id="markdown-heading" value="    ">
<textarea id="blank-line" style="visibility:hidden"></textarea>
<p>Please enter the basic details of what department you are moving from (source) and to (destination) as well as what your home department is</p>
<ul></br>
<li><label for="home-dept">Home Department:</label>
<select name="* [x]  home-dept" id="home-dept">
  <option value="">Select Your Home Department location</option>
  <option value="ISED">Innovation, Science, Economic Development Canada</option>
  <option value="NRCAN">Natural Resources Canada</option>
  <option value="PCO">Privy Council Office</option>
  <option value="PSPC">Public Services and Procurement Canada</option>
  <option value="SSC">Shared Services Canada</option>
  <option value="TC">Transport Canada</option>
  <option value="TBS">Treasury Board Secretariate</option>
</li>
<li><label for="source-dept">Source Host Department:</label>  <input type="text" name="* [x] source-dept"></li>
<li><label for="destination-dept">Destination Host Department:</label>  <input type="text" name="* [x] destination-dept"></li>
</ul>
</fieldset>
<fieldset>

<legend><b>Service Type</b></legend>
<input type="hidden" name="## Service Type" id="markdown-heading" value="    ">
<textarea id="blank-line" style="visibility:hidden"></textarea>
<p>Please select the service type that best matches the issue you are facing. If you have multiple issues of multiple types please launch one request for issue type</p>
<ul>
<li><label for="service-type">Service Type:</label>
<select name="* [x] service-type" id="service-type">
  <option value="">Select a Service Type</option>
  <option value="full-service /label ~'Full Service'">Full Crossboarding Service</option>
  <option value="poc3-ms365-teams-pilot /label ~'POC3 365'">POC3: MS365/Teams Pilot</option>
  <option value="opportunity /label ~Opportunities">Opportunity Hunting</option>
  <option value="accessibility /label ~Opportunities">Accessibility</option>
  <option value="accommodation /label ~Accommodation">Accommodation</option>
  <option value="agreement /label ~Agreement">Letter of Agreement</option>
  <option value="data-mgnt /label ~'Data Management'">Data Management</option>
  <option value="devices /label ~Devices">Devices</option>
  <option value="email /label ~Email">E-Mail</option>
  <option value="information-mgnt /label ~'Information Management'">Information Management</option>
  <option value="mykey /label ~MyKey">MyKey</option>
  <option value="network /label ~Network">Network Accounts</option>
  <option value="security /label ~Security">Security</option>
  <option value="service-desk /label ~'Service Desk'">Service Desk</option>
  <option value="software /label ~Software">Software</option>
  <option value="telework /label ~Telework">Telework</option>
</li>
</ul>
</fieldset>

<fieldset>
<legend><b>Service Request Description</b></legend>
<input type="hidden" name="## Description" id="markdown-heading" value="    ">
<textarea id="blank-line" style="visibility:hidden" rows="0"></textarea>
<p>Please describe the nature of the mobility or crossboarding issue you are currently facing. Include any steps already completed, or resources already used. Providing contact information of other people you have already been in contact with when trying to solve this issue would also be useful. </p>
<ul>
<li><label for="description">Request Description:</label> <textarea name="description" id="description" cols="25" rows="10"></textarea></li>
</ul>
</fieldset>
<input type="hidden" name="gitlab" id="gitlab" value="/label ~Priority::Medium ~New ~"Service Desk"">
<input type="submit" value="Send">
<input type="reset" value="Reset">
</form>


If the form above fails, then try to **launch a service request** by [sending us an email to the GCmobility Service]( mailto:gcmobility.canada.ca@gmail.com?subject=GCmobility%20Service%20Desk%3A%20%5BShort%20Issue%20Title%5D&body=%23%23%20Client%20Details%3A%0D%0A%0D%0A**Client%20Name**%3A%0D%0A**Home%20Department**%3A%0D%0A%0D%0A**Source%20Department%20(leaving)**%3A%0D%0A**Destination%20Department%20(joining)**%3A%0D%0A%0D%0A**Preferred%20e-mail**%3A%0D%0A**Secondary%20e-mail**%3A%0D%0A**Client%20Phone%20(optional)**%3A%0D%0A%0D%0A%23%23%20Existing%20Service%20Desk%20issues%3F%3A%0D%0A%0D%0A(Please%20list%20any%20existing%20service%20desk%20issues%20already%20submitted%20to%20the%20following%20service%20desks.%20Multiple%20tickets%20can%20be%20captured%20using%20comma%20separated%20lists%20%5Beg.%20%239876%2C%20%232345)%0D%0A%0D%0A**Home%20Department%20Service%20Desk%20ticket%20%23's**%3A%0D%0A**Source%20Department%20Service%20Desk%20ticket%20%23's**%3A%0D%0A**Destination%20Department%20Service%20Desk%20ticket%20%23's**%3A%0D%0A%0D%0A%23%23%20GCmobility%20Service%20Type%3A%0D%0A%0D%0A(Please%20change%20'%5B%20%5D'%20to%20'%5Bx%5D'%20next%20to%20each%20of%20the%20following%20service%20types%20to%20categorize%20your%20request%0D%0A%0D%0A*%20%5B%20%5D%20Opportunity%20Hunting%0D%0A*%20%5B%20%5D%20Letter%20of%20Agreement%0D%0A*%20%5B%20%5D%20Security%0D%0A*%20%5B%20%5D%20Service%20Desk%0D%0A*%20%5B%20%5D%20Accommodation%0D%0A*%20%5B%20%5D%20MyKey%0D%0A*%20%5B%20%5D%20E-Mail%0D%0A*%20%5B%20%5D%20Network%20Accounts%0D%0A*%20%5B%20%5D%20Devices%0D%0A*%20%5B%20%5D%20Phones%0D%0A*%20%5B%20%5D%20Software%0D%0A*%20%5B%20%5D%20Accessibility%0D%0A*%20%5B%20%5D%20Information%20Management%0D%0A*%20%5B%20%5D%20Data%20Management%0D%0A*%20%5B%20%5D%20Telework%20(eg.%20VPN%2C%20GCSRA)%0D%0A%0D%0A%23%23%20GCmobility%20Service%20Resources%3A%0D%0A%0D%0A(Please%20change%20'%5B%20%5D'%20to%20'%5Bx%5D'%20for%20each%20of%20the%20following%20resources%20you%20have%20already%20searched%20to%20try%20to%20solve%20this%20problem%20)%0D%0A%0D%0A*%20%5B%20%5D%20Free%20Agent%20Toolbox%20(GCpedia)%0D%0A*%20%5B%20%5D%20Cross%20Boarding%20Manual%20for%20Canadian%20Federal%0D%0AMobile%20Workers%20(https%3A%2F%2Fdrive.google.com%2Ffile%2Fd%2F1zpzGlShuhc7Nr5oZL98Oe2YyYQt-kuR7%2Fview%3Fusp%3Dsharing)%0D%0A*%20%5B%20%5D%20MyKey%20Standard%20Operating%20Procedure%20for%20Free%20Agents%0D%0A*%20%5B%20%5D%20%23techproblems%20channel%20in%20the%20Free%20Agent%20Slack%20Environment%0D%0A*%20%5B%20%5D%20Other%3A%20%5BDescribe%20Here%5D%0D%0A%0D%0A%23%23%20GCmobility%20Service%20Request%20Description%3A%0D%0A%0D%0A(Please%20describe%20the%20nature%20of%20the%20mobility%20or%20crossboarding%20issue%20you%20are%20currently%20facing)%0D%0A(Please%20include%20any%20steps%20already%20completed%2C%20or%20resources%20already%20used%2C%20or%20other%20people%20who%20have%20already%20been%20included%20in%20trying%20to%20solve%20this%20issue%20if%20any).%0D%0A%0D%0A%0D%0A%0D%0A%0D%0A%0D%0A%0D%0A%0D%0A%0D%0A**Thank%20you%20for%20using%20the%20GCmobility%20Service**.%20You%20should%20receive%20a%20confirmation%20email%20within%20the%20next%201-2%20hours.%20If%20you%20do%20not%20receive%20a%20confirmation%20within%20this%20time%20please%20feel%20free%20to%20use%20the%20Contact%20Us%20page%20at%3A%20https%3A%2F%2Fgcmobility.gitlab.io%2Fpages%2Fcontact-us.html%0D%0A%0D%0A%0D%0A%0D%0A%0D%0A%0D%0A%0D%0A%0D%0A%0D%0A%0D%0A%0D%0A)
