Title: Contact Us
Date: 2020-02-06
Modified: 2020-02-06
Tags: pages, contact
Slug: contact-us
Toc_Include_Title: false

Welcome to our contact us page. The main ways to 

## Follow Us
There are multiple ways that you can follow our project progress.

* Join our [Mailing List]({filename}/pages/mailing-lists.md)
* Follow us [on twitter](https://twitter.com/GCmobility)

## Contact Us
Use the form below if you want to reach out to the team directly. Once you submit the form, we will receive an e-mail and get back to you shortly.

<iframe src="https://docs.google.com/forms/d/e/1FAIpQLSexcsO9kIgqovIO1BYwVikoI6LGPPy_HBhsaB8_aIwAEVTimQ/viewform?embedded=true" width="640" height="1206" frameborder="0" marginheight="0" marginwidth="0">Loading…</iframe>


<a href="https://twitter.com/intent/tweet?button_hashtag=gcmobility&ref_src=twsrc%5Etfw" class="twitter-hashtag-button" data-size="large" data-show-count="false">Tweet #gcmobility</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

<script type="text/javascript" src="//downloads.mailchimp.com/js/signup-forms/popup/unique-methods/embed.js" data-dojo-config="usePlainJson: true, isDebug: false"></script><script type="text/javascript">window.dojoRequire(["mojo/signup-forms/Loader"], function(L) { L.start({"baseUrl":"mc.us8.list-manage.com","uuid":"392c9bd75a4f1ba26b9bffe0a","lid":"cdcf56eed1","uniqueMethods":true}) })</script>
