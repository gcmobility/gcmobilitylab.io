Title: About The New GCmobility Service (Alpha)
Date: 2020-04-20
Modified: 2020-04-20
Tags: pages, service
Slug: service-about
Toc_Include_Title: false
Status: hidden

Welcome to the GCmobility Service.

## Submit a Service Request 
**Are you a returning client?**, then go ahead and [Launch a New Service Request]({filename}/pages/service-request.md).

**If you are a new client of this service**, pleas ekeep reading the following sections to learn more about the service.

* [About The GCmobility Service](#about)
    * [Design Principles](#design principles)
    * [Service Status (Alpha)](#service status)
    * [Core Service Components](#core service components)
    * [Supporting Services](#supporting services_1)
* [Getting Started](#getting started)
    * [Before Contacting the GCmobility Service](#before contacting)
    * [Submitting a Service Request](#submit a service request)
    * [After Care](#after care)
    * [Feedback](#feedback)


## About This Service
The GCmobility initiative launched in April 2019, and the GCmobility service launched in April 2020. This service is provided as-is by the GCmobility initiative, a partnership between [Shared Services Canada](https://www.canada.ca/en/shared-services.html) and [Canada's Free Agents](https://wiki.gccollab.ca/Canada%27s_Free_Agents).

This service is currently operating under an Alpha Status and as such is offered to its users in a continually evolving state that might include bus, issues and defects.

The service has a limited number of components at this time

## Target Client
Currently this service is only available to Federal Employees enrolled in the Canada's Free Agent program. As resourcing and staff permit, the intention is to offer mobility to the following groups.

* Embedded SSC staff
* Federal Public Servants enrolled in other HR mobility programs
* All Public servants who move between departments.

## Service Status
This service is in the **Alpha** stage of development. Wikipedia defines this staf of the release cycle as:

> "Alpha software is software that is not thoroughly tested by the developer before it is released to customers. Alpha software may contain serious errors, and any resulting instability could cause crashes or data loss.[3] Alpha software may not contain all of the features that are planned for the final version.[4] In general, external availability of alpha software is uncommon in proprietary software, while open source software often has publicly available alpha versions. The alpha phase usually ends with a feature freeze, indicating that no more features will be added to the software. At this time, the software is said to be feature complete. A beta test is carried out following acceptance testing at the supplier's site (alpha test) and immediately prior to general release of the software as a product." (source: [Wikipedia](https://en.wikipedia.org/wiki/Software_release_life_cycle#Alpha))

## Design Principles
The following principles help to guide the development of this service.

* Empower the client with information to solve their own problems
* Empower clients to support each other
* Connect pre-exisiting systems throughout the enterprise
* Connect the people behind the services to the people using the service
* Reuse before building
* Build simple solutions
* Measure the impact of the services
* Learn from the users experience of the service
* Demonstrate iterative progress on the service.

## Core Service Components
The GCmobility Service offers the following Components

* GCmobility [Consierge Service](#consierge service)
* GCmobility [Resource Library](#resource library)
* GCmobility [Community support](#community support)
* GCmobility [Discussion on GCmessage](#discussion on gcmessage)
* GCmobility [Mailing List](#mailing list)
* GCmobility [News Feeds](#community support)
* GCmobility on [Twitter](#twitter)

### Consierge Service
The consierge service is a service-desk-like service that combines a dedicated person to work through issues with the client. Currently there is only one staff and capacity to only serve Canada's Free Agent Community. To access this service please proceed to [Getting Started](#getting-started)

### Resource Library
The resource library offers clients access to various self-service products to aid in their mobility. To learn more [Visit the GCmobility Resource Library]({filename}/pages/service-library.md)

### Community Support
Community support for the Mobility Service is offered through two main mechanisms: Slack for Free Agents and GCmessage for the broader community. Previously GCcollab Groups was heavily used, however we are in the process of moving away from this platform. 

#### Slack
The [#techproblems channel](https://free-agents.slack.com/archives/C9G7RV4M8) within the Free Agent slack environment provides the most active support to our current target clients within the Canada's Free Agents program. The #gcmobility channel also provides a mechanism to provide project updates.

#### Discussion on GCmessage
As our capacity to support employees beyond the Free Agent program increases we aim to support more mobile employees. To achieve this goal we wanted to open up more general two-way communication channels. Currently there is already a [GCmobility channel](https://message.gccollab.ca/channel/gcmobility) within [GCmessage](https://message.gccollab.ca/home) that is lightly monitored. GCmessage is a text based discussion forum hosted by the GCTools team.

#### GCcollab 
**Depricated**
GCmobility has also used the [Discussion Forums](https://gccollab.ca/discussion/owner/2183003) feature in the GCmobility group on GCcollab to engage with users. However this is mainly for open disucssions for group members and not necesarily monitored by the GCmobility Service.

### Mailing List
The GCmobility mailing list is currently our primary means of sharing project information. If you have joined the GCmobility community on GCcollab, filled out our stakeholder intake survey, been identified for our stakeholder inventory or been in generla communication with the GCmobility project you might already belong to this list.

The mailing list is currently managed through a spreadsheet and an MS Outlook contact list. Mailings are currently handled through using the "mail merge" feature of MS Office.

With over 300 stakeholders and now multiple communication channels we are looking into an alternative means of managing the mailing list. So stay tuned for what is next.

### News Feeds
As you surf various site throughout the World Wide Web (The Web), you may from time to time come across "news feeds". The two predominant standards for news feeds include Realy Simple Syndication (RSS) and the Atom Publishing Protocal (Atom). While some news feed clients can hande either standard, some tools like MS Outlook can only handle one (ie. RSS).

If you are a fan of news feeds you can follow using either standard:
* GCmobility [RSS Feeds](https://gcmobility.gitlab.io/feeds/all.rss.xml)
* GC Mobility [Atom Feed](https://gcmobility.gitlab.io/feeds/all.atom.xml)

### Twitter
Originall, GCmobility was simply using the [#gcmobility](https://twitter.com/hashtag/gcmobility) hash tag to track articles related to career and employee mobility within the Canadian Federal Public Service. However, it became increasingly challenging to separate project (aka work) related content from other (personal) content.

You can now follow [GCmobility on Twiter](https://twitter.com/GCmobility) while also still using the [#gcmobility](https://twitter.com/hashtag/gcmobility) hash tag.

Please follow us to stay up to date with our progress.

## Supporting Services
The main design principle of the GCmobility service to act as the glue or connection to bridge existing services. There are many existing services that clients can connect with on their own or through the GCmobility service including, but not limited to:

### Home Department
* Support from your current Talent Mananger
* Support from the adminsitrative support team working with you Talent Manager
* Home Service Desk

### Source Host Department (Leaving)
* Your previous Hiring Manager
* The support team supporting you previous hiring manager
* Previous Host Service Desk

### Destination Host Department (Joining)
* Your current Hiring Manager
* The support team supporting you current hiring manager
* Host Service Desk

## Service Types
The GCmobility service is designed to create bridges and connections between existing services. GCmobility aims to provide solutions for the following list of service types. For services noted as 'inactive' or 'Under Development', you may still submit a request, however our ability to solve these issues may be limnited at this time.

* [ ] Opportunity Hunting (Under Development)
* [ ] Letter of Agreement (Under Development)
* [ ] Security (Under Development)
* [ ] Service Desk (Active)
* [ ] Accommodation (Inactive)
* [ ] MyKey (Active)
* [ ] E-Mail (Active)
* [ ] Network Accounts
* [ ] Devices (Active)
* [ ] Phones (Active)
* [ ] Software (Under Development)
* [ ] Accessibility (Under Development)
* [ ] Information Management (Inactive)
* [ ] Data Management (Inactive)
* [ ] Telework (eg. VPN, GCSRA) (Active)

## Getting Started

This section is designed to lead a new or existing client through the process of interacting with the GCmobility Services. The following steps have been grouped into one of the following phases of the client experience.

1. [Before Contacting the service](#before-contacting)
2. [Launching A Service Request](#launch-a-service-request)
3. [After Care](#after-care)
4. [Feedback](#feedback)

### Before Contacting
1. Define the problem you are having.
2. Review the materials in the [Resource Library](#resource-library)
3. Follow any existing business processes
4. Launch a service desk ticket with either the Source, Destination or Home Department(s).

### Launch A Service Request
To launch a new service request please follow this link to [Launch a New Service Request]({filename}/pages/service-request.md).

### Follow up on an existing service Request
Simply reply to the

### After Care
The after care component of this service is still in the works. if you have any ideas, please let us know through one of the [feedback tools](#feedback)

### Feedback
To improve the GCmobility service, your feedback is required to help us understand what is working and what is not. This section outlines various means clients can use to provide feedback to the service.

* Usability Form
* [Contact us Form]({filename}/pages/contact-us.md)
* Feature Request Form
