Title: GCmobility Objectives
Date: 2020-02-12
Category: Blog
Tags: gcmobility
Slug: our-objectives
Toc_Include_Title: false
Status: hidden

The primary objectives of this partnership are 2 fold:

1. Decrease the time it takes for mobile employees (eg. Free Agents) to cross board (onboard and off board) between departments during their short term and rapid assignments throughout the Canadian Federal Government
2. Decrease the technology footprint of embedded employees that identify with both a home department, to whom they belong; and a host or partner department, to whom they provide services (eg. SSC employees); who are issued multiple devices (eg 5-20 laptops) by each partner or host department for which they provide services
