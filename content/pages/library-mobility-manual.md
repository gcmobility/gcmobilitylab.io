Title: Mobility Manual
Date: 2020-04-28
Modified: 2020-04-28
Tags: pages, library
Slug: library-mobility-manual
Toc_Include_Title: false
Status: hidden

Welcome to the Mobility Manual.

## About the Manual
The purpose of this manual is to act as a reference to help mobile workers navigate the crossboarding process. We have broken down the most common steps involved in cross boarding for mobile employees and organized them into the following groupings:

* Before Crossboarding 
* During crossboarding
* After Crossboarding

Within each section we have broken down the individual steps to provide a snapshot of who is responsible for what. For example:

* Home Department 
* Host Department 
* Employee 

We also try to add additional resources for additional support.

The manual is currently managed using Google Slides for now and is include below.

We look forward to [receiving your feedback]({filename}/pages/contact-us.md) on this resource.


## Mobility Manual Slide deck

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTkwD4WCJba0x84dEFeDPYriuFacPg456WCDxsJsgUKN6LcW7FY9rRo6mGf0-6Z0Tn7M90nhaJu4uzE/embed?start=false&loop=false&delayms=10000" frameborder="0" width="700" height="410" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
