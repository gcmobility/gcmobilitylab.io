Title: Your Service Request Has Been Submitted
Date: 2020-04-21
Modified: 2020-04-21
Tags: pages, service
Slug: service-request-thank-you
Toc_Include_Title: false
Status: hidden

Thank you for using the GCmobility service request form.

After clicking submit on the service request form your e-mail client show have created a new e-mail based on the form data.

If this e-mail has not appeared, look to see if it opened in the background or saved to your DRAFTS folder.

If this form failed to produce an e-mail, please send us a short e-mail at [gcmobility.canada.ca@gmail.com]( mailto:gcmobility.canada.ca@gmail.com?subject=Service%20Desk%3A%20Request%20Form%20Error&body=This%20e-mail%20is%20to%20inform%20you%20that%20the%20form%20located%20at%20http%3A%2F%2Fgcmobility.gitlab.io%2Fpages%2Fgcmobility-service-request.html%20has%20failed.%0D%0A%0D%0AMy%20mobility%20service%20issue%20is%20described%20here%3A%0D%0A%0D%0A)
