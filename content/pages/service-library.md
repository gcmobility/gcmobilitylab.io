Title: GCmobility Service Library (Alpha)
Date: 2020-04-22
Modified: 2020-04-22
Tags: pages, service
Slug: service-library
Toc_Include_Title: false
Status: hidden

Welcome to the GCmobility Service Library.

## About the service
The GCmobility initiative launched in April 2019, and the GCmobility service launched in April 2020. This service is provided as-is by the GCmobility initiative, a partnership between [Shared Services Canada](https://www.canada.ca/en/shared-services.html) and [Canada's Free Agents](https://wiki.gccollab.ca/Canada%27s_Free_Agents).

This service is currently operating under an Alpha Status and as such is offered to its users in a continually evolving state that might include bugs, issues and defects.

The service has a limited number of components at this time

To learn mopre about this service please use our [Contact Us Form]({filename}/pages/contact-us.md)





# Library Resources
The purpose of this library is to pull together a number of resources that might prove useful to the target audience of the GCmobility Service.



## Government of Canada Resources

## GCmobilility Resources
* [Cross Boarding Manual for Canadian Federal
Mobile Workers ](https://drive.google.com/file/d/1zpzGlShuhc7Nr5oZL98Oe2YyYQt-kuR7/view?usp=sharing)

### Canada's Free Agents Toolbox (GCpedia)    
* Free Agents Toolbox (GCpedia)

### FlexGC    
"Supporting public servants to successfully work from anywhere. #FlexGC
Aider les fonctionnaires à travailler avec succès de n'importe où. #FlexGC"

Follow [FlexGC on Twitter](https://twitter.com/FlexGCInfo) for more great resources

* Tips and Resources for New (and Old) Remote Workers including tips for getting started, for distributed teams, and for managers)
* Leading Remote Teams: Five strategies managers can use to help teams be successful during — and after — the COVID-19 pandemic
* Going Remote Guide covering topics like mental health, virtual meetings, culture, professional development, etc.

## Other Resources for the Mobile, remote or Gig Worker
